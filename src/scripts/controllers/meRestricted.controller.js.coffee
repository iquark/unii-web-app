(->

  MeRestrictedController = ['CurrentUser', 'I18nService', '$log', 'Navigation', 'Post', '$q', '$rootScope', '$scope', 'UserService', (CurrentUser, I18nService, $log, Navigation, Post, $q, $rootScope, $scope, UserService) ->

    Navigation.leaf()

    $scope.blockedUsers = []
    $scope.mutedUsers = []

    _blockUserIds = []
    _muteUserIds = []

    $scope.cancel = ->
      if (_blockUserIds+_muteUserIds).length
        data = config: 'cancel.warning'
        $rootScope.$emit 'SHOW_MODAL', data, ->
          Navigation.go('me')
      else
        Navigation.go('me')

    $scope.submit = ->
      return if $scope.savingProfile
      return unless (_blockUserIds+_muteUserIds).length
      $scope.savingProfile = true
      promises = []
      promises.push _unblockUsers(_blockUserIds) if _blockUserIds.length
      promises.push _unmuteUsers(_muteUserIds) if _muteUserIds.length
      
      _success = ->
        Post.clearCache()
        Navigation.go('me')

      _error = ->
        $log.info 'MeRestrictedController: There was an error trying to save the restricted users.'
        data = config: 'save.failed'
        $rootScope.$emit 'SHOW_MODAL', data, ->
          $log.debug 'MeRestrictedController: User is re-trying to save restrcited users.'
          $scope.submit()

      _updateUI = ->
        $scope.savingProfile = false

      $q
        .all(promises)
        .then(_success, _error)
        .finally(_updateUI)

    _unblockUsers = (users) ->
      promises = []
      for userId in users
        promises.push UserService.unblock(userId)
      $q.all(promises)

    _unmuteUsers = (users) ->
      promises = []
      for userId in users
        promises.push UserService.unmute(userId)
      $q.all(promises)

    $scope.unblock = (user) ->
      return if user.blockSelect
      data = config: 'confirm.unblock'
      $rootScope.$emit 'SHOW_MODAL', data, ->
        _blockUserIds.push user.id
        user.blockSelect = true

    $scope.unmute = (user) ->
      return if user.muteSelect
      data = config: 'confirm.unmute'
      $rootScope.$emit 'SHOW_MODAL', data, ->
        _muteUserIds.push user.id
        user.muteSelect = true

    _getRestrictedUsers = ->
      $scope.loadingUsers = true
      _success = (data) ->
        for user in data.users.data
          $scope.blockedUsers.push user if user.blocked
          $scope.mutedUsers.push user if user.muted

        $scope.mutedEmpty = true unless $scope.mutedUsers.length
        $scope.blockedEmpty = true unless $scope.blockedUsers.length

      _error = ->
        $log.info 'MeRestrictedController: There was an error trying to retrieve restricted users.'

      _updateUI = ->
        $scope.loadingUsers = false

      CurrentUser
        .restrictedUsers()
        .success(_success)
        .error(_error)
        .finally(_updateUI)

    _getRestrictedUsers()

  ]

  angular.module('Unii')
    .controller('MeRestrictedController', MeRestrictedController)
)()