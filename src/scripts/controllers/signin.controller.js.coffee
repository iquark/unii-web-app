# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

Unii = angular.module 'Unii'

Unii.controller 'SigninCtrl', ['$scope', 'CurrentUser', 'Navigation', 'Validation', 'I18nService', 'Mixpanel', ($scope, CurrentUser, Navigation, Validation, I18nService, Mixpanel) ->

  Mixpanel.startEvent('Sign In')

###############################################################################
#                                 AUTHENTICATION                              #
###############################################################################

  $scope.signin_form = {
    errors: []
  }

  $scope.view = Navigation.go

  $scope.request_sent = false

  # Regex to override default email regex for validation
  # Default allows emails such as 'x@x'
  $scope.email_regex = Validation.regex.email

  $scope.open_forgot_modal = ->
    angular.element("#forgot-email").focus()
    $scope.forgot_password = true
    $scope.request_sent = false
    $scope.error = false

  $scope.forgot_email = ''

  $scope.reset_password = (email) ->

    if $scope.ResetPasswordForm.$valid and $scope.forgot_email
      $scope.reseting_password = true
      request = CurrentUser.resetPassword($scope.forgot_email)

      request.success ->
        $scope.reset_message  = I18nService.t('password_reset_success_body')

      request.error ->
        $scope.reset_message  = I18nService.t('password_reset_error_body')

      request.finally ->
        $scope.forgot_email   = ''
        $scope.request_sent   = true
        $scope.reseting_password = false

    else
      $scope.error = true


]


