###
# @desc Controller for the static pages
# @file staticPages.controller.js.coffee
###

(->

  staticPagesController = ['$scope', 'Navigation', '$timeout', '$window', ($scope, Navigation, $timeout, $window) ->

    Navigation.leaf()

    $scope.view_support = (type) ->
      Navigation.go('support').search({type: type})

    $scope.view_eula = ->
      Navigation.go('eula')

    $scope.view_privacy = ->
      Navigation.go('privacy')

    $scope.goBack = Navigation.back
  ]

  angular.module('Unii')
    .controller('StaticPagesController', staticPagesController)
)()