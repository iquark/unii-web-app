###
# @desc Controller editing a users password
# @file meEditPassword.controller.js.coffee
###

(->
  meEditPasswordController = ['CurrentUser', '$scope', 'I18nService', '$log', 'Navigation', 'Normalize', '$rootScope', (CurrentUser, $scope, I18nService, $log, Navigation, Normalize, $rootScope) ->

    Navigation.leaf()

    $scope.view = Navigation.go
    $scope.userColor = CurrentUser.profileForm.color || $rootScope.userSettings.color

    $scope.form =
      errors: []

    $scope.submit = ->
      return if $scope.sendingForm
      $scope.sendingForm = true

      # clear any existing errors
      $scope.form.errors = []

      if $scope.form.password != $scope.form.password_confirmation
        $scope.form.errors.push I18nService.t('password_confirmation_does_not_match_error_body')

      unless $scope.form.current_password?.length
        $scope.form.errors.push I18nService.t('current_password_cant_be_blank_error_body')

      unless $scope.form.password
        $scope.form.errors.push I18nService.t('password_cant_be_blank_error_body')

      if $scope.form.password?.length < 8
        $scope.form.errors.push I18nService.t('password_is_too_short_error_body')

      if $scope.form.password?.length > 128
        $scope.form.errors.push I18nService.t('password_is_too_long_error_body')

      if $scope.form.errors.length
        $scope.sendingForm = false
      else
        # submit form
        form =
          current_password: $scope.form.current_password
          password: $scope.form.password

        _success = ->
          $log.info 'CurrentUser: changed password successfully.'
          Navigation.go('meEdit')

        _error = (data) ->
          $log.error 'CurrentUser: failed to change password.'
          $log.debug data
          angular.forEach data.errors, (error) ->
            $scope.form.errors.push I18nService.t(Normalize.error(error.code)+'_error_body')

        _updateUI = ->
          $scope.sendingForm = false

        CurrentUser
          .editPassword(form)
          .success(_success)
          .error(_error)
          .finally(_updateUI)


        

  ]

  angular
    .module 'Unii'
    .controller 'MeEditPasswordController', meEditPasswordController
)()