Unii = angular.module 'Unii'

Unii.controller 'MainCtrl', ['$scope', '$rootScope', '$window', 'File', 'UI', 'Request', '$interval', 'Navigation', 'Browser', '$q', 'CurrentUser', 'ipCookie', 'AssetsService', '$compile', '$timeout', 'INTERVAL_NOTIFICATION', '$route', 'API_DOMAIN', '$log', 'PRE_UNI', '$filter', '$location', 'Notification', 'Mixpanel', ($scope, $rootScope, $window, File, UI, Request, $interval, Navigation, Browser, $q, CurrentUser, ipCookie, AssetsService, $compile, $timeout, INTERVAL_NOTIFICATION, $route, API_DOMAIN, $log, PRE_UNI, $filter, $location, Notification, Mixpanel) ->


  #############################################################################
  #                               USER INTERFACE                              #
  #############################################################################

  # when a 'pre_uni' user tries to go to any page, will be redirected to the pre-uni page to confirm its emafil
  check_unverified = ->

    if CurrentUser.state() in PRE_UNI.unverifiedStates and ipCookie('token') and $location.path() isnt PRE_UNI.verificationPath
      Navigation.go('preUni')

  authorize = (path, app_load) ->
    if ipCookie('token')
      if app_load
        CurrentUser.get()
          .then ->
            Mixpanel.track 'App Opened', {'signed_in?':true}
            check_unverified()

            # inform UI user is signed in
            UI.signedIn = true
            Notification.start() unless CurrentUser.state() in PRE_UNI.unverifiedStates

      regEx = /(signin|signup|signup_location|intro)/

      if regEx.test(path)
        Navigation.go('feed')

    else
      if app_load
        Mixpanel.track 'App Opened', {'signed_in?':false}
      urlRegEx = /(signin|signup|eula|privacy|support|challenges)/
      unless urlRegEx.test(path)
        Navigation.go('intro')

  authorize($location.path(), true)

  # push current page to google analytics
  routeChangeSuccessListener = $rootScope.$on '$routeChangeSuccess', (e, data) ->
    $window.ga('send', 'pageview', { page: $location.path() })
    Navigation.history($location.$$path)

    unless /^(\/|\/profile\/\d+|\/me)$/.test($location.$$path)
      UI.view_loading = false
    $rootScope.noInternet = false


  routeChangeStartListener = $rootScope.$on '$routeChangeStart', (e, data) ->
    UI.view_loading = true if Browser.isMobile()
    authorize(data.$$route.originalPath, false) if data.$$route
    check_unverified()

  routeChangeErrorListener = $rootScope.$on '$routeChangeError', (e, current, previous, rejection) ->
    UI.view_loading = false
    $log.error 'routeChangeError:', e, current, previous, rejection
    $rootScope.noInternet = true

  $scope.retry_network = ->
    $window.location.reload()

  # spread changes in UI to the scope
  uiListener = $scope.$watch(
    -> UI
    -> $scope.ui = UI
    )

  $scope.location = $location

  $scope.galleries = {}

  $scope.gallery_inview = (post_id, inview) ->
    $scope.galleries[post_id] = inview

  $scope.view_feed = ->
    Navigation.go('feed')

  $scope.show_image = (url) ->
    $log.debug url

  $scope.viewing = (path) ->
    $location.path() == path


  #############################################################################
  #                               MEDIA UPLOADER                              #
  #############################################################################

  $scope.select_media = (media_type) ->
    UI.selecting_media = true
    $scope.media_type = media_type
    angular.element('#media-file').click()

    true

  $scope.select_photo = (media_type, referrer) ->
    $scope.media_type = media_type
    $scope.avatar_referrer = referrer
    angular.element('#image-file').trigger 'click'
    true

  $scope.select_video = (media_type) ->
    $scope.media_type = media_type
    angular.element('#video-file').trigger 'click'
    true


  clearPostsQueueListener = $rootScope.$on 'CLEAR_POSTS_QUEUE', ->
    $scope.post_queue = []
    media_types = []

  $scope.$on '$destroy', ->
    uiListener()
    authSignOutListener()
    authSignedInListener()
    routeChangeErrorListener()
    routeChangeStartListener()
    routeChangeSuccessListener()
    clearPostsQueueListener()
]
