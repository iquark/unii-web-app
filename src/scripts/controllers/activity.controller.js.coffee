###
# @desc Controller for the page Activity
# @file activity.controller.js.coffee
###

(->
  activityController = ['$scope', '$log', 'Mixpanel', 'Navigation', 'Notification', 'CurrentUser', '$timeout', 'Post', 'UserService', '$rootScope', ($scope, $log, Mixpanel, Navigation, Notification, CurrentUser, $timeout, Post, UserService, $rootScope) ->

    Mixpanel.startEvent('Notifications Viewed')

    # public
    $scope.loading = true
    $scope.notificationRedirect = Post.view
    $scope.goBack = Navigation.back

    $scope.paginate = ->
      return if $scope.paginating or $rootScope.noInternet
      $scope.paginating = true
      Notification
        .paginate($scope)

    $scope.refresh = ->
      $scope.loading = true
      $timeout _check, 750


    # private
    _check = ->
      if Notification.check($scope)
        $scope.loading = false
      else
        $timeout _check, 2000
    _check()
    

    $scope.$on '$destroy', ->
      Notification.markAllViewed()
      Mixpanel.notificationsViewed()
  ]

  angular
    .module 'Unii'
    .controller 'ActivityController', activityController
)()


