###
# @desc Controller editing a users password
# @file meEditPassword.controller.js.coffee
###

(->
  meEditCourseController = ['CurrentUser', '$scope', 'I18nService', '$log', 'Navigation', 'Normalize', '$rootScope', '$route', (CurrentUser, $scope, I18nService, $log, Navigation, Normalize, $rootScope, $route) ->

    Navigation.leaf()

    $scope.userProfile = $route.current.locals.profileData
    $scope.userColor = CurrentUser.profileForm.color || $rootScope.userSettings.color

    $scope.loadingCourses = true
    selectedCourse = null

    $scope.profileForm = CurrentUser.profileForm

    $scope.view = (path) ->
      if selectedCourse
        data =
          config: 'cancel.warning'
        $rootScope.$emit 'SHOW_MODAL', data, ->
          Navigation.go(path)
      else
        Navigation.go(path)

    $scope.setCourse = (event) ->
      targetId = event.target.id
      parentId = event.target.parentNode.id
      courseIndex = parseInt(targetId, 10) || parseInt(parentId, 10)

      selectedCourse = $scope.courses[courseIndex]
      $scope.courseSearch = selectedCourse.name

    $scope.returnToEdit = ->
      $scope.sendingForm = true
      CurrentUser.profileForm.course = selectedCourse
      Navigation.go('meEdit')

    _success = (data) ->
      $scope.courses = data

    _updateUI = ->
      $scope.loadingCourses = false

    CurrentUser
      .getCourses($scope.userProfile.university.id)
      .then(_success)
      .finally(_updateUI)

  ]

  angular
    .module 'Unii'
    .controller 'MeEditCourseController', meEditCourseController
)()