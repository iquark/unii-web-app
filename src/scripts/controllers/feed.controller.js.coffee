###
# @desc Controller for the Feed page
# @file feed.controller.js.coffee
###

(->

  feedController = ['$anchorScroll', 'CoachMarksService', 'CurrentUser', 'Feed', 'File', 'LazyLoad', '$location', '$log', 'MediaService', 'MobileMenuService', 'Navigation', 'PostCreation', 'Post', '$q', '$rootScope', '$scope', 'STATUSES', '$swipe', '$timeout', 'UI', 'ipCookie', ($anchorScroll, CoachMarksService, CurrentUser, Feed, File, LazyLoad, $location, $log, MediaService, MobileMenuService, Navigation, PostCreation, Post, $q, $rootScope, $scope, STATUSES, $swipe, $timeout, UI, ipCookie) ->


    $scope.university = CurrentUser.getUniversity()
    uniListener = $rootScope.$on 'USER_SETTINGS', ->
      $scope.university = CurrentUser.getUniversity()

    CoachMarksService.setName($scope.university.name_short)
    ###########################################################################
    #                              USER INTERFACE                             #
    ###########################################################################

    $location.hash('')

    $scope.feed = []
    $scope.whatsHot = []
    country = null  # country for What's Hot
    $scope.nav = 'feed'
    $scope.fetchingPosts = false
    $scope.fetchingWhatsHot = false
    $scope.feedSwitcher = ""
    $scope.errorLoadingFeed = false
    $scope.errorLoadingWhatsHot = false
    changingFeed = false
    
    postForm = null
    currentFeed = Feed.onWhatsHot()

    if currentFeed
      $scope.referrer = 'whatsHot'
    else
      $scope.referrer = 'feed'

    # timers
    timerCheckStatus = null
    timerAnchor = null
    timerPopulate = null
    timerPopulateWH = null

    checkPostStatus = ->
      $scope.creatingPost = PostCreation.status() == STATUSES.WORKING
      $scope.savingPost = PostCreation.status() == STATUSES.PROCESSING
      $scope.postFailed = PostCreation.status() == STATUSES.ERROR
      timerCheckStatus = $timeout(checkPostStatus, 1000) if PostCreation.status() == STATUSES.WORKING or PostCreation.status() == STATUSES.PROCESSING

    $scope.postFailed = PostCreation.status() == STATUSES.ERROR

    checkPostStatus() if PostCreation.status() == STATUSES.WORKING or PostCreation.status() == STATUSES.PROCESSING



    _initializeFeed = ->

      _complete = ->
        if changingFeed
          $location.hash('')
          $anchorScroll.yOffset = 0
          $anchorScroll() # scroll to page top when changing feeds
          LazyLoad.trigger()
          country = null

        changingFeed = false

        if currentFeed
          # What's Hot
          _move feedPageLoader, -width
        else
          # Main
          _move feedPageLoader, width

        $scope.$broadcast 'CHANGING_FEED', false


      _success = (posts) ->
        $scope.feed = posts
        feedPageLoader.removeClass('transitions')
        feedViewPort.removeClass('transitions')
        _move feedViewPort, 0

        $timeout(_complete, 1000)

      _error = (message) ->
        $log.log message

      Post
        .initializeFeed(currentFeed, country)
        .then(_success)
        .catch(_error)


    $timeout(_initializeFeed, 500) if ipCookie('token')


    $scope.loadingPosts = false

    $scope.paginateFeed = ->
      return if $scope.loadingPosts
      $scope.loadingPosts = true

      _success = (posts) ->
        $scope.feed = posts

      _error = (message) ->
        $log.warn message

      _updateUI = ->
        $scope.loadingPosts = false

      Post
        .paginateFeed(currentFeed, $scope.feed)
        .then(_success)
        .catch(_error)
        .finally(_updateUI)



    $scope.cancelPost =  ->
      PostCreation.cancel()
      $scope.postFailed = false

    $scope.refreshFeed = ->
      Post.clearCache()
      $scope.feed = []
      _initializeFeed()


    $scope.refreshWhatsHot = ->
      Post.clearCacheWhatsHot()
      $scope.feed = []
      _initializeFeed()


    $scope.resendPost = ->
      $scope.postFailed = false
      request = PostCreation.send()
      checkPostStatus()
      request.then(
        ->
          $log.info "PostCreation:success"
        (message) ->
          $scope.postFailed = true
          $log.error 'PostCreation:error', message
        )

    $scope.viewActivity = ->
      Navigation.go('activity')

    $scope.viewMe = ->
      Navigation.go('me')

    $scope.writePost = ->
      Navigation.go('compose')

    _whatsHotChangeCountry = (countryCode) ->
      country = countryCode
      CurrentUser.settings hot_feed_country: countryCode
      $scope.refreshWhatsHot()
      MobileMenuService.close()

    $scope.selectCountry = ->
      MobileMenuService.setOptions(
          [
            {label: "UK", highlight: ($rootScope.userSettings.hot_feed_country == 'uk'), handler: -> _whatsHotChangeCountry('uk')},
            {label: "US", highlight: ($rootScope.userSettings.hot_feed_country == 'us'), handler: -> _whatsHotChangeCountry('us')},
            {label: "UK & US", highlight: ($rootScope.userSettings.hot_feed_country == 'all'), handler: -> _whatsHotChangeCountry('all')}
          ]
        )
      MobileMenuService.showCancel(false)
      MobileMenuService.setType('under-header')
      MobileMenuService.show()

    _move = (element, position) ->
      element.css
        '-webkit-transform': 'translate3d('+position+'px, 0, 0)'
        '-ms-transform': 'translate3d('+position+'px, 0, 0)'
        transform: 'translate3d('+position+'px, 0, 0)'

    pointX = 0
    pointY = 0
    width = 0
    feedWidth = 0
    navNames = angular.element('#we-are-at')
    universityNav = navNames.find('#university-nav')
    whatsHotNav = navNames.find('#whats-hot-nav')
    whatsHotPosition = 0
    # The distance between both words will be from the middle point of each,
    # because it will be the point that will be placed on feedWidth/2
    navNamesWidth = navNames[0].clientWidth
    uniNavName = 0
    whatsHotNavName = -(navNamesWidth/2)
    navNamesRate = 0
    dotRate = 0
    viewport = angular.element('#draggable-viewport')
    feedViewPort = angular.element('.view-port-container')
    feedPageLoader = angular.element('.feed-page-loader')

    # University name
    head = angular.element('.brand')

    # The two dots above the university name
    feedDot = head.find('#feed-dot')
    whatsHotDot = head.find('#whatshot-dot')
    currentDot = head.find('#current-dot')
    threshold = 0

    currentDotPosition = currentDot[0].offsetLeft
    uniDotPosition = feedDot[0].offsetLeft-currentDotPosition
    whatsHotDotPosition = whatsHotDot[0].offsetLeft-currentDotPosition

    _move currentDot, uniDotPosition

    delta = 0 # offset on feed
    deltaY = 0
    showFeed = true

    width = viewport[0].clientWidth
    navNamesRate = navNamesWidth/(width*2)

    dotRate = 20/width
    threshold = width*0.1
    feedWidth = width/2
    whatsHotPosition = -(width - feedWidth)

    movingViewport = false


    _addClass = (elements, className) ->
      for e in elements
        e.addClass className

    _removeClass = (elements, className) ->
      for e in elements
        e.removeClass className


    # The user makes click to start the movement
    viewport.on 'touchstart', (e) ->
      navNames.removeClass 'transitions'
      feedViewPort.removeClass 'transitions'

      _removeClass([navNames, feedViewPort], 'transitions')

      pointX = e.originalEvent.touches[0].clientX
      pointY = e.originalEvent.touches[0].clientY


    # The user drags by the screen
    viewport.on 'touchmove', (e) ->

      delta = e.originalEvent.touches[0].clientX - pointX
      deltaY = e.originalEvent.touches[0].clientY - pointY


      if movingViewport or (Math.abs(delta) > threshold) and (Math.abs(deltaY)<Math.abs(delta))
        movingViewport = true
        if delta > 0 and currentFeed

          # switching from What's Hot feed to main feed.
          dotMove = Math.max(whatsHotDotPosition-dotRate*(delta-threshold), uniDotPosition)
          navNamesMove = Math.min(whatsHotNavName+(navNamesRate*(delta-threshold)), 0)
          feedPageLoaderMove = -(width)+(delta-threshold)
          feedViewPortMove = Math.max(0, (delta-threshold))

          whatsHotOpacity = 1+(delta-threshold)/whatsHotNavName
          feedOpacity = 1-whatsHotOpacity

          if feedOpacity > 0.5
            changingFeed = true

        else if delta <= 0 and not currentFeed
          # switching from main feed to What's Hot feed
          dotMove = Math.min(uniDotPosition-dotRate*(delta+threshold), whatsHotDotPosition)
          navNamesMove = Math.max(navNamesRate*(delta+threshold), whatsHotNavName)
          feedPageLoaderMove = width+delta+threshold
          feedViewPortMove = Math.min(0, (delta+threshold))

          whatsHotOpacity = (navNamesRate*(delta+threshold))/(whatsHotNavName)
          feedOpacity = 1-whatsHotOpacity

          if whatsHotOpacity > 0.5
            changingFeed = true

        _move currentDot, dotMove
        _move navNames, navNamesMove
        _move feedPageLoader, feedPageLoaderMove
        _move feedViewPort, feedViewPortMove

        universityNav.css 'opacity', feedOpacity
        whatsHotNav.css 'opacity', whatsHotOpacity


    _showMainFeed = ->
    
      whatsHotNav.css 'opacity', 0
      universityNav.css 'opacity', 1

      Feed.onWhatsHot(false)

      CoachMarksService.showFeed()
      _move navNames, uniNavName
      _move currentDot, uniDotPosition

      if changingFeed
        _move feedViewPort, width
        _move feedPageLoader, 0
        currentFeed = false
        $scope.referrer = 'feed'
      else
        _removeClass([feedPageLoader, feedViewPort], 'transitions')
        _move feedViewPort, 0
        _move feedPageLoader, width

      
    _showWhatsHotFeed = ->

      whatsHotNav.css 'opacity', 1
      universityNav.css 'opacity', 0

      Feed.onWhatsHot(true)

      CoachMarksService.showWhatsHot()
      _move navNames, whatsHotNavName
      _move currentDot, whatsHotDotPosition

      if changingFeed
        _move feedPageLoader, 0
        _move feedViewPort, -width
        currentFeed = true
        $scope.referrer = 'whatsHot'

      else
        _removeClass([feedPageLoader, feedViewPort], 'transitions')
        _move feedPageLoader, -width
        _move feedViewPort, 0


    # the action finishes
    viewport.on 'touchend', (e) ->

      if movingViewport
        # enable transitions
        _addClass([navNames, feedPageLoader, feedViewPort], 'transitions')

        if changingFeed
          currentFeed = !currentFeed
          $scope.$broadcast 'CHANGING_FEED', true

        if currentFeed
          _showWhatsHotFeed()
        else
          _showMainFeed()

        
        _resetUI = ->
          movingViewport = false
          delta = 0

        $timeout(_resetUI, 800)
        $timeout(_initializeFeed, 800) if changingFeed
        
      return true





    viewContentLoaded = $scope.$on '$viewContentLoaded', ->

      if Feed.onWhatsHot()
        feed = 'whatsHot'
        _showWhatsHotFeed()
      else
        feed = 'feed'
        CoachMarksService.showFeed()

      id = Post.scrollId(feed)
      id = parseInt(id, 10)
      attempt = 0

      _updateUI = ->
        UI.view_loading = false
        LazyLoad.trigger()

      _scrollToPost = ->
        $location.hash('post_' + id)
        $anchorScroll.yOffset = 64 #offset 64px for header
        $anchorScroll()
        $timeout _updateUI, 800

      anchorScroll = ->
        attempt++
        if id
          # User was viewing a post previously

          postLoaded = false

          if $scope.feed.length

            for post in $scope.feed
              postLoaded = true if id == post.id

            if postLoaded
              _scrollToPost()
            else
              # load more posts
              request = $scope.paginateFeed()
              if request and (attempt < 60)
                request.then(anchorScroll)
              else
                UI.view_loading = false
                $log.info('FeedController: Post id given to scroll to. But unable to find post id in feed after loading all posts, so cannot scroll to it :(')

          else
            $log.info('FeedController: Post id given to scroll to. However, waiting for it to be initialized. Will try again in 1s.')
            $timeout(anchorScroll, 1000)

        else
          _updateUI()

      anchorScroll()


    postDeletionListener = $rootScope.$on 'POST.DELETED', (e, index) ->
      $scope.feed.splice index, 1

    postCreationListener = $rootScope.$on 'POST.NEW', (e, post) ->
      $scope.feed.unshift post
  

    $scope.$on '$destroy', ->
      viewContentLoaded()
      uniListener()
      postDeletionListener()
      postCreationListener()
      $timeout.cancel timerCheckStatus
      $timeout.cancel timerAnchor
      $timeout.cancel timerPopulate
      $timeout.cancel timerPopulateWH
  ]

  angular
    .module 'Unii'
    .controller 'FeedController', feedController
)()