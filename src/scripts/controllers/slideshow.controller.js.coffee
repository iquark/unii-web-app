Unii = angular.module 'Unii'


Unii.controller 'SlideshowCtrl', ['$scope', 'Navigation', '$route', 'MediaService', ($scope, Navigation, $route, MediaService) ->

  Navigation.leaf()

  object = $route.current.locals.resolvedData
  id = $route.current.params.id

  if object.meta
    # profile
    $scope.slides = MediaService.cachedImages(id)
    $scope.total_slides = object.meta.images_count
    $scope.view_images = ->
      Navigation.go('profileImages', {id: id})

  else
    # post
    $scope.slides = object.medias.data
    MediaService.cacheAlbum(id, object)
    $scope.total_slides = object.medias_count
    $scope.view_images = ->
      Navigation.go('postImages', {id: id})

  $scope.goBack = Navigation.back
]


Unii.loadProfileImages = ['$q', 'MediaService', '$route', ($q, MediaService, $route) ->
  deferred = $q.defer()

  request = MediaService.getImages($route.current.params.id)

  request.then(
    (data) ->
      deferred.resolve(data)
    (message) ->
      deferred.reject(message)
    )

  deferred.promise
]
