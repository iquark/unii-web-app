###
# @desc Controller for the page to accept a new avatar
# @file avatar.controller.js.coffee
###

(->
  avatarController = ['$scope', '$routeParams', '$log', 'MediaService', 'Mixpanel', 'STATUSES', 'Navigation', 'CurrentUser', '$rootScope', 'UserService', ($scope, $routeParams, $log, MediaService, Mixpanel, STATUSES, Navigation, CurrentUser, $rootScope, UserService) ->

    Navigation.leaf()

    referrer = $routeParams.referrer if $routeParams.referrer

    # sync view media queue with service
    $scope.mediaQueue = MediaService.mediaQueue()

    $scope.usePhoto = ->
      CurrentUser.setStatus(STATUSES.WORKING)
      Mixpanel.profilePhotoAdded()

      _success = (data) ->
        # Store updated user in memory
        CurrentUser.store data.user
        CurrentUser.setStatus(STATUSES.IDLE)

      _error = (message) ->
        CurrentUser.setStatus(STATUSES.IDLE)
        $log.error('avatarController: ' + message)

      CurrentUser
        .setAvatar()
        .then(_success, _error)

      Navigation.go('me').search({state: 'new-avatar'})
      

    $scope.returnToMe = ->
      MediaService.destroyForm()
      Navigation.go('me').search({})

  ]

  angular
    .module 'Unii'
    .controller 'AvatarController', avatarController
)()