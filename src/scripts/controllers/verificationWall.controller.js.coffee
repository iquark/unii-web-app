Unii = angular.module 'Unii'

Unii.controller 'PreUniCtrl', ['Authentication', '$scope', '$route', 'CurrentUser', 'Normalize', 'Validation', 'UI', '$rootScope', '$location', 'PRE_UNI', 'Navigation', 'I18nService', (Authentication, $scope, $route, CurrentUser, Normalize, Validation, UI, $rootScope, $location, PRE_UNI, Navigation, I18nService) ->

  Navigation.leaf()

  if CurrentUser.state() not in PRE_UNI.unverifiedStates and $location.path() is PRE_UNI.verificationPath
    Navigation.go('feed')

  # functions for the different states of the modal
  _modal_close = ->
    UI.set {
      show_modal: false
      request_sending: false
      request_sent: false
      post_warning: false
    }

  $scope.modal_close = _modal_close

  _modal_sending = ->
    UI.set {
      show_modal: true
      request_sending: true
      request_sent: false
      post_warning: false
    }

  _modal_sent = ->
    UI.set {
      show_modal: true
      request_sending: false
      request_sent: true
      post_warning: false
    }

  _modal_error = ()->
    data =
      config: 'preuni.warning'
      meta:
        title: $scope.errors.join('<br>')
    $rootScope.$emit 'SHOW_MODAL', data

    UI.set {
      show_modal: true
      request_sending: false
      request_sent: false
      post_warning: true
    }

  # move the user data to session storage, to expire it after close the browser
  CurrentUser.switchStorage(false)

  # endpoint to send the email, showing or hiding the modal
  _resend_email = ->
    $scope.errors = []
    request_resend = CurrentUser.resendConfirmationEmail()

    request_resend.success ->
      _modal_sent()

    request_resend.error (data) ->
      if data?
        for error in data.errors
          if error.code == 'university:cant_be_blank'
            message = I18nService.t('email_is_not_a_valid_university_email_error_body')
          else
            message = I18nService.t(I18nService.t(Normalize.error(error.code)+'_error_body'))
          $scope.errors.push(message)
      else
        $scope.errors.push(I18nService.t('no_internet_connection_error_body'))
      _modal_close()
      _modal_error() if !$scope.editing

  # initialise the modal
  _modal_close()

  $scope.editing = false
  $scope.profile = $route.current.locals.profileData
  $scope.errors = []

  $scope.signOut = Authentication.signout

  # Regex to override default email regex for validation
  # Default allows emails such as 'x@x'
  $scope.email_regex = Validation.regex.email

  $scope.resend_confirmation_email = ->
    # show the modal in 'Sending' status
    _modal_sending()
    $scope.errors = []

    if $scope.editing
      $scope.editing = false
      if $scope.email
        user_data =
          email: $scope.email
        request = CurrentUser.edit(user_data)

        request.success () ->
          $scope.profile.email = $scope.email
          _resend_email()

        request.error (data) ->
          if data?
            for error in data.errors
              if error.code == 'email:is_not_a_valid_university_email'
                message = I18nService.t('email_is_not_a_valid_university_email_error_body')
                $scope.errors.push message
          else
            $scope.errors.push(I18nService.t('no_internet_connection_error_body'))
          _modal_close()
          _modal_error() if !$scope.editing
      else
        $scope.errors = [I18nService.t('email_cant_be_blank_error_body')]
        _modal_close()
    else
      _resend_email()
]
