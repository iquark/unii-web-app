###
# @desc Controller for the list of images of a user profile or a post
# @file images.controller.js.coffee
###

(->
  imagesController = ['$scope', '$route', '$rootScope', '$routeParams', 'Mixpanel', 'Navigation', '$log', 'MediaService', 'Request', ($scope, $route, $rootScope, $routeParams, Mixpanel, Navigation, $log, MediaService, Request) ->
    object = $route.current.locals.resolvedData
    id = $route.current.params.id

    profile = object.first_name?
    minIndexViewed = 0
    maxIndexViewed = null

    if profile

      $scope.profile = object

      $scope.writePost = ->
        Navigation.go('compose')

      $scope.viewSlide = (index) ->
        Navigation.go('profileSlides', {id: id}).hash(index+1)

      # retrieves the images page per page
      $scope.paginateImages = ->
        $scope.loadingImages = true

        success = (data) ->
          $scope.images = MediaService.cachedImages(object.id)

        error = (message) ->
          $log.warn message

        updateUI = ->
          $scope.loadingImages = false

        MediaService
          .paginateImages(object.id)
          .then(success)
          .catch(error)
          .finally(updateUI)


      loadImages = ->
        $scope.loadingImages = true
        success = (data) ->
          $scope.images = MediaService.cachedImages(object.id)

        error = (message) ->
          $log.warn message

        updateUI = ->
          $scope.loadingImages = false
          $scope.noImages = true unless $scope.images.length

        MediaService
          .getImages(object.id)
          .then(success)
          .catch(error)
          .finally(updateUI)

      loadImages()


      $scope.thumbInView = (image) ->
        if image.inview
          maxIndexViewed = Math.max(maxIndexViewed, image.index)

    else # post

      $scope.viewSlide = (index) ->
        Navigation.go('postSlides', {id: id}).hash(index+1)

      $scope.images = object.medias.data
      nextPage = object.medias.pagination.next_page

      # retrieves the images page per page
      $scope.paginateImages = ->
        $scope.loadingImages = true

        success = (data) ->
          $scope.images = $scope.images.concat data.data.medias.data
          nextPage = data.data.medias.pagination.next_page

        error = (message) ->
          $log.warn message

        updateUI = ->
          $scope.loadingImages = false

        if nextPage
          Request
          .send nextPage, 'GET'
          .then(success)
          .catch(error)
          .finally(updateUI)
        else
          $scope.loadingImages = false

      if $scope.images.length < 12 and $scope.images.length < object.medias_count
        $scope.paginateImages()

    $scope.goBack = Navigation.back


    $scope.$on '$destroy', ->
      currentUser = parseInt(id, 10) == $rootScope.userSettings.id
      Mixpanel.photoGridScrolled(currentUser, maxIndexViewed+1) if profile



  ]

  angular.module('Unii')
    .controller('ImagesController', imagesController)
)()
