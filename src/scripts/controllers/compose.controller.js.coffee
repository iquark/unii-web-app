Unii = angular.module 'Unii'

Unii.controller 'PostsCtrl', ['$scope', '$q', '$interval', 'UI', '$log', '$rootScope', 'Mixpanel', 'Navigation', 'PostCreation', 'MediaService', 'I18nService', 'Util', 'Validation', ($scope, $q, $interval, UI, $log, $rootScope, Mixpanel, Navigation, PostCreation, MediaService, I18nService, Util, Validation) ->

  Navigation.leaf()

  Mixpanel.startEvent('Compose Post')

  # create media form dynamically on ng-click
  $scope.add_media = MediaService.createForm

  MediaService.setCallbacks({})

  # sync view media queue with service
  $scope.media_queue = MediaService.mediaQueue()
  # initialise post form
  $scope.post_form = {}


  $scope.autoGrowTextArea = (event) ->
    options = minRows: 4
    Util.autoGrowTextArea(event.target, options)

  $scope.cancel_file = (file, index) ->
    $log.log 'cancel file'
    # abort file upload
    file.$cancel() if file.$state() == 'pending'
    MediaService.removeFromMediaQueue(index)


  $scope.create_post = ->
    if $scope.post_form?.content?.match(Validation.regex.url) and $scope.media_queue.length>0
      data = config: 'post.multimedia_warning'
      $rootScope.$emit 'SHOW_MODAL', data
    else
      _success = ->
        $log.info "PostCreation:success"
        $scope.post_form = {}

      _error = (message) ->
        $log.error 'PostCreation:error', message

      PostCreation
        .send($scope.post_form, $scope.media_queue)
        .then(_success, _error)

      # redirect user to feed
      Navigation.go('feed')


  $scope.cancel_post = ->
    if $scope.post_form.content or $scope.media_queue.length
      data = config: 'cancel.warning'
      $rootScope.$emit 'SHOW_MODAL', data, ->
        PostCreation.cancel()
        $scope.post_form = {}
        UI.uploading = false
        Navigation.go('feed')

    else
      Navigation.back()

  UI.selecting_media = false

]
