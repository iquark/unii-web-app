Unii = angular.module 'Unii'

Unii.controller 'ProfileCtrl', ['$scope', 'UserService', 'Navigation', '$route', 'CurrentUser', '$timeout', '$anchorScroll', '$log', '$rootScope', '$routeParams', 'MediaService', 'STATUSES', 'Post', '$location', 'UI', 'Mixpanel', 'MobileMenuService', 'Authentication', 'I18nService', ($scope, UserService, Navigation, $route, CurrentUser, $timeout, $anchorScroll, $log, $rootScope, $routeParams, MediaService, STATUSES, Post, $location, UI, Mixpanel, MobileMenuService, Authentication, I18nService) ->

  $location.hash('')
  Mixpanel.startEvent('Profile Viewed')
  pageLoad = true

  $scope.slide = 'avatar'
  documentWidth   = parseInt($(document).width(), 10)

  user_profile = $route.current.locals.profileData

  timer = null

  $scope.getting_more_user_posts = false
  $scope.profile = user_profile
  $scope.feed = user_profile.posts.data
  $scope.pagination = user_profile.posts.pagination
  timer = null

  setCurrentUser = ->
    if $rootScope.userSettings?.id
      $scope.current_user = user_profile.id == $rootScope.userSettings.id
    else
      $timeout(setCurrentUser, 100)

  setCurrentUser()

  if $routeParams.state == 'new-avatar' and $scope.current_user
    $scope.avatar_loading = true

    _checkStatus = ->

      _setProfile = (data) ->
        $scope.profile = data
        $scope.trigger = true

      _error = ->
        $log.error 'profileController: Could not get current user.'

      _getCurrentUser = ->
        CurrentUser
          .get()
          .then(_setProfile, _error)

      if CurrentUser.status() == STATUSES.WORKING
        timer = $timeout(_checkStatus, 1000)
      else
        timer = $timeout(_getCurrentUser, 500)

    _checkStatus()


  # moves the user to the album of photos of the profile is watching
  # @todo this function and the below one exists in me_ctrl.js consider refactoring
  $scope.get_user_images = ->
    Navigation.go('profileImages', {id: $scope.profile.id})

  # create media form dynamically on ng-click
  $scope.add_media = ->
    MediaService.createForm(true, true) # destroys any existent form, and only images
    MediaService.setCallbacks(
      {
        fileuploadprocessdone: ->
          Navigation.go('meAvatarPreview').search({referrer: 'profile'})
      }
    )

  $scope.write_post = ->
    Navigation.go('compose')

  # retrieves more posts from the current user
  $scope.paginate_posts = ->
    if $scope.pagination.next_page == null
      $scope.end_posts = true
      return
    $scope.loading_posts = true

    _success = (data) ->
      $scope.feed = $scope.feed.concat data.posts.data
      $scope.pagination = data.posts.pagination

    _error = ->
      $log.warn 'ProfileController: There was an error getting more profile posts...'

    _updateUI = ->
      $scope.loading_posts = false

    UserService
      .paginatePosts($scope.pagination)
      .success(_success)
      .error(_error)
      .finally(_updateUI)


  # once the page is loaded, the viewport is moved to the top
  contentLoadedListener = $scope.$on '$viewContentLoaded', ->
    id = Post.scrollId('profile')
    attempt = 0

    _scrollToPost = ->
      $location.hash('post_' + id)
      $anchorScroll.yOffset = 0
      $anchorScroll()
      UI.view_loading = false

    anchorScroll = ->
      attempt++
      if id
        # User was viewing a post previous
        postLoaded = false

        for post in $scope.feed
          postLoaded = true if id == post.id

        if postLoaded
          _scrollToPost()
        else
          request = $scope.paginate_posts()
          if request and (attempt < 60)
            request.then(anchorScroll)
          else
            UI.view_loading = false
            $log.debug('ProfileController: Unable to find post id in feed so cannot scroll to it.')

      else
        $anchorScroll()
        UI.view_loading = false

    anchorScroll()

  # shows the bio of the user
  # @todo refactor this function and below's, is also in me_ctrl.js and can be changed by a 'switch' function
  $scope.view_bio = ->
    return unless user_profile.tagline or $scope.current_user
    $('.slider-window').scrollLeft(documentWidth)
    $scope.slide = 'bio'
    Mixpanel.startEvent('Bio Viewed')

    true

  $scope.view_edit = ->
    Navigation.go('meEdit')

  $scope.view = Navigation.go

  # shows the avatar of the user
  $scope.view_avatar = ->
    $('.slider-window').scrollLeft(0)
    $scope.slide = 'avatar'
    Mixpanel.bioViewed($scope.current_user)
    true


  $scope.bioInView = (inview) ->
    if $scope.slide == 'bio'
      if inview and not pageLoad
        Mixpanel.startEvent('Bio Viewed')
      else
        Mixpanel.bioViewed($scope.current_user)
    pageLoad = false

  $scope.settingsMenu = ->
    if $scope.current_user
      options =
        [
            label: I18nService.t('edit_profile_nav')
            handler: ->
              $scope.view('meEdit')
              MobileMenuService.close()
          ,
            label: I18nService.t('about_nav')
            handler: ->
              $scope.view('about')
              MobileMenuService.close()
          ,
            label: I18nService.t('signout_nav')
            handler: ->
              Authentication.signout()
              MobileMenuService.close()
        ]
      MobileMenuService.showCancel(true)
    else
      options =
        [
            label: if $scope.profile.muted then I18nService.t('unmute_nav') else I18nService.t('mute_nav')
            handler: ->
              # toggle mute user
              return if $scope.muting
              $scope.muting = true

              _toggle_mute = ->
                $scope.profile.muted = !$scope.profile.muted
                data =
                  config: 'general.errors'
                  meta:
                    title: if $scope.profile.muted then I18nService.t('user_muted_body') else I18nService.t('user_unmuted_body')
                $rootScope.$emit 'SHOW_MODAL', data

              if $scope.profile.muted
                _toggle_mute()
                request = UserService.unmute($scope.profile.id, true)
                request.error ->
                  _toggle_mute()

              else
                _toggle_mute()
                request = UserService.mute($scope.profile.id, true)
                request.error ->
                  _toggle_mute()

              request.finally ->
                $scope.muting = false
              MobileMenuService.close()
          ,
            label: if $scope.profile.blocked then I18nService.t('unblock_nav') else I18nService.t('block_nav')
            handler: ->
              # toggle block user
              return if $scope.blocking
              $scope.blocking = true
              if $scope.profile.blocked
                request = UserService.unblock($scope.profile.id, true)
                request.success ->
                  $scope.profile.blocked = false

              else
                request = UserService.block($scope.profile.id, true)
                request.success ->
                  $scope.profile.blocked = true

              request.finally ->
                $scope.blocking = false
                data =
                  config: 'general.errors'
                  meta:
                    title: if $scope.profile.blocked then I18nService.t('user_blocked_body') else I18nService.t('user_unblocked_body')
                $rootScope.$emit 'SHOW_MODAL', data

              MobileMenuService.close()
        ]

    MobileMenuService.setType('bottom')
    MobileMenuService.setOptions(options)
    MobileMenuService.show()

  $scope.goBack = Navigation.back

  postDeletionListener = $rootScope.$on 'POST.DELETED', (e, index) ->
    $scope.feed.splice index, 1

  $scope.$on '$destroy', ->
    contentLoadedListener()
    postDeletionListener()
    MobileMenuService.close()
    $timeout.cancel timer

    if $scope.current_user
      Mixpanel.myProfileViewed()
    else
      Mixpanel.userProfileViewed()
]

Unii.loadProfile = ['$q', 'UserService', '$route', ($q, UserService, $route) ->
  deferred = $q.defer()

  request = UserService.profile($route.current.params.id)

  request.then(
    (data) ->
      deferred.resolve(data)
    (message) ->
      deferred.reject(message)
    )

  deferred.promise
]

Unii.loadMe = ['$q', 'CurrentUser', ($q, CurrentUser) ->
  deferred = $q.defer()

  request = CurrentUser.get()

  request.then(
    (data) ->
      deferred.resolve(data)
    (message) ->
      deferred.reject(message)
    )

  deferred.promise
]
