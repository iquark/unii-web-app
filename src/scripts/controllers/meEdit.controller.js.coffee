Unii = angular.module 'Unii'

Unii.controller 'MeEditCtrl', ['$scope', 'I18nService', '$route', 'UI', '$log', 'Validation', 'CurrentUser', 'Navigation', 'Normalize', '$routeParams', '$rootScope', 'Util', ($scope, I18nService, $route, UI, $log, Validation, CurrentUser, Navigation, Normalize, $routeParams, $rootScope, Util) ->

  $scope.profile = $route.current.locals.profileData
  $scope.profileForm = CurrentUser.profileForm

  $scope.goBack = Navigation.back
  $scope.view = Navigation.go

  $scope.autoGrowTextArea = (event) ->
    options = minRows: 1
    $scope.remainingCharacters = Util.autoGrowTextArea(event.target, options)
    $scope.editForm.bio.$error.length = $scope.remainingCharacters < 0

  $scope.setUserColor = (color) ->
    return if color == $scope.profileForm.color.code
    color = Normalize.color(color)
    $scope.profileForm.color = color
    $scope.editForm.$setDirty(true)


  $scope.cancelEdit = ->

    if $scope.editForm.$dirty
      data = config: 'cancel.warning'
      $rootScope.$emit 'SHOW_MODAL', data, ->
        CurrentUser.resetProfileForm()
        Navigation.back()

    else
      Navigation.back()


  $scope.$on '$viewContentLoaded', ->
    CurrentUser.populateForm($scope.profile, $scope.editForm)
    # force textarea to be correct size on page load
    options =
      formContent: $scope.profileForm.tagline
      minRows: 1
    Util.autoGrowTextArea(angular.element('#bio-text')[0], options)
]
