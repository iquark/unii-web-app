###
# @desc Controller for the View Post page
# @file view.controller.js.coffee
###

(->

  viewController = ['$scope', 'Post', '$routeParams', 'UI', 'CurrentUser', '$interval', 'Comment', '$route', '$log', 'MediaService', 'Link', 'Mixpanel', 'Navigation', 'Notification', 'PostActionsService', '$rootScope', '$timeout',  ($scope, Post, $routeParams, UI, CurrentUser, $interval, Comment, $route, $log, MediaService, Link, Mixpanel, Navigation, Notification, PostActionsService, $rootScope, $timeout) ->

    Mixpanel.startEvent('Post Viewed')
  
    userSettingsListener = $rootScope.$on 'USER_SETTINGS', ->
      $scope.userSettings = $rootScope.userSettings
    $scope.userSettings = $rootScope.userSettings

    favouritesPagination       = null
    $scope.viewingFavourites   = false
    $scope.viewingComments     = false
    $scope.comment_form        = {}

    $scope.post = $route.current.locals.postData

    $scope.viewSlide = (post, index) ->
      Navigation.go('postSlides', {id: post.id}).hash(index+1)

    $scope.showMoreComments = !!$scope.post.comments.pagination.next_page or $scope.post.comments.data.length < $scope.post.comments_count


    $scope.setCommentHeight = ->
      hidden = angular.element(".hidden-textarea")
      commentTextarea = angular.element(".comment-form-text")
      commentTextarea.css 'height', hidden.height()
      true

    # check if are there notifications about someone commenting in this post, they will be marked as viewed
    Notification.markViewed($scope.post.id)

    contentLoadedListener = $scope.$on '$viewContentLoaded', ->

      switch $routeParams.state
        when 'comments'
          openComments = ->
          $timeout($scope.viewComments, 500, true)
        when 'favourites'
          $timeout($scope.openFavourites, 500)

    # retrieves the comments in a paginated mode
    $scope.fetchComments = (pagination) ->
      if pagination.next_page?
        $scope.loadingComments = true
        request = Comment.load pagination

        request.success (data) ->
          reversedComments = data.comments.data.reverse()

          # prepend new data to existing comments
          reversedComments.push.apply(reversedComments, $scope.post.comments.data)
          $scope.post.comments.data = reversedComments
          $scope.post.comments.pagination = data.comments.pagination
          $scope.showMoreComments = $scope.post.comments.data.length < $scope.post.comments_count

        request.error () ->
          $log.log "there was an error trying to get more comments..."

        request.finally ->
          $scope.loadingComments = false
      else
        $scope.showMoreComments = false


    $scope.viewComments = (open) ->
      $scope.viewingComments = if open then open else !$scope.viewingComments


    $scope.transitionComment = (comment) ->
      comment.user_favourited = !comment.user_favourited
      if comment.user_favourited
        comment.favourites_count += 1
        Comment
          .favourite($scope.post.id, comment.id)
          .success ->
            Mixpanel.commentFavourited()
          .error ->
            comment.user_favourited = false
            comment.favourites_count -= 1
      else
        comment.favourites_count -= 1
        Comment
          .unfavourite($scope.post.id, comment.id)
          .success ->
            Mixpanel.commentUnfavourited()
          .error ->
            comment.user_favourited = true
            comment.favourites_count += 1

    $scope.closeFavourites = ->
      $scope.viewingFavourites = false
      Mixpanel.favouritesScreenViewed($scope.post)

    $scope.openFavourites = ->
      $scope.viewingFavourites = true
      $scope.loadingFavourites = true
      Mixpanel.startEvent('Favourites Viewed')

      success = (data) ->
        $scope.favourites = data.favourites.data
        favouritesPagination = data.favourites.pagination

      error = (message) ->
        $log.error "postLikes: #{message}"

      updateUI = ->
        $scope.loadingFavourites = false

      Post.favourites($scope.post)
        .success(success)
        .error(error)
        .finally(updateUI)


      $scope.paginateFavourites = ->
        $scope.loadingFavourites = true

        success = (data) ->
          favouritesPagination = data.favourites.pagination
          $scope.favourites = $scope.favourites.concat data.favourites.data

        error = (message) ->
          $log.info "postLikesPagination: #{message}"
          $scope.endFavourites = true

        Post
          .paginateFavourites(favouritesPagination)
          .then(success)
          .catch(error)
          .finally(updateUI)

    # end openFavourites()

    $scope.externalLink = Link.external

    $scope.goBack = ->
      if $scope.comment_form.content
        data = config: 'cancel.comment'
        $rootScope.$emit 'SHOW_MODAL', data, ->
          $scope.comment_form = {}
          Navigation.back()
      else
        Navigation.back()

    $scope.createMenu = (event, index)->
      event.stopPropagation() if event

      PostActionsService.show($scope.post, angular.element('.content-wrapper'), index)
      return

    $scope.$on '$destroy', ->
      contentLoadedListener()
      userSettingsListener()
      PostActionsService.closeAll()
      Mixpanel.postViewed($scope.post)
  ]

  angular.module('Unii')
    .controller('ViewController', viewController)
    .loadPost = ['$q', 'Post', '$route', ($q, Post, $route) ->
      deferred = $q.defer()
      request = Post.get($route.current.params.id)

      request.then(
        (data) ->
          deferred.resolve(data)

        (message) ->
          deferred.reject(message)
        )

      deferred.promise
    ]
)()