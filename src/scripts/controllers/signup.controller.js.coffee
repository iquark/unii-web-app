Unii = angular.module 'Unii'

Unii.controller 'SignupCtrl', ['API_DOMAIN', '$log', 'Mixpanel', 'Navigation', 'Request', '$scope', (API_DOMAIN, $log, Mixpanel, Navigation, Request, $scope) ->

  Mixpanel.startEvent('Sign Up')
  $scope.view = Navigation.go

  _success = (data) ->
    domains = []
    for domain in data.domains.data
      domains.push domain.domain
    localStorage.setItem 'promo_domains', JSON.stringify domains

  _error = (data) ->
    $log.error('SignupCtrl: There was an error trying to get promo domains.', data)

  Request
    .send("http://#{API_DOMAIN}/api/v2/universities/domains/promo?limit=4000", "GET")
    .success(_success)
    .error(_error)



]
