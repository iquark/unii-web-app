###
# @desc Provider to send the errors to a service such as NewRelic or Bugsense (if in production)
# @file exceptionHandeler.provider.js.coffee
###

(->

  Handler = ->
    this.$get = ['$log', '$window', ($log, $window) ->
      _handler = (exception, cause) ->
        exception.message += ' (caused by "' + cause + '")' if cause?
        $log.error exception.message, exception.stack

      return _handler
    ]
    return

  angular.module('Unii')
    .provider({$exceptionHandler: Handler})
)()
