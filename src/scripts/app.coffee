
angular.module 'Unii', ['ngRoute', 'ngMessages', 'blueimp.fileupload', 'ngSanitize', 'Services', 'ngTouch', 'infinite-scroll', 'ipCookie', 'ngAnimate', 'unii-localization', 'angular-inview']

angular.module 'Unii'
  .config ['$routeProvider', '$compileProvider', '$sceDelegateProvider', '$provide', 'BrowserProvider', 'I18N_CONSTANT', ($routeProvider, $compileProvider, $sceDelegateProvider, $provide, BrowserProvider, I18N_CONSTANT) ->

    userLocale = I18N_CONSTANT().locale()


    $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|file|blob):|data:image\//)

    $sceDelegateProvider.resourceUrlWhitelist(['self', "https://s3-us-west-2.amazonaws.com/unii-unii-prod/**", "http://unii-web-staging-herokuapp-com.global.ssl.fastly.net", "http://unii-web-herokuapp-com.global.ssl.fastly.net", "http://unii-unii-dev.s3.amazonaws.com/"])

    if BrowserProvider.$get().isMobile()
      angular.element('.fix-orientation').removeClass 'hidden'
      $routeProvider
      .when '/', {
        templateUrl: ->
          '/views/'+userLocale+'/feed.html'
        reloadOnSearch: false
        controller: 'FeedController as vm'
      }
      .when '/signin', {
        templateUrl: ->
          '/views/'+userLocale+'/auth/signin.html'
        controller: 'SigninCtrl'
      }
      .when '/signup', {
        templateUrl: ->
          '/views/'+userLocale+'/auth/signup.html'
        controller: 'SignupCtrl'
      }
      .when '/challenges/:team', {
        templateUrl: ->
          '/views/'+userLocale+'/auth/signup.html'
        controller: 'SignupCtrl'
      }
      .when '/intro', {
        templateUrl: ->
          '/views/'+userLocale+'/intro.html'
      }
      .when '/write', {
        templateUrl: ->
          '/views/'+userLocale+'/post/compose.html'
        controller: 'PostsCtrl'
      }
      .when '/me', {
        templateUrl: ->
          '/views/'+userLocale+'/profile.html'
        controller: 'ProfileCtrl'
        reloadOnSearch: false
        resolve: {
          profileData: angular.module('Unii').loadMe
        }
      }
      .when '/me/avatar', {
        templateUrl: ->
          '/views/'+userLocale+'/me/edit_avatar.html'
        controller: 'AvatarController as vm'
      }
      .when '/me/edit', {
        templateUrl: ->
          '/views/'+userLocale+'/me/edit.html'
        controller: 'MeEditCtrl'
        resolve: {
          profileData: angular.module('Unii').loadMe
        }
      }
      .when '/me/edit/password', {
        templateUrl: ->
          '/views/'+userLocale+'/me/edit_password.html'
        controller: 'MeEditPasswordController'
        resolve: {
          profileData: angular.module('Unii').loadMe
        }
      }
      .when '/me/edit/course', {
        templateUrl: ->
          '/views/'+userLocale+'/me/edit_course.html'
        controller: 'MeEditCourseController'
        resolve: {
          profileData: angular.module('Unii').loadMe
        }
      }
      .when '/me/restricted', {
        templateUrl: ->
          '/views/'+userLocale+'/me/restricted_users.html'
        controller: 'MeRestrictedController'
      }
      .when '/activity', {
        templateUrl: ->
          '/views/'+userLocale+'/notifications.html'
        controller: 'ActivityController as vm'
        resolve: {
          profileData: angular.module('Unii').loadMe
        }
      }
      .when '/view/:id', {
        templateUrl: ->
          '/views/'+userLocale+'/post/view.html'
        controller: 'ViewController as vm'
        resolve: {
          postData: angular.module('Unii').loadPost
        }
      }
      .when '/view/:id/slides', {
        templateUrl: ->
          '/views/'+userLocale+'/photos/slideshow.html'
        controller: 'SlideshowCtrl'
        reloadOnSearch: false
        resolve: {
          resolvedData: angular.module('Unii').loadPost
        }
      }
      .when '/view/:id/images', {
        templateUrl: ->
          '/views/'+userLocale+'/photos/thumbs.html'
        controller: 'ImagesController as vm'
        resolve: {
          resolvedData: angular.module('Unii').loadPost
        }
      }
      .when '/profile/:id', {
        templateUrl: ->
          '/views/'+userLocale+'/profile.html'
        controller: 'ProfileCtrl'
        reloadOnSearch: false
        resolve: {
          profileData: angular.module('Unii').loadProfile
        }
      }
      .when '/profile/:id/images', {
        templateUrl: ->
          '/views/'+userLocale+'/photos/thumbs.html'
        controller: 'ImagesController as vm'
        resolve: {
          resolvedData: angular.module('Unii').loadProfile
        }
      }
      .when '/profile/:id/slides', {
        templateUrl: ->
          '/views/'+userLocale+'/photos/slideshow.html'
        controller: 'SlideshowCtrl'
        reloadOnSearch: false
        resolve: {
          resolvedData: angular.module('Unii').loadProfileImages
        }
      }
      .when '/pre-uni', {
        templateUrl: ->
          '/views/'+userLocale+'/auth/verification_wall.html'
        controller: 'PreUniCtrl'
        resolve: {
          profileData: angular.module('Unii').loadMe
        }
      }
      .when '/about', {
        templateUrl: ->
          '/views/'+userLocale+'/me/about.html'
        controller: 'StaticPagesController as vm'
      }
      .when '/eula', {
        templateUrl: ->
          '/views/'+userLocale+'/docs/eula.html'
        controller: 'StaticPagesController as vm'
      }
      .when '/support', {
        templateUrl: ->
          '/views/'+userLocale+'/me/forms/support.html'
        controller: 'StaticPagesController as vm'
      }
      .when '/privacy', {
        templateUrl: ->
          '/views/'+userLocale+'/docs/privacy.html'
        controller: 'StaticPagesController as vm'
      }
      .otherwise {
        redirectTo: '/'
      }
    else
      angular.element('.fix-orientation').addClass 'hidden'
      $routeProvider
      .when '/', {
        templateUrl: ->
          '/views/'+userLocale+'/desktop.html'
        reloadOnSearch: false
        controller: 'StaticPagesController as vm'
      }
      .when '/intro', {
        templateUrl: ->
          '/views/'+userLocale+'/desktop.html'
        reloadOnSearch: false
        controller: 'StaticPagesController as vm'
      }

    $provide.decorator '$templateRequest', ['$delegate', ($delegate) ->
      mySilentProvider = (tpl, ignoreRequestError) ->
        return $delegate(tpl, true)

      return mySilentProvider;
    ]

    $provide.decorator '$browser', ['$delegate', ($delegate) ->
      # This is required because of a known issue with Chrome for iOS.
      # Specifically, how it handles the second # within a URL.
      # For updates, see https://github.com/angular/angular.js/issues/7699
      originalUrl = $delegate.url
      $delegate.url = ->
        result = originalUrl.apply(this, arguments)
        if (result && result.replace)
          result = result.replace(/%23/g, '#')
        return result
      return $delegate
    ]


  ]
  .run ['CurrentUser', 'Feed', 'ipCookie', '$location', '$log', 'Navigation', 'Notification', 'Post', '$rootScope', 'UI', '$window', (CurrentUser, Feed, ipCookie, $location, $log, Navigation, Notification, Post, $rootScope, UI, $window)->
    # function to redirect the browser to an external link
    $rootScope.redirectTo = (url)->
      $window.location.href = url

    $rootScope.$on 'auth.signedin', (e, user) ->
      $log.log('Event: User has signed in.')
      UI.signedIn = true
      CurrentUser.clear()
      CurrentUser.store(user)
      Notification.start()
      CurrentUser.getCourses(user.university.id)

    $rootScope.$on 'auth.signout', ->
      $log.log('Event: User has signed out.')
      UI.signedIn = false
      ipCookie.remove 'token', {domain: $location.host()}
      CurrentUser.clear()
      Notification.stop()
      Post.clearCache()
      Feed.onWhatsHot(false)
      Navigation.go('intro')
  ]
