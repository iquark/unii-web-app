Unii = angular.module 'Unii'

Unii.service 'AssetsService', ->

  assetUrls = {
    imagePlaceholder: 'images/unii-icons/misc/image-placeholder.png'
    videoPlaceholder: 'images/unii-icons/misc/video-placeholder.png'
  }


  _getAsset = (name) ->
    assetUrls[name]

  {
    getAsset: _getAsset
  }