###
# @desc Service that handles API requests for emails
# @file email.service.js.coffee
###

(->

  Email = ['API_DOMAIN', 'Request', (API_DOMAIN, Request) ->

    _feedbackForm = (form) ->
      Request.send "http://#{API_DOMAIN}/feedback", 'POST', form

    # API
    {
      feedbackForm: _feedbackForm
    }

  ]

  angular.module('Unii')
    .service('Email', Email)
)()
