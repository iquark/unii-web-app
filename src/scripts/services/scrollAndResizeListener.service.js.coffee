###
# @desc Service that creates the listeners for Scroll and Resize events
# @file scrollAndResizeListener.service.js.coffee
###

(->

  scrollAndResizeListener = ['$window', '$document', '$timeout' , ($window, $document, $timeout) ->

    id = 0
    listeners = {}
    scrollTimeoutId = null
    resizeTimeoutId = null

    _invokeListeners = ->
      clientHeight = $document[0].documentElement.clientHeight
      clientWidth = $document[0].documentElement.clientWidth
      # call listener with given arguments

      listeners[key](clientHeight, clientWidth) for key of listeners

    $window.addEventListener 'resize', ->
      $timeout.cancel resizeTimeoutId
      resizeTimeoutId = $timeout _invokeListeners, 200

    $window.addEventListener 'scroll', ->
      # cancel previous timeout (simulates stop event)
      $timeout.cancel scrollTimeoutId
      # wait for 200ms and then invoke listeners (simulates stop event)
      scrollTimeoutId = $timeout _invokeListeners, 200

    _bindListener = (listener) ->
      index = ++id

      listeners[id] = listener

      return ->
        delete listeners[index]

    # API
    {
      bindListener: _bindListener
    }

  ]

  angular.module('Unii')
    .service('scrollAndResizeListener', scrollAndResizeListener)
)()