###
# @desc Service that gathers all the used validations along the page
# @file validation.service.js.coffee
###

(->

  Validation = () ->

    _regex =
      email: /[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/
      url: /(https?:\/\/)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)/
      name: /^[a-zA-Z\s]+$/

    _checkNameLength = (name, element)->

      names = if name then name.split(/\s/) else ['','']

      first_name = names.shift()
      last_name = names.join(' ')
      element.$error.length = (first_name.length > 12) or (last_name.length > 30)

      invalid = element.$invalid

      if !element.$error.length and name
        invalid = false

      invalid

    _checkLastName = (name, element)->

      names = if name then name.split(/\s/) else ['','']

      names.shift()
      last_name = names.join(' ')

      element.$error.lastname = !last_name.length

    _checkName = (name, element)->
      if name?
        element.$error.pattern = !name.match(_regex.name)
        element.$dirty = true
        element.$invalid = _checkNameLength(name, element) or _checkLastName(name, element) or !name.match(_regex.name)

    # API
    {
      checkNameLength: _checkNameLength
      checkLastName: _checkLastName
      checkName: _checkName
      regex: _regex
    }

  angular.module('Unii')
    .service('Validation', Validation)
)()
