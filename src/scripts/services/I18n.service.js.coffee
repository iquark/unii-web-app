

I18nService = ->
  i18n = I18n || {}
  locale = 'en-GB'

  locales = Object.keys(i18n) if angular.isObject(i18n)
  
  _locale = ->
    if navigator.language in locales
      locale = navigator.language
    locale

  _t = (key, variables) ->
    if i18n[locale]?[key]?
      string = i18n[locale][key]
      if variables
        for k, v of variables
          string = string.replace("%{#{k}}", v)
      string
      
    else
      key.replace('_', ' ').replace /(?:^|\s)\S/g, (a) -> a.toUpperCase()

  {
    locale: _locale
    t: _t
  }

angular.module('unii-localization', [])
  .service('I18nService', I18nService)

