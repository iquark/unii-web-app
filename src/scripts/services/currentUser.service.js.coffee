(->

  CurrentUser = ['API_DOMAIN', '$log', 'MediaService', 'Normalize', '$q', 'Request', '$rootScope', 'STATUSES', '$timeout', (API_DOMAIN, $log, MediaService, Normalize, $q, Request, $rootScope, STATUSES, $timeout) ->

    status = STATUSES.IDLE

    requiredSettings = ['id', 'color', 'avatar', 'name', 'first_name', 'last_name', 'flags', 'hot_feed_country']

    notifications = {
      data: []
      pagination: {}
    }


    # methods to store the user info
    _storage =
      engine: localStorage
      set: (key, attributes) ->
        try
          this.engine.setItem key, JSON.stringify attributes
        catch
          $log.log 'User in private browsing'
      get: (key) ->
        JSON.parse this.engine.getItem key
      remove: (key) ->
        this.engine.removeItem key

    _switchStorage = (local = true)->  # the default is localstorage
      # retrieve the possible info of the user
      currentUser = _storage.get('currentUser')
      _storage.remove('currentUser') if currentUser
      _storage.engine = if local then localStorage else sessionStorage
      # re-store the user info
      _storage.set('currentUser', currentUser) if currentUser

    _status = ->
      status

    _setStatus = (s) ->
      status = s

    _initialize = ->
      # app has been initialized. Ensure localStorage is being used.
      _switchStorage(true)
      _settings({})


    _state = ->
      currentUser = _storage.get('currentUser')
      currentUser?.verified_state || null

    _store = (attributes) ->
      # normalize current user and store in localStorage
      currentUser = Normalize.user(attributes)
      _storage.set('currentUser', currentUser)

      _settings(currentUser)

    _settings = (attributes) ->
      currentUser = _storage.get('currentUser')
      # check if there are settings for this user in localStorage
      localStorageUserSettings = _storage.get("user_#{currentUser.id}") || {}

      angular.forEach attributes, (value, key) ->
        localStorageUserSettings[key] = value if key in requiredSettings
      _storage.set("user_#{currentUser.id}", localStorageUserSettings)

      # make settings available to view.
      $rootScope.userSettings = localStorageUserSettings

      $rootScope.$emit 'USER_SETTINGS'


    _get = ->
      deferred = $q.defer()
      request = Request.send "http://#{API_DOMAIN}/api/v2/users/me", 'GET'

      request.success (data) ->
        deferred.resolve Normalize.user(data.user)
        _store(data.user)

      request.error ->
        deferred.reject 'error trying to get current user from API'

      deferred.promise

    _edit = (form) ->
      Request.send "http://#{API_DOMAIN}/api/v2/users/me", 'PUT', form


    _setAvatar = ->
      deferred = $q.defer()

      # upload to S3 storage
      # make API call and attach metadata to file
      MediaService.send()

      checkMediaService = ->
        if MediaService.status() == STATUSES.WORKING
          $timeout(checkMediaService, 1000)
        else
          try
            file = MediaService.mediaQueue()[0]
            form = {
              avatar: {
                url: file.metadata.details.url
              }
            }
            request = _edit(form)
            request.success (data) ->
              deferred.resolve(data)
            request.error ->
              deferred.reject('Error updating user avatar')
            request.finally ->
              MediaService.destroyForm()

          catch
            $log.error 'SetAvatar: There are no files in the media queue OR the metadata has not been attached to the file OR there is no network connection.'
            deferred.reject('Error trying to update user avatar')

      checkMediaService()

      deferred.promise

    _clear = ->
      _storage.remove()

    _notifications = ->
      Request.send "http://#{API_DOMAIN}/api/v2/notifications", 'GET', null, true

    # Currently not being called from anywhere
    _paginateNotifications = (pagination) ->
      Request.send pagination.next_page, 'GET'

    _markNotificationsAsViewed = (ids) ->
      request = Request.send "http://#{API_DOMAIN}/api/v2/notifications", 'PUT', {ids: ids}

      request.success (data) ->
        $log.log "marked notifications as viewed", data

    _resendConfirmationEmail = ->
      Request.send "http://#{API_DOMAIN}/api/v2/users/me/resend_confirmation_email", 'POST'

    _resetPassword = (email) ->
      Request.send "http://#{API_DOMAIN}/api/v2/users/me/password/forgot", 'POST', {email: email}

    _editPassword = (form) ->
      Request.send "http://#{API_DOMAIN}/api/v2/users/me/password", 'PUT', form

    _getCourses = (uniId) ->
      deferred = $q.defer()
      if _storage.get("#{uniId}_courses")
        deferred.resolve(_storage.get("#{uniId}_courses"))
      else

        _success = (data) ->
          _switchStorage(true) # ensure localStorage in being used
          _storage.set("#{uniId}_courses", data.courses.data)
          deferred.resolve(data.courses.data)

        _error = ->
          $log.error("CurrentUser: Error trying to get users courses")
          deferred.reject()

        Request
          .send("http://#{API_DOMAIN}/api/v2/universities/#{uniId}/courses?limit=2500", 'GET')
          .success(_success)
          .error(_error)

      deferred.promise

    _populateForm = (attributes, form) ->
      angular.forEach attributes, (value, key) ->
        if _profileForm.hasOwnProperty(key)
          form.$setDirty(true)
        else
          _profileForm[key] = value
          
    _resetProfileForm = ->
      angular.forEach _profileForm, (value, key) ->
        delete _profileForm[key] unless key == 'errors'

    _getUniversity = ->
      currentUser = _storage.get('currentUser')
      currentUser?.university || {name: '', name_short: ''}

    _profileForm = {
      errors: []
    }

    _setFlag = (name) ->
      if not $rootScope.userSettings.flags? or !($rootScope.userSettings.flags instanceof Array)
        $rootScope.userSettings.flags = []

      $rootScope.userSettings.flags.push(name)

      _settings $rootScope.userSettings

    _isFlagged = (name) ->
      name in $rootScope.userSettings.flags if $rootScope.userSettings?.flags? and $rootScope.userSettings.flags instanceof Array

    _restrictedUsers = ->
      Request.send("http://#{API_DOMAIN}/api/v2/users/me/restricted_users", 'GET')

    # API
    {
      status: _status
      state: _state
      setStatus: _setStatus
      settings: _settings
      get: _get
      getUniversity: _getUniversity
      store: _store
      edit: _edit
      editPassword: _editPassword
      setAvatar: _setAvatar
      switchStorage: _switchStorage
      getCourses: _getCourses
      clear: _clear
      notifications: _notifications
      paginateNotifications: _paginateNotifications
      markNotificationsAsViewed: _markNotificationsAsViewed
      resendConfirmationEmail: _resendConfirmationEmail
      resetPassword: _resetPassword
      initialize: _initialize
      profileForm: _profileForm
      populateForm: _populateForm
      resetProfileForm: _resetProfileForm
      setFlag: _setFlag
      isFlagged: _isFlagged
      restrictedUsers: _restrictedUsers
    }
  ]

  angular
    .module('Unii')
    .service('CurrentUser', CurrentUser)
)()
