###
# @desc Service that structures API requests
# @file request.service.js.coffee
###

(->

  Request = ['$http', 'ipCookie', 'TIMEOUT', '$rootScope', '$timeout', ($http, ipCookie, TIMEOUT, $rootScope, $timeout) ->

    connectionStatus = true
    timeout = null

    _send = (url, method, data, bg = false) ->
      request = $http
        method: method
        url: url
        timeout: TIMEOUT
        data: data
        headers:
          'Authorization' : 'Bearer ' + ipCookie('token') if ipCookie('token')

      request.error (data, status) ->
        if status == 0 and not bg
          if connectionStatus
            $rootScope.$emit 'TIMEOUT', true
            connectionStatus = false
            timeout = $timeout(
              ->
                connectionStatus = true
              , TIMEOUT)
        else
          if status == 401
            # user is already signed out
            $rootScope.$emit 'auth.signout'

      request.success ->
        unless connectionStatus and not bg
          $rootScope.$emit 'TIMEOUT', false
          $timeout.cancel(timeout)
          connectionStatus = true

    # API
    {
      send: _send
    }

  ]

  angular.module('Unii')
    .service('Request', Request)
)()
