###
# @desc Service that controls the notifications
# @file notification.service.js.coffee
###

(->
  Notification = ['CurrentUser', '$filter', 'INTERVAL_NOTIFICATION', 'Post', '$rootScope', '$timeout', 'UI', '$window', 'Request', '$log', (CurrentUser, $filter, INTERVAL_NOTIFICATION, Post, $rootScope, $timeout, UI, $window, Request, $log) ->

    _notifications = {
      data: []
      pagination: {}
    }

    lastOrientationSync  = 0
    sleepTime             = 30000 # 30 seconds
    notificationsTout = null
    confirmedEmpty = false
    notificationsPolled = false

    _empty = ->
      confirmedEmpty

    _setOrientation = ->
      lastOrientationSync = Date.now()
      $window.orientation = window.orientation || 0

    # retrieves the notifications from the server
    _start = ->
      # set screen orientation
      now = Date.now()
      if (now - lastOrientationSync) > sleepTime
        _setOrientation()

      # get notifications
      request = CurrentUser.notifications()
      request.success (data) ->
        notificationsPolled = true
        _notifications = data.notifications
        UI.new_notifications = 0
        confirmedEmpty = _notifications.data.length == 0

        angular.forEach(data.notifications.data, (notification) ->
          UI.new_notifications += 1 unless notification.is_viewed
          notification.user.name = $filter('capitalize')(notification.user.name)

          # cleans the cache for each element involved in the new notifications
          if not notification.is_viewed
            if notification.type is 'CommentPost'
              Post.free notification.payload.post_id
        )

        notificationsTout = $timeout(_start, INTERVAL_NOTIFICATION) if $rootScope.userSettings and $rootScope.userSettings.id

      request.error (data, status) ->
        # it will simply check if are there new notifications, if the current ones are empty it will blink from full to empty and full again
        notificationsTout = $timeout(_start, INTERVAL_NOTIFICATION) if status!=401 and $rootScope.userSettings and $rootScope.userSettings.id

    # stops checking the notifications
    _stop = ->
      $timeout.cancel(notificationsTout)

    # Searches all notifications that are type CommentPost and which ID is the given post ID
    _markViewed = (post_id) ->
      ids = []

      angular.forEach _notifications.data, (notification) ->
        if not notification.is_viewed
          if notification.type is 'CommentPost' and notification.payload.post_id is post_id
            ids.push notification.id
            UI.new_notifications -= 1

      CurrentUser.markNotificationsAsViewed ids if ids.length > 0

    _markAllViewed = ->
      ids = []

      angular.forEach(_notifications.data, (notification) ->
        unless notification.is_viewed
          notification.is_viewed = true
          ids.push notification.id
        )

      UI.new_notifications = 0

      CurrentUser.markNotificationsAsViewed(ids)


    _get = ->
      _notifications

    _check = ($scope) ->
      return false unless notificationsPolled
      notifications = _get()
      needUpdate = false

      # in case there are notifications and have the same length
      # the comparison must be performed
      if $scope.notifications and $scope.notifications.data and notifications and notifications.data and $scope.notifications.data.length == notifications.data.length
        data = notifications.data
        (
          needUpdate = true
          break  #once we know we need to update, stop looking for more
        ) for notification, idx in data when notification.id isnt $scope.notifications.data[idx].id
      else
        needUpdate = true


      if needUpdate
        $scope.notifications = notifications

        if _empty()
          $scope.no_notifications = true

      return true


    _paginate = ($scope) ->
      url = $scope.notifications.pagination.next_page

      _updateUI = ->
        $scope.paginating = false

      if url
        _success = (data) ->
          $scope.notifications.data = $scope.notifications.data.concat data.notifications.data
          $scope.notifications.pagination = data.notifications.pagination

        _error = ->
          $log.warn 'NotificationService: There was an error trying to paginate notifications'

        Request
          .send(url, 'GET')
          .success(_success)
          .error(_error)
          .finally(_updateUI)
      else
        _updateUI()
        $log.warn 'NotificationService: No more notifications to paginate'


    # API
    {
      start: _start,
      stop: _stop,
      get: _get
      empty: _empty
      markViewed: _markViewed
      markAllViewed: _markAllViewed
      check: _check
      paginate: _paginate
    }
  ]

  angular
    .module('Unii')
    .factory('Notification', Notification)

)()
