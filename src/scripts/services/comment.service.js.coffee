###
# @desc Service that handles API requests for the Comment Model
# @file comment.service.js.coffee
###

(->

  Comment = ['API_DOMAIN', 'Request', (API_DOMAIN, Request) ->

    _create = (postId, data) ->
      Request.send "http://#{API_DOMAIN}/api/v2/posts/#{postId}/comments", 'POST', data

    _favourite = (postId, commentId) ->
      Request.send "http://#{API_DOMAIN}/api/v2/posts/#{postId}/comments/#{commentId}/favourites", 'POST'

    _unfavourite = (postId, commentId) ->
      Request.send "http://#{API_DOMAIN}/api/v2/posts/#{postId}/comments/#{commentId}/favourites", 'DELETE'

    _load = (pagination) ->
      Request.send pagination.next_page, 'GET'


    # API
    {
      create: _create
      favourite: _favourite
      unfavourite: _unfavourite
      load: _load
    }

  ]

  angular.module('Unii')
    .service('Comment', Comment)
)()
