###
# @desc Service that calls the directive Mobile Menu and stores the data it accesses
# @file mobileMenu.service.js.coffee
###

(->

  MobileMenu = ['$compile', '$rootScope', '$window', ($compile, $rootScope, $window) ->
    menu = null
    showCancel = true
    internalScope = $rootScope.$new()
    options = []
    type = 'bottom' # types are 'bottom', 'top', 'under-header'

    _getOptions = ->
      options

    _setOptions = (opts) ->
      options = opts

    _setType = (value) ->
      type = value

    _getType = (value) ->
      type

    _close = ->
      if menu
        menu.remove()
        angular.element($window).unbind 'popstate', _close
        internalScope.$destroy()
        angular.element('.ng-view').removeClass 'blurred'
      menu = null

    _show = ->
      unless menu
        angular.element('.ng-view').addClass 'blurred'
        html = angular.element('<unii-mobile-menu></unii-mobile-menu>')
        view = angular.element(document.querySelector('body'))

        view.append(html)
        internalScope = $rootScope.$new()
        $compile(html)(internalScope)
        menu = html

        angular.element($window).bind 'popstate', _close
      true

    _showCancel = (value)->
      showCancel = value if value?
      showCancel

    # API
    {
      close: _close
      getOptions: _getOptions
      setOptions: _setOptions
      getType: _getType
      setType: _setType
      show: _show
      showCancel: _showCancel
    }
  ]

  angular.module('Unii')
    .service('MobileMenuService', MobileMenu)
)()
