###
# @desc Service that handles post creation
# @file postCreation.service.js.coffee
###

(->

  PostCreation = ['File', '$q', 'API_DOMAIN', '$compile', '$rootScope', '$log', 'Request', 'Post', 'MediaService', '$timeout', 'STATUSES', 'Validation', 'Mixpanel', (File, $q, API_DOMAIN, $compile, $rootScope, $log, Request, Post, MediaService, $timeout, STATUSES, Validation, Mixpanel) ->

    status = STATUSES.IDLE
    form = null

    # Sends or retries the new post
    _send = (_form, mediaQueue) ->
      _setStatus(STATUSES.WORKING)

      MediaService.send()

      form = _form || form
      mediaQueue = mediaQueue || MediaService.mediaQueue()

      deferred = $q.defer()

      # If is successful will clean all about media files
      _createPost = ->
        Mixpanel.createPost(form)
        request = Request.send("http://#{API_DOMAIN}/api/v2/posts", 'POST', form)
        request.success (data) ->
          if data.meta?.job_url?
            _setStatus(STATUSES.PROCESSING)
            attempt = 0
            checkJob =  ->
              Request.send data.meta.job_url, 'GET'
                .success (info) ->
                  $log.debug 'checking job', info
                  if info.job?.status? and info.job.status == 'complete' or attempt >= 600 #limit to 10 minutes
                    Request.send("http://#{API_DOMAIN}/api/v2/posts/#{data.post.id}", 'GET')
                      .success (post) ->
                        _jobComplete(post)
                      .error ()->
                        _jobComplete(data)
                  else
                    attempt++
                    timer = $timeout(checkJob, 1000)
                .error ->
                  # if is there any error asking for the job that is processing
                  # the media, the post is directly shown
                  _jobComplete(data)

            checkJob()
          else
            _jobComplete(data)

        request.error ->
          _setStatus(STATUSES.ERROR)
          deferred.reject('There was an error trying to create post')

      # Checks if all files are resolved, if any file fails or for any cause
      # there is no form it will fail
      _checkErrors = ->
        if form?
          form.medias = []
          angular.forEach mediaQueue, (file) ->
            $log.debug 'Checking if file is resolved', file, file.$state()
            form.medias.push file.metadata.details if file.$state() == 'resolved' and file.metadata?

          if mediaQueue.length == form.medias.length
            _createPost()
          else
            _setStatus(STATUSES.ERROR)
            deferred.reject('Some media failed to upload')
        else
          deferred.reject('There is no form')


      # Displays post in feed
      # Removes form from view template and temp local variable
      # Returns promise to _send() call
      # Resets service status to idle
      _jobComplete = (data) ->
        data.post.medias[0].url = form.medias[0].url if form?.medias?[0].url and data.post.type == 'link' and !data.post.medias[0].url
        Post.addToFeed(data.post)
        MediaService.destroyForm()
        deferred.resolve()
        _setStatus(STATUSES.IDLE)
        form = null


      # If there are no media files, the post will check if a link is present and
      # will be directly created.
      # If there are media files, will wait until they are sent, after check
      # that everything is crrect, will start creating the post
      _checkMediaService = ->
        if mediaQueue.length
          if MediaService.status() == STATUSES.WORKING
            $timeout(_checkMediaService, 1000)
          else if MediaService.status() == STATUSES.ERROR
            _setStatus(STATUSES.ERROR)
            deferred.reject('There was an error with the MediaService')
          else
            _checkErrors()
        else
          # TODO check if are there links
          if form.content and link = form.content.match(Validation.regex.url)
            form.medias = [{type: 'Link', url: link[0]}]

            MediaService.parseLink(form.content)
              .then (data) ->
                if form
                  form.medias = [{id: data.data.link.id}]
                else
                  $log.info "PostCreation: MediaService has finished parsing link. However, the form not longer exists."
                _createPost()
              , ->
                _setStatus(STATUSES.ERROR)
          else
            _createPost()

      _checkMediaService()

      deferred.promise


    _status = ->
      status

    _setStatus = (newStatus) ->
      status = newStatus

    _cancel = ->
      MediaService.destroyForm()
      _setStatus(STATUSES.IDLE)
      form = null

    # API
    {
      send: _send
      status: _status
      cancel: _cancel
    }

  ]

  angular.module('Unii')
    .service('PostCreation', PostCreation)
)()
