###
# @desc Service that handles media uploading/viewing
# @file media.service.js.coffee
###


(->

  Media = ['Request', 'File', 'API_DOMAIN', '$compile', '$rootScope', '$q', '$log', 'STATUSES', 'UI', 'Validation', 'Link', '$interval', 'I18nService', (Request, File, API_DOMAIN, $compile, $rootScope, $q, $log, STATUSES, UI, Validation, Link, $interval, I18nService) ->

    form = null
    internalScope = $rootScope.$new()
    mediaQueue = []
    dataQueue = []
    auxQueue = []
    status = STATUSES.IDLE
    callbacks = {}
    profileImages = {}
    albumImages = {}

    _status = ->
      status

    _setStatus = (new_status) ->
      status = new_status

    _addToQueue = (data) ->
      mediaQueue.push data.files[0]
      dataQueue.push data
      $log.debug mediaQueue, dataQueue

    _mediaQueue = ->
      mediaQueue

    _removeFromMediaQueue = (index) ->
      mediaQueue.splice index, 1
      dataQueue.splice index, 1

    _clearQueues = ->
      mediaQueue = []
      dataQueue = []

    _createForm = (force = false, onlyImages = false) ->
      # the user can wish to be sure the form is regenerated
      # for example when selecting an avatar
      if force
        _destroyForm()

      unless form
        html = angular.element('<unii-media-form></unii-media-form>')
        view = angular.element(document.querySelector('body'))

        view.append(html)
        internalScope = $rootScope.$new()
        $compile(html)(internalScope)
        form = html

      if onlyImages
        form.find('input#image-file')[0].click()
      else
        form.find('input[type=file]')[0].click()
      true

    _destroyForm = ->
      if form
        form.remove()
        internalScope.$destroy()
      form = null
      _clearQueues()
      _setCallbacks({})
      _setStatus(STATUSES.IDLE)


    _addSingle = (file_ext) ->
      deferred = $q.defer()

      request = Request.send "http://#{API_DOMAIN}/api/v2/uploads", 'POST', {extension: file_ext}

      request.success (data) ->
        deferred.resolve(data)
      request.error ->
        deferred.reject("I don't have the power captain")


      deferred.promise


    _convertImage = (file) ->

      deferred = $q.defer()

      _base64ToBlob = (data) ->
        byteString = atob data.split(',')[1]
        arrayBuffer = new ArrayBuffer byteString.length
        intArray = new Uint8Array arrayBuffer

        for i in [0..byteString.length]
          intArray[i] = byteString.charCodeAt i

        new Blob [intArray], { type: 'image/jpeg' }

      reader = new FileReader()
      img = new Image()

      reader.onloadend = ->
        img.src = reader.result

      reader.readAsDataURL(file)

      try
        img.onload = ->

          canvas = document.createElement('canvas')
          ctx = canvas.getContext('2d')

          height = img.height
          width = img.width

          canvas.height = height
          canvas.width = width

          ctx.drawImage(img, 0, 0)

          convertedImage = _base64ToBlob(canvas.toDataURL('image/jpeg'))

          # convertedImage.details = media_details[id].details

          convertedImage.metadata = file.metadata if file.metadata

          deferred.resolve(convertedImage)
      catch
        deferred.reject('Could not convert image')

      deferred.promise


    _send = ->
      auxQueue = dataQueue.slice(0)
      $rootScope.$broadcast 'fileretry', mediaQueue, auxQueue

    _setCallbacks = (_callbacks) ->
      callbacks = _callbacks

    _getCallback = (name) ->
      if callbacks[name]
        callbacks[name]
      else
        ->
          null

    # if inside the text a link appears, tries to load it
    _parseLink = (text) ->
      UI.getting_metadata = true
      deferred = $q.defer()
      _setStatus(STATUSES.WORKING)

      createMediaObject = (text) ->
        if Validation.regex.url.test(text)
          link = text.match(Validation.regex.url)[0]
          $log.info link, text.match(Validation.regex.url)
          Link.load(link)
        else
          deferred.reject("No links found....")
          UI.getting_metadata = false
          _setStatus(STATUSES.IDLE)
          deferred.promise

      # gets the job url to be polling when the data retrieval finishes
      pollApiForStatus = (mediaObject) ->
        $log.debug 'polling', mediaObject

        jobUrl = mediaObject.data.meta.job_url
        linkId = mediaObject.data.link.id

        # 100/30 is 3.3, but if it reaches the 30 the 'Posting' banner will disappear
        progress = 3.5
        # checks the job getting the metadata
        pollURL = ->
          $('.progress-bar').css 'width', "#{progress}%"
          progress += 3.5
          Link.poll(jobUrl)
          .success (data) ->
            if data.job.status == 'complete'
              $log.debug 'complete!', data
              $interval.cancel(poll)
            else if data.job.status == 'failed'
              $log.debug 'failed!', data
              $interval.cancel(poll)
              _setStatus(STATUSES.ERROR)

        poll = $interval(pollURL, 1000, 30)
        poll.then(
          (data) ->
            UI.getting_metadata = false
            $log.debug 'API did not complete metadata search..', data

            _setStatus(STATUSES.ERROR)
            deferred.reject('API did not complete metadata search..')
           ->
            $log.info 'finished'
            deferred.resolve linkId
        )

        deferred.promise

      # once finished returns the metadata
      getMetaData = (linkId) ->
        $log.debug 'get metadata of '+linkId
        request = Link.getMetadata(linkId)
        request.error ->
          _setStatus(STATUSES.ERROR)
        request.success ->
          _setStatus(STATUSES.IDLE)
          $('.progress-bar').css 'width', "100%"

      createMediaObject(text).then(pollApiForStatus).then(getMetaData)


    _cachedImages = (id) ->
      if profileImages[id]
        profileImages[id].data
      else
        []

    _cacheImages = (id, data) ->
      profileImages[id].data = profileImages[id].data.concat data.images.data
      profileImages[id].pagination = data.images.pagination
      profileImages[id].meta = data.meta

    _getImages = (id) ->
      deferred = $q.defer()

      if profileImages[id]
        deferred.resolve(profileImages[id])
      else
        profileImages[id] = {
          data: []
        }
        request = Request.send "http://#{API_DOMAIN}/api/v2/users/#{id}/images?limit=15", 'GET'

        request.success (data) ->
          _cacheImages(id, data)
          deferred.resolve(profileImages[id])

        request.error ->
          deferred.reject("MediaService: There was an error trying to get profile images.")

      deferred.promise


    _paginateImages = (id) ->
      deferred = $q.defer()

      if profileImages[id]?.pagination?.next_page?
        url = profileImages[id].pagination.next_page
        request = Request.send url, 'GET'

        request.success (data) ->
          if !!url.match(/posts/)
            _cacheAlbum(id, data)
          else
            _cacheImages(id, data)
          deferred.resolve(profileImages[id])

        request.error ->
          deferred.reject("MediaService: There was an error trying to paginate profile images.")

      else
        deferred.reject(I18nService.t('end_pagination_photos_nav'))


      deferred.promise

    # ALBUMS
    _cacheAlbum = (id, data) ->
      if not profileImages[id]
        profileImages[id] = {
          data: []
        }
      profileImages[id].data = profileImages[id].data.concat data.medias.data
      profileImages[id].pagination = data.medias.pagination

    # API
    {
      addSingle: _addSingle
      addToQueue: _addToQueue
      convertImage: _convertImage
      createForm: _createForm
      destroyForm: _destroyForm
      getCallback: _getCallback
      mediaQueue: _mediaQueue
      parseLink: _parseLink
      removeFromMediaQueue: _removeFromMediaQueue
      send: _send
      setCallbacks: _setCallbacks
      setStatus: _setStatus
      status: _status
      cachedImages: _cachedImages
      cacheAlbum: _cacheAlbum
      getImages: _getImages
      paginateImages: _paginateImages
    }


  ]

  angular.module('Unii')
    .service('MediaService', Media)
)()
