###
# @desc Service that calls the directive with the menu for Post Actions
# @file postActions.service.js.coffee
###

(->

  postActions = ['$compile', '$rootScope', ($compile, $rootScope) ->
    menu = {}
    internalScope = {}

    _closeAll = ->
      for postId of menu
        internalScope[postId].$destroy()
        menu[postId].remove()
        delete internalScope[postId]
        delete menu[postId]

    _close = (postId)->
      if menu[postId]
        internalScope[postId].$destroy()
        menu[postId].remove()
        delete menu[postId]
        delete internalScope[postId]

    _show = (post, element, index)->
      unless !post or menu[post.id]
        html = angular.element("<unii-post-actions class=\"actions-modal\" id=\"actions-modal-#{post.id}\" ng-click=\"destroyMenu($event)\" index=\"#{index}\" ng-cloak></unii-post-actions>")
        element.prepend(html)
        internalScope[post.id] = $rootScope.$new()
        $compile(html)(internalScope[post.id])
        internalScope[post.id].post = post
        menu[post.id] = html
      
      true

    # API
    {
      closeAll: _closeAll
      close: _close
      show: _show
    }
  ]

  angular.module('Unii')
    .service('PostActionsService', postActions)
)()