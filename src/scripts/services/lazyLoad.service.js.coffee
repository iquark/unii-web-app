###
# @desc Service that handles the lazy loading of images
# @file lazyLoad.service.js.coffee
###

(->

  LazyLoad = ['$document', 'scrollAndResizeListener', ($document, scrollAndResizeListener) ->

    _trigger = ->
      if document.createEvent
        event = document.createEvent("HTMLEvents")
        event.initEvent("resize", true, true)
      else
        event = document.createEventObject()
        event.eventType = "resize"
      event.eventName = "resize"
      if document.createEvent
        window.dispatchEvent(event)
      else
        window.fireEvent("on" + event.eventType, event)

    _load = ($element, callback) ->
      return if not callback?

      listenerRemover = null

      $element.addClass 'bg-dynamic'

      isInView = (clientHeight, clientWidth) ->
        # get element position
        imageRect = $element[0].getBoundingClientRect()

        # increment the bounding box by using offsets to show whether partially shown or near to be shown images
        # increases to the double
        offsetHeight = clientHeight
        offsetWidth = clientWidth

        if (imageRect.top >= 0-offsetHeight && imageRect.bottom <= clientHeight+offsetHeight) and (imageRect.left >= 0-offsetWidth && imageRect.right <= clientWidth+offsetWidth)
          callback()

          # unbind event listeners when image src has been set
          listenerRemover()

      # bind listener
      listenerRemover = scrollAndResizeListener.bindListener(isInView)

      # unbind event listeners if element was destroyed
      # it happens when you change view, etc
      $element.on '$destroy', ->
        listenerRemover()

      # explicitly call scroll listener (because, some images are in viewport already and we haven't scrolled yet)
      isInView(
        $document[0].documentElement.clientHeight,
        $document[0].documentElement.clientWidth
      )


    # API
    {
      trigger: _trigger
      load: _load
    }

  ]

  angular.module('Unii')
    .factory('LazyLoad', LazyLoad)
)()