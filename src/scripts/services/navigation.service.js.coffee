###
# @desc Service that handles app navigation
# @file navigation.service.js.coffee
###

(->

  Navigation = ['$rootScope', 'ipCookie', '$location', '$log', ($rootScope, ipCookie, $location, $log) ->

    navHistory = []
    leafBlackList = ['/about']
    level = 1
    ERRORS = {
      ID: 'Navigation: Must provide ID parameter.'
    }

    _go = (path, params) ->
      paths[path](params)

    _leaf = ->
      navHistory.pop() unless $location.$$path in leafBlackList

    _back = ->
      if navHistory[navHistory.length-1] == $location.$$path
        navHistory.pop()
        _back()
      else
        last_path = navHistory.pop() || if ipCookie('token') then '/' else '/signup'
        _go_to(last_path).hash('').search({})

    _history = (value) ->
      if typeof value == 'number'
        navHistory[navHistory.length-value] || 'Error'
      else
        navHistory.push value

    _go_to = (path) ->
      $location.path(path).replace()

    paths =

      activity: ->
        _go_to('/activity')

      about: ->
        _go_to('/about')

      compose: ->
        _go_to('/write')

      eula: ->
        _go_to('/eula')

      feed: ->
        _go_to('/')

      intro: ->
        _go_to('/intro')

      me: ->
        _go_to('/me')

      meEdit: ->
        _go_to('/me/edit')

      meEditPassword: ->
        _go_to('/me/edit/password')

      meEditCourse: ->
        _go_to('/me/edit/course')

      meAvatarPreview: ->
        _go_to('/me/avatar')

      meRestricted: ->
        _go_to('/me/restricted')

      post: (params) ->
        if params.hasOwnProperty('id')
          _go_to("/view/#{params.id}")
        else
          $log.error ERRORS.ID
          $location

      postImages: (params) ->
        if params.hasOwnProperty('id')
          _go_to("/view/#{params.id}/images")
        else
          $log.error ERRORS.ID
          $location

      postSlides: (params) ->
        if params.hasOwnProperty('id')
          _go_to("/view/#{params.id}/slides")
        else
          $log.error ERRORS.ID
          $location

      preUni: ->
        _go_to('/pre-uni')

      privacy: ->
        _go_to('/privacy')

      profile: (params) ->
        if params.hasOwnProperty('id')
          if params.id == $rootScope.userSettings.id
            this.me(params)
          else
            _go_to("/profile/#{params.id}")
        else
          $log.error ERRORS.ID
          $location

      profileImages: (params) ->
        if params.hasOwnProperty('id')
          _go_to("/profile/#{params.id}/images")
        else
          $log.error ERRORS.ID
          $location

      profileSlides: (params) ->
        if params.hasOwnProperty('id')
          _go_to("/profile/#{params.id}/slides")
        else
          $log.error ERRORS.ID
          $location

      signIn: ->
        _go_to('/signin')

      signUp: ->
        _go_to('/signup')

      support: ->
        _go_to('/support')

    
    # API
    {
      go: _go
      back: _back
      history: _history
      leaf: _leaf
    }

  ]

  angular.module('Unii')
    .service('Navigation', Navigation)
)()