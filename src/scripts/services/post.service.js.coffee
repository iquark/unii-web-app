###
# @desc Service that handles API requests for the Post Model
# @file post.service.js.coffee
###

(->

  Post = ['$rootScope', '$q', '$interval', 'UI', 'API_DOMAIN', 'Request', 'CurrentUser', '$log', 'Navigation', 'Link', 'Mixpanel', 'Feed', ($rootScope, $q, $interval, UI, API_DOMAIN, Request, CurrentUser, $log, Navigation, Link, Mixpanel, Feed) ->

    paginatingFeed = false
    PAGINATION_CACHE_BUFFER = 10

    scrollIds =
      feed: null
      whatsHot: null
      profile: null


    centralCache = []
    centralCacheIndex = {}

    cache =
      feed:
        data: []
        pagination: {}
      whatsHot:
        data: []
        pagination: {}
      posts: {}

    # favourites by post
    favourites = {}


    _updateCaches = (post, referrer, favourite) ->

      switch referrer
        when 'profile'
          _updatePostCache(post, favourite)
          _updateCentralCache(post, favourite)

        when 'post'
          _updateCentralCache(post, favourite)
          _freeProfileFromCache(post)

        when 'feed'
          _updatePostCache(post, favourite)
          _freeProfileFromCache(post)

        when 'whatsHot'
          _updatePostCache(post, favourite)
          _freeProfileFromCache(post)

        when 'create_post'
          _freeProfileFromCache(post)

    _clearFeedCache = ->
      $log.debug 'emptying the feed cache...'
      cache.feed.data = []
      cache.feed.pagination = {}

    _clearWhatsHotCache = ->
      $log.debug 'emptying the What\'s Hot cache...'
      cache.whatsHot.data = []
      cache.whatsHot.pagination = {}

    _freeProfileFromCache = (post) ->
      # Clear cached profiles
      if $rootScope.userSettings.id == post.user.id
        CurrentUser.clear()

    _updateCentralCache = (post, favourite) ->

      if centralCacheIndex[post.id] >= 0
        post = centralCache[centralCacheIndex[post.id]]
        if favourite
          post.user_favourited = true
          post.favourites_count += 1
        else
          post.user_favourited = false
          post.favourites_count -= 1

    _updatePostCache = (post, favourite) ->
      if cache.posts[post.id]
        if favourite
          cache.posts[post.id].favourites_count += 1
          cache.posts[post.id].user_favourited = true
        else
          cache.posts[post.id].favourites_count -= 1
          cache.posts[post.id].user_favourited = false

    # a user favourites a post
    _favourite = (post, referrer, index) ->
      post.user_favourited = true
      post.favourites_count += 1

      Request
        .send "http://#{API_DOMAIN}/api/v2/posts/#{post.id}/favourites", 'POST'
        .success ->
          _updateCaches(post, referrer, true)
          Mixpanel.postFavourited(post, referrer)
        .error ->
          post.user_favourited = false
          post.favourites_count -= 1


    # a user unfavourites a post
    _unfavourite = (post, referrer) ->
      post.user_favourited = false
      post.favourites_count -= 1

      Request
        .send "http://#{API_DOMAIN}/api/v2/posts/#{post.id}/favourites", 'DELETE'
        .success ->
          _updateCaches(post, referrer, false)
          Mixpanel.postUnfavourited(post, referrer)
        .error ->
          post.user_favourited = true
          post.favourites_count += 1


    _get = (postId) ->
      deferred = $q.defer()

      if cache.posts[postId]
        deferred.resolve(cache.posts[postId])
      else
        request = Request.send "http://#{API_DOMAIN}/api/v2/posts/#{postId}", 'GET'

        request.success (data) ->
          # comments are reverted because they are coming in descendant chronological order (latest first)
          data.post.comments.data = data.post.comments.data.reverse()
          cache.posts[data.post.id] = data.post
          # updates the post in the feed (if exists)
          post.comments_count = cache.posts[data.post.id].comments_count for post in cache.feed.data when post.id is data.post.id
          deferred.resolve(data.post)

        request.error ->
          deferred.reject("there was an error getting post")

      # return promise
      deferred.promise


    _indexPosts = ->
      for post, i in centralCache
        centralCacheIndex[post.id] = i

    _filterPosts = (posts) ->
      # will remove invite posts
      post for post in posts when post.id

    _cachePosts = (posts) ->
      for post in posts
        centralCache.push post unless centralCacheIndex[post.id]
      _indexPosts()

    _getPostIDs = (posts) ->
      postIDs = []
      for post in posts
        postIDs.push post.id
      postIDs

    _getPostsFromCache = (ids) ->
      posts = []
      for id in ids
        posts.push centralCache[centralCacheIndex[id]]
      posts

    _initializeFeed = (whatsHotFeed, country) ->
      deferred = $q.defer()
      if whatsHotFeed
        selectedFeed = 'whatsHot'
        url = "http://#{API_DOMAIN}/api/v2/posts/hot"+(if country then "?hot_feed_country=#{country}" else "")
      else
        selectedFeed = 'feed'
        url = "http://#{API_DOMAIN}/api/v2/posts"

      _clearWhatsHotCache() if country

      if cache[selectedFeed].data.length
        # Initialize feed with only 5 posts...
        cachedPostIDs = cache[selectedFeed].data.slice(0, 5)
        deferred.resolve(_getPostsFromCache(cachedPostIDs))
      else
        _success = (data) ->
          posts = _filterPosts(data.posts.data)
          _cachePosts(posts)
          cache[selectedFeed].data = _getPostIDs(posts)
          cache[selectedFeed].pagination = data.posts.pagination
          deferred.resolve(posts)

        _error = ->
          deferred.reject "Error trying to initialize feed"

        Request
          .send(url, 'GET')
          .success(_success)
          .error(_error)

      deferred.promise


    # Should return a promise to deliver next page of posts.
    # IF posts are available in the cache then it should return the next
    # 5 from there. IF cache is empty or near the end, the API
    # should be called.
    _paginateFeed = (whatsHotFeed, existingFeed) ->
      deferred = $q.defer()
      selectedFeed = if whatsHotFeed then 'whatsHot' else 'feed'

      _callAPI = (returningPosts) ->

        # Block API call if no internet connection
        if $rootScope.noInternet
          deferred.reject('PostService: Not calling API as no internet.')
          return

        _success = (data) ->
          # Remove invite posts
          posts = _filterPosts(data.posts.data)

          # Add these post to the central cache
          _cachePosts(posts)

          # Get IDs from new posts returned from API
          newIDs = _getPostIDs(posts)

          # Add new post IDs to the selected feed cache
          newPostIDs = cache[selectedFeed].data = cache[selectedFeed].data.concat newIDs

          # Store the next page pagination details for selected feed
          cache[selectedFeed].pagination = data.posts.pagination

          # The cache was empty when call was made.
          # Must return the posts in additon to caching them.
          if returningPosts

            # Retrieve posts from the central cache
            posts = _getPostsFromCache(newPostIDs)

            # Return extracted posts as part of the promise.
            deferred.resolve(posts)
          

        _error = ->
          # false indicates API has not reached the end of posts available.
          deferred.reject "PostService: There was an error trying to paginate feed."

        _notBusy = ->
          paginatingFeed = false

        url = cache[selectedFeed].pagination.next_page

        if url and not paginatingFeed
          paginatingFeed = true
          Request
            .send(url, 'GET')
            .success(_success)
            .error(_error)
            .finally(_notBusy)

        else
          # true indicates API has reached the end of posts available.
          deferred.reject 'PostService: API has no more posts.'


      # Try and provide next 5 posts from cache if possible
      sliceAmount = Math.min(5, cache[selectedFeed].data.length-existingFeed.length)
      if sliceAmount < 1
        $log.log 'PostService: Feed is already showing all posts in the cache. Calling API...'
        _callAPI(true)
      else
        # exisitngFeedIDs = [1,2,3,4]
        existingFeedIDs = _getPostIDs(existingFeed)

        # Return next slice of cached post IDs
        # cachedPostIDs = [1,2,3,4,5,6,7,8,9,10,11,12]
        # sliceAmount = 5
        # return [5,6,7,8,9]
        cachedPostIDs = cache[selectedFeed].data.slice(existingFeed.length, existingFeed.length+sliceAmount)

        # concat existing feed to next slice
        # return [1,2,3,4,5,6,7,8,9]
        newPostIDs = existingFeedIDs.concat cachedPostIDs

        # Retrieve posts from the central cache
        posts = _getPostsFromCache(newPostIDs)

        # resolve promise and return the new feed
        deferred.resolve(posts)

        # Call API if there are less than 10 posts remaining in the cache
        if (centralCache.length-newPostIDs.length) < PAGINATION_CACHE_BUFFER
          $log.log "PostService: There are less than #{PAGINATION_CACHE_BUFFER} posts remaining in the cache. Calling API..."
          _callAPI(false)


      # complete promise of next page of posts.
      deferred.promise

    _cache = (post) ->
      return if cache.posts[post.id]
      cache.posts[post.id] = post

    _free = (postId) ->
      if cache.posts[postId]?
        delete cache.posts[postId]

    _newComment = (postId) ->
      i = centralCacheIndex[postId]
      if (typeof i == 'number') && (i>=0)
        centralCache[i].comments_count += 1

    _view = (postId, index, options) ->
      Feed.currentPostIndex(index)
      if options
        options.event.stopPropagation() if options.event
        _setScrollId(postId, options.referrer)
      state = options?.state || null
      Navigation.go('post', {id: postId}).hash('').search({state: state})

    _favourites = (post) ->
      Request.send "http://#{API_DOMAIN}/api/v2/posts/#{post.id}/favourites", 'GET'

    _paginateFavourites = (pagination) ->

      deferred = $q.defer()

      # set url
      url = pagination.next_page

      if url
        request = Request.send url, 'GET'
        request.success (data) ->
          deferred.resolve(data)
        request.error ->
          deferred.reject('Error trying to load favourites')
      else
        deferred.reject('No more favourites.')

      deferred.promise


    _addToFeed = (post) ->

      _updateUI = ->
        cache.feed.data.unshift post.id if cache.feed.data.length
        centralCache.unshift post
        _indexPosts()
        $rootScope.$emit 'POST.NEW', post


      if post.medias?[0]?.type == 'Link'
        Link.getMetadata(post.medias[0].id)
          .success (data) ->
            if data.link.metadata
              # attach media object to post form
              post.medias[0] = {id: data.link.id, metadata: data.link.metadata, url: post.medias?[0]?.url}
              post.medias[0].url = post.medias[0].url if post.medias?[0]?.url?
          .finally (data) ->
            _updateUI()
      else
        _updateUI()


    _destroy = (post, index) ->
      request = Request.send "http://#{API_DOMAIN}/api/v2/posts/#{post.id}", 'DELETE'

      request.success ->
        centralCache.splice centralCacheIndex[post.id], 1 if centralCacheIndex[post.id]
        delete cache.posts[post.id]
        _indexPosts() # change has been made to cache, MUST re-index.

        # Remove Post ID from any cached feeds it may be in.
        if cache.whatsHot.data.indexOf(post.id) >= 0
          cache.whatsHot.data.splice cache.whatsHot.data.indexOf(post.id), 1
        if cache.feed.data.indexOf(post.id) >= 0
          cache.feed.data.splice cache.feed.data.indexOf(post.id), 1

        # Broadcast event so view controller can remove post from the view.
        $rootScope.$emit 'POST.DELETED', index

      request

    _report = (post) ->
      Request.send "http://#{API_DOMAIN}/api/v2/posts/#{post.id}/report", 'POST'

    _scrollId = (referrer) ->
      id = scrollIds[referrer]
      delete scrollIds[referrer]
      id

    _setScrollId = (id, referrer) ->
      scrollIds[referrer] = id

    _clearScrollIds = () ->
      scrollIds =
        feed: null
        whatsHot: null
        profile: null

    _feed = ->
      if Feed.onWhatsHot()
        cache.whatsHot.data
      else
        cache.feed.data

    # API
    {
      get: _get
      clearCache: _clearFeedCache
      clearCacheWhatsHot: _clearWhatsHotCache
      clearScrollIds: _clearScrollIds
      cache: _cache
      free: _free
      newComment: _newComment
      favourite: _favourite
      unfavourite: _unfavourite
      view: _view
      favourites: _favourites
      paginateFavourites: _paginateFavourites
      addToFeed: _addToFeed
      destroy: _destroy
      report: _report
      scrollId: _scrollId
      setScrollId: _setScrollId
      initializeFeed: _initializeFeed
      paginateFeed: _paginateFeed
      feed: _feed
    }

  ]

  angular.module('Unii')
    .service('Post', Post)
)()

