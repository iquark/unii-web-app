###
# @desc Service that handles the calls to mixpanel
# @file mixpanel.service.js.coffee
###

(->

  Mixpanel = ['APP_VERSION', 'Feed', 'Navigation', 'RAILS_ENV', (APP_VERSION, Feed, Navigation, RAILS_ENV) ->

    events = {}

    postTypes =
      status_update: 'Status Update'
      image: 'Image'
      gallery: 'Gallery'
      video: 'Video'
      link: 'Link'

    feedTypes =
      whatsHot: 'Hot'
      feed: 'Uni'
      me: 'Me'
      profile: 'Profile'


    _finishEvent = (key) ->
      time = (Date.now() - events[key])/1000
      events[key] = null
      parseFloat(time.toFixed(1))

    _postType = (type) ->
      postTypes[type]

    _history = (num) ->
      num ||= 2
      path = Navigation.history(num)
      if /me/.test(path)
        'Me'
      else if /profile/.test(path)
        'Profile'
      else if /activity/.test(path)
        'Notification'
      else if /^\/$/.test(path)
        if Feed.onWhatsHot()
          'Hot'
        else
          'Uni'
      else
        'Unknown'

    _postFullscreen = ->
      path = Navigation.history(1)
      /view/.test(path)

    _track = (key, data) ->
      mixpanel.track key, data

    _register = (properties) ->
      mixpanel.register properties


    if RAILS_ENV is 'production'
      track: (key, data) ->
        mixpanel.track key, data
      identify: (id) ->
        mixpanel.identify id
      register: (user) ->
        data =
          university: user.university.name || 0
          uni_country: user.university.country || 0
          user_email: user.email
          user_post_count: user.post_count || 0
          user_favourite_count: user.favourite_count || 0
          user_comment_count: user.comment_count || 0
          days_since_verified: user.days_since_verified || 0
          release_version: APP_VERSION
          platform: 'web'
        _register data


      createComment: (comment, post) ->
        data =
          origin: _history()
          post_id: post.id
          post_author_id: post.user.id
          post_type: post.type
          prev_num_comments: post.comments_count

        _track 'Comment Created', data


      createPost: (form) ->
        data =
          'Duration' : _finishEvent('Compose Post')

        if form.medias?.length
          data.text_added = form.content?.length > 0

          if form.medias.length > 1
            # gallery
            data.num_photos = form.medias.length
            _track 'Album Post Created', data
          else
            # link, video or image
            switch form.medias[0].type
              when 'Image'
                _track 'Photo Post Created', data
              when 'Video'
                _ track 'Video Post Created', data
              else
                _track 'Link Post Created', data
                  
        else
          # status post
          _track 'Status Post Created', data


      commentFavourited: ->
        data =
          origin: _history()

        _track 'Comment Favourited', data

      commentUnfavourited: ->
        data =
          origin: _history()

        _track 'Comment Unfavourited', data

      editProfile: (form) ->
        existing = JSON.parse(localStorage.getItem('currentUser'))
        existing ||= {}
        data =
          name_changed: existing.name != form.name
          colour_changed: existing.color.code != form.color
          bio_changed: existing.tagline != form.tagline
          course_changed: existing.course.id != form.course_id
          gender_changed: existing.gender != form.gender

        _track 'Profile Edited', data

      startEvent: (key) ->
        unless events[key]
          events[key] = Date.now()

      postType: (type) ->
        postTypes[type]

      postFavourited: (post, referrer) ->
        fullscreen = 'post' == referrer
        num = if fullscreen then 2 else 1
        data =
          origin: _history(num)
          post_id: post.id
          post_author_id: post.user.id
          post_type: _postType(post.type)
          prev_num_favourites: post.favourites_count-1
          post_fullscreen: fullscreen

        _track 'Post Favourited', data

      postUnfavourited: (post, referrer) ->
        fullscreen = 'post' == referrer
        num = if fullscreen then 2 else 1
        data =
          origin: _history(num)
          post_id: post.id
          post_author_id: post.user.id
          post_type: _postType(post.type)
          prev_num_favourites: post.favourites_count+1
          post_fullscreen: fullscreen

        _track 'Post Unfavourited', data

      introScreenViewed: (screen) ->
        data =
          screen_number: screen
          'Duration' : _finishEvent('Intro Screen Viewed')

        _track 'Intro Screen Viewed', data

      favouritesScreenViewed: (post) ->
        data =
          post_type: post.type
          num_favourites: post.favourites_count
          'Duration' : _finishEvent('Favourites Viewed')
          origin: _history()

        _track 'Favourites Screen Viewed', data

      signinSubmitted: ->
        data =
          'Duration' : _finishEvent('Sign In')
        _track 'Sign-In Submitted', data

      signupSubmitted: (form) ->
        data =
          promo_code: form.promo_code
          'Duration' : _finishEvent('Sign Up')
        _track 'Sign-Up Submitted', data


      muteUser: (post) ->
        data =
          origin: _history()
          post_type: _postType(post.type)
          user_id: post.user.id
          post_fullscreen: _postFullscreen()

        _track 'Post User Muted', data

      unmuteUser: (post) ->
        data =
          origin: _history()
          post_type: _postType(post.type)
          user_id: post.user.id
          post_fullscreen: _postFullscreen()

        _track 'Post User Unmuted', data

      myProfileViewed: ->
        data =
          'Duration' : _finishEvent('Profile Viewed')
        _track 'My Profile Viewed', data

      userProfileViewed: ->
        data =
          'Duration' : _finishEvent('Profile Viewed')
        _track 'User Profile Viewed', data


      notificationsViewed: ->
        data =
          'Duration' : _finishEvent('Notifications Viewed')
        _track 'Notifications Screen Viewed', data

      bioViewed: (currentUser) ->
        if events['Bio Viewed']
          data =
            'Duration' : _finishEvent('Bio Viewed')

          if currentUser
            _track 'My Bio Viewed', data
          else
            _track 'User Bio Viewed', data

      profilePhotoAdded: ->
        _track 'Profile Photo Added', {}

      slideshowViewed: (num, currentUser) ->
        data =
          'Duration' : _finishEvent('Slideshow Viewed')
          num_photos_viewed: num
          my_profile: currentUser

        _track 'Profile Photos Screen Viewed', data

      photoCarouselScrolled: (currentUser, num) ->
        data =
          my_profile: currentUser
          num_photos_scrolled: num
          'Duration' : _finishEvent('Scrolling Carousel')

        _track 'Profile Photos Carousel Scrolled', data


      photoGridScrolled: (currentUser, num) ->
        data =
          my_profile: currentUser
          num_photos_scrolled: num
    
        _track 'Profile Photos Grid Scrolled', data

      postViewed: (post) ->
        data =
          post_type: _postType(post.type)
          num_favourites: post.favourites_count
          num_comments: post.comments_count
          origin: _history(1)
          'Duration' : _finishEvent('Post Viewed')

        _track 'Post Viewed', data

      feedBrowse: (feed, min, max) ->
        numOfPostsViewed = (max-min)+1
        feed = feedTypes[feed]
        data =
          feed: feed
          num_posts: numOfPostsViewed

        _track 'Feed Browse Mixpanel', data


    else
      track: (key, data) ->
        null
      identify: (data) ->
        null
      register: (properties) ->
        null
      createComment: (comment, post) ->
        null
      createPost: (form) ->
        null
      commentFavourited: ->
        null
      commentUnfavourited: ->
        null
      editProfile: (form) ->
        null
      startEvent: (key) ->
        null
      postType: (type) ->
        null
      history: ->
        null
      postFavourited: (post, referrer) ->
        null
      postUnfavourited: (post, referrer) ->
        null
      introScreenViewed: (screen) ->
        null
      favouritesScreenViewed: (scope) ->
        null
      signinSubmitted: ->
        null
      signupSubmitted: (form) ->  
        null
      muteUser: (post) ->
        null
      unmuteUser: (post) ->
        null
      myProfileViewed: ->
        null
      userProfileViewed: ->
        null
      notificationsViewed: ->
        null
      bioViewed: (currentUser) ->
        null
      profilePhotoAdded: ->
        null
      slideshowViewed: (currentUser, num) ->
        null
      photoCarouselScrolled: (currentUser, num) ->
        null
      photoGridScrolled: (currentUser, num) ->
        null
      postViewed: (post) ->
        null
      feedBrowse: (feed, min, max) ->
        null

  ]

  angular.module('Unii')
    .service('Mixpanel', Mixpanel)
)()
