###
# @desc Service that handles link creation
# @file link.service.js.coffee
###

(->

  Link = ['API_DOMAIN', 'Request', (API_DOMAIN, Request) ->

    _load = (link) ->
      Request.send "http://#{API_DOMAIN}/api/v2/medias", 'POST', {type: 'Link', url: link}

    _poll = (url) ->
      Request.send url, 'GET'

    _getMetadata = (link_id) ->
      Request.send "http://#{API_DOMAIN}/api/v2/medias/#{link_id}", 'GET'

    _external = (url) ->
      urlRegEx = /https?:\/\/\S+/
      if urlRegEx.test(url)
        url
      else
        "http://#{url}"

    # API
    {
      load: _load
      poll: _poll
      getMetadata: _getMetadata
      external: _external
    }

  ]

  angular.module('Unii')
    .service('Link', Link)
)()
