###
# @desc Service that handles console logs
# @file logger.service.js.coffee
###

(->

  Log = ['RAILS_ENV', '$window', (RAILS_ENV, $window) ->

    if RAILS_ENV is 'production'
      log: ->
        null
      info: ->
        null
      warn: ->
        null
      debug: ->
        null
      error: (error) ->
         # send to NewRelic
        if $window.NREUM?
          # this saves limitations for some browsers, apart of force to send an Error object
          try
            throw new Error(error)
          catch error
            $window.NREUM.noticeError(error)
    else
      log: ->
        console.log.apply(console, arguments)
      info: ->
        console.info.apply(console, arguments)
      warn: ->
        console.warn.apply(console, arguments)
      debug: ->
        console.debug.apply(console, arguments)
      error: ->
        console.error.apply(console, arguments)

  ]

  angular.module('Unii')
    .service('$log', Log)
)()
