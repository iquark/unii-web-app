(->

  UserService = ['API_DOMAIN', '$log', 'MediaService', 'Navigation', 'Normalize', '$q', 'Request', 'Post', 'STATUSES', '$timeout', (API_DOMAIN, $log, MediaService, Navigation, Normalize, $q, Request, Post, STATUSES, $timeout) ->

    status = STATUSES.IDLE

    _status = ->
      status

    _setStatus = (s) ->
      status = s

    _mute = (id, clear = false) ->
      Post.clearCache() if clear
      Request.send "http://#{API_DOMAIN}/api/v2/users/#{id}/mute", 'POST'

    _unmute = (id, clear = false) ->
      Post.clearCache() if clear
      Request.send "http://#{API_DOMAIN}/api/v2/users/#{id}/mute", 'DELETE'

    _block = (id, clear = false) ->
      Post.clearCache() if clear
      Request.send "http://#{API_DOMAIN}/api/v2/users/#{id}/block", 'POST'

    _unblock = (id, clear = false) ->
      Post.clearCache() if clear
      Request.send "http://#{API_DOMAIN}/api/v2/users/#{id}/block", 'DELETE'

    _profile = (id) ->
      deferred = $q.defer()
      request = Request.send "http://#{API_DOMAIN}/api/v2/users/#{id}", 'GET'

      request.success (data) ->
        data = Normalize.user(data.user)
        deferred.resolve(data)

      request.error ->
        deferred.reject("There was an error trying to get user profile...")

      deferred.promise

    _paginatePosts = (pagination) ->
      Request.send pagination.next_page, 'GET'

    _viewProfile = (id, options) ->
      if options?.event
        options.event.stopPropagation()
      if options?.postId and options?.referrer
        Post.setScrollId(options.postId, options.referrer)
      Navigation.go('profile', {id: id})

    # API
    {
      status: _status
      setStatus: _setStatus
      mute: _mute
      unmute: _unmute
      block: _block
      unblock: _unblock
      profile: _profile
      paginatePosts: _paginatePosts
      viewProfile: _viewProfile
    }

  ]

  angular
    .module('Unii')
    .service('UserService', UserService)
)()