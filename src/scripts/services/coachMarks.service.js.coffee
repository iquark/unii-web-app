###
# @desc Service that manages the coachmarks
# @file coachMarks.service.js.coffee
###

(->

  CoachMarks = ['$compile', 'CurrentUser', '$rootScope', ($compile, CurrentUser, $rootScope) ->
    name = 'university'
    showingFeed = true
    coachmarks = null

    _setName = (value) ->

      name = value

    _getName = (value) ->
      name

    _close = ->
      coachmarks.remove() if coachmarks
      coachmarks = null
      angular.element('.feed').removeClass 'blurred'
      angular.element('#switching-feed').removeClass('notVisible')

    _loadDirective = ->
      page = if showingFeed then 'feed' else 'whatsnew'
      unless coachmarks or CurrentUser.isFlagged("coachmarks-#{page}")
        angular.element('.feed').addClass 'blurred'
        html = angular.element(
          '<unii-coachmarks data-name="'+name+'" data-page="'+page+'">
          </unii-coachmarks>')
        view = angular.element('.home-header')

        view.prepend(html)

        $compile(html)($rootScope.$new())
        coachmarks = html

      true

    _show = ->
      if $rootScope.userSettings
        _loadDirective()
      else
        listenUserSettings = $rootScope.$on 'USER_SETTINGS', ->
          _loadDirective()
          listenUserSettings()
      true

    _showFeed = ()->
      showingFeed = true
      _show()

    _showWhatsHot = ()->
      showingFeed = false
      _show()

    # API
    {
      close: _close
      getName: _getName
      setName: _setName
      showFeed: _showFeed
      showWhatsHot: _showWhatsHot
    }
  ]

  angular.module('Unii')
    .service('CoachMarksService', CoachMarks)
)()
