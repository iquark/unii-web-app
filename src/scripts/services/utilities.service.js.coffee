
angular.module('Services', [])

.factory 'Browser', ['$window', ($window) ->

  userAgent: ->
    $window.navigator.userAgent

  has: (value) ->
    pattern = new RegExp value, 'i'
    $window.navigator.userAgent.match(pattern) != null

  isIos: ->
    $window.navigator.userAgent.match(/(iPod|iPhone|iPad)/i) != null

  isIos7: ->
    this.isIos() and $window.navigator.userAgent.match(/(OS 7)/i) != null

  ipad: ->
    $window.navigator.userAgent.match(/iPad/i) != null

  iphone: ->
    $window.navigator.userAgent.match(/iPhone/i) != null

  ipodTouch: ->
    $window.navigator.userAgent.match(/iPod/i) != null

  isAndroid: ->
    $window.navigator.userAgent.match(/Android/i) != null

  isBlackBerryMobile: ->
    $window.navigator.userAgent.match(/BlackBerry|BB10/i) != null

  isWindowsMobile: ->
    $window.navigator.userAgent.match(/IEMobile/i) != null

  isTablet: ->
    this.has("(Android 3)") or !this.has("Mobile") or this.ipad() or this.has("PlayBook") or this.has("SCH-I800") or this.has("Tablet") or this.has("Nexus (7|9|10)")

  isMobile: ->
    !this.isTablet() and (this.isAndroid() or this.iphone() or this.ipodTouch() or this.isBlackBerryMobile() or this.isWindowsMobile())

]

.factory 'Resolution', ['$window', ($window) ->

  isRetina: ->
    $window.devicePixelRatio >= 2

  devicePixelRatio: ->
    switch
      when $window.devicePixelRatio == 1
        '1x'
      else
        '2x'
]


.factory 'File', ->

  isType: (regex, file) ->
    regex.test(file.type) or regex.test(file.name)

  isJpg: (file) ->
    this.isType /(\.|\/)(jpe?g)$/i, file

  isPng: (file) ->
    this.isType /(\.|\/)(png)$/i, file

  isValid: (file) ->
    this.isType /(\.|\/)(mov|m4v|mp4|jpe?g|png)$/i, file

  extension: (file) ->
    return ".jpg" if this.isJpg file
    return ".jpg" if this.isPng file
    return ".unknown"

  getExtension: (file) ->
    if file.name
      ext = file.name.split(/\./).pop()
      switch ext
        when 'png'
          '.jpg'
        else
          "." + ext
    else if file.type
      switch file.type
        when 'image/jpeg'
          '.jpg'
        when 'image/png'
          '.jpg'
        when 'video/mpeg'
          '.mp4'
        when 'video/mp4'
          '.mp4'
        when 'video/x-m4v'
          '.m4v'
        when 'video/quicktime'
          '.mov'
        else
          null

    else
      null


  getType: (file) ->
    type = file.type.split(/\//)[0]
    type.charAt(0).toUpperCase() + type.slice(1)

.factory 'Text', ->

  hashtagRegEx = /#(\S+)/g

  hasHashtags: (text) ->
    hashtagRegEx.test(text)

  getHashtags: (text) ->
    text.match(hashtagRegEx)


.factory 'Normalize', ->

  userColors = {
    _ffcb2f: 'yellow'
    _60c9f8: 'blue'
    _fe4444: 'red'
    _cb49cb: 'purple'
    _fe9526: 'orange'
    _5bffc6: 'green'
  }

  color: (color) ->
    return unless color
    key = color.split(/#/)[1]
    {name: userColors["_#{key}"], code: color}

  user: (attributes) ->
    if attributes.hasOwnProperty('color')
      attributes.color = this.color(attributes.color) unless typeof attributes.color == 'object'
    attributes

  error: (code) ->
    code.replace(/\:/g, '_')


.factory 'Util', ->

  autoGrowTextArea: (textField, options) ->
    textField.value = options.formContent if options.hasOwnProperty('formContent')
    charsPerRow = textField.offsetWidth/8 # allow 8px width per character
    newLines = (textField.value.match(/\n/g)||[]).length
    rowsUsed = Math.floor(textField.value.length/charsPerRow) + newLines

    if textField.clientHeight < textField.scrollHeight
      textField.rows += 1
    else if (rowsUsed < textField.rows-2) # 2 is the buffer rows
      textField.rows -= 1 unless textField.rows <= options.minRows
    else if '' == textField.value
      textField.rows = options.minRows

    return 160 - parseInt(textField.value.length, 10)


