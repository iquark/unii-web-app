###
# @desc Service object for handling UI states
# @file ui.service.js.coffee
###

(->

  UI = ->

    _set = (attributes) ->
      angular.forEach attributes, (value, key) =>
        this[key] = value

    # API
    {
      creating_comment: false
      uploading: false
      sigining_in: false
      signed_in: false
      new_notifications: 0
      selecting_media: false
      getting_metadata: false
      sending_form: false
      view_loading: false
      show_modal: false
      request_sending: false
      request_sent: false
      post_warning: false
      form_error: false
      dismissedFeedInfo: localStorage.getItem('dismissed_feed_info') || false

      # post view
      viewingComments: false
      viewingFavourites: false
      set: _set
    }

  angular.module('Unii')
    .service('UI', UI)
)()
