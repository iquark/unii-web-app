###
# @desc Service that holds feed information
# @file feed.service.js.coffee
###

(->

  Feed = ->

    whatsHotFeed = false
    currentPostIndex = 0

    _onWhatsHot = (value) ->
      if arguments.length
        whatsHotFeed = value
      else
        whatsHotFeed

    _currentPostIndex = (value) ->
      if arguments.length
        currentPostIndex = parseInt(value, 10)
      else
        currentPostIndex

    # API
    {
      onWhatsHot: _onWhatsHot
      currentPostIndex: _currentPostIndex
    }


  angular.module('Unii')
    .service('Feed', Feed)
)()
