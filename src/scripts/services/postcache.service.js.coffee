###
# @desc Service that controls the cache of posts, stores at localStorage
# @file postcache.service.js.coffee
###

(->

  postCache = ->

    _save = (post)->
      posts = JSON.parse(localStorage.getItem('posts')) || {}
      posts[post.id] = post
      localStorage.setItem('posts', JSON.stringify(posts))

    _get = (postId)->
      if postId instanceof Array
        posts = JSON.parse(localStorage.getItem('posts')) || {}
        list = []
        list.push post for post in posts when post.id in postId
        list
      else
        JSON.parse(localStorage.getItem('posts'))[postId] || null

    _clear = ->
      # cleans the data at localStorage
      localStorage.setItem('posts', {})

    _remove = (postId)->
      posts = JSON.parse(localStorage.getItem('posts')) || {}
      delete posts[postId] if posts[postId]
      localStorage.setItem('posts', JSON.stringify(posts))

    # API
    {
      save: _save
      get: _get
      clear: _clear
      remove: _remove
    }

  angular.module('Unii')
  .factory('postCache', postCache)

)()