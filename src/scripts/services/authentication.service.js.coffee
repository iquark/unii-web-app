###
# @desc Service that handles authentication API requests
# @file authentication.service.js.coffee
###

(->

  Authentication = ['PROXY_DOMAIN', 'Request', '$rootScope', (PROXY_DOMAIN, Request, $rootScope) ->

    _signin = (email, password) ->
      Request.send "#{PROXY_DOMAIN}/api/signin", 'POST', {email: email, password: password}

    _signout = ->
      request = Request.send "#{PROXY_DOMAIN}/api/signout", 'GET'
      request.success ->
        $rootScope.$emit 'auth.signout'

    _signup = (data) ->
      Request.send "#{PROXY_DOMAIN}/api/signup", 'POST', data


    # API
    {
      signin: _signin
      signout: _signout
      signup: _signup
    }

  ]

  angular.module('Unii')
    .service('Authentication', Authentication)
)()
