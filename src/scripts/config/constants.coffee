angular.module 'Unii'
  .constant('APP_VERSION', '@@appVersion')
  .constant('API_DOMAIN', '@@apiDomain')
  .constant('PROXY_DOMAIN', '@@proxyDomain')
  .constant('RAILS_ENV', '@@environment')
  .constant('TIMEOUT', 15000)
  .constant('INTERVAL_NOTIFICATION', 5000)
  .constant('ITUNES_LINK', 'https://itunes.apple.com/gb/app/unii/id900539930?mt=8')
  .constant('STATUSES', {IDLE: 0, ERROR: 1, WORKING: 2, FINISHED: 3})
  .constant('PRE_UNI', {unverifiedStates: ['pre_uni', 'uni_unverified', 'unverified'], verificationPath: '/pre-uni'})
  .constant 'I18N_CONSTANT', ->
    i18n = I18n || {}
    locale = 'en-GB'

    locales = Object.keys(i18n) if angular.isObject(i18n)
    
    _locale = ->
      if navigator.language in locales
        locale = navigator.language
      locale

    _t = (key, variables) ->
      if i18n[locale]?[key]?
        string = i18n[locale][key]
        if variables
          for k, v of variables
            string = string.replace("%{#{k}}", v)
        string
        
      else
        key.replace('_', ' ').replace /(?:^|\s)\S/g, (a) -> a.toUpperCase()

    {
      locale: _locale
      t: _t
    }
