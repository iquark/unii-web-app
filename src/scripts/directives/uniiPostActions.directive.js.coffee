(->

  PostActions = ['I18nService', 'Mixpanel', 'Navigation', 'Normalize', '$log', 'Post', 'PostActionsService', '$rootScope', 'STATUSES', 'UserService', 'UI', (I18nService, Mixpanel, Navigation, Normalize, $log, Post, PostActionsService, $rootScope, STATUSES, UserService, UI) ->

    link = (scope, element, attrs) ->

      scope.destroyMenu = ->
        PostActionsService.close(scope.post.id)

      _set_status = (post, status) ->
        scope["working_#{post.id}"] = status

      scope.working = (post) ->
        scope["working_#{post.id}"] == STATUSES.WORKING

      scope.toggle_actions = () ->
        scope.userSettings = $rootScope.userSettings

        if scope.post?.user
          scope.mutedState = if scope.post.user.muted then I18nService.t('unmute_nav') else I18nService.t('mute_nav')

      scope.toggle_mute_user = (e, post, element) ->
        e.stopPropagation() if e
        return if scope.working(post)
        element = angular.element(e.target)
        _set_status(post, STATUSES.WORKING)

        _toggle_mute = ->
          post.user.muted = !post.user.muted

        if post.user.muted
          Mixpanel.unmuteUser(post)
          _toggle_mute()
          request = UserService.unmute(post.user.id)
          request.success ->
            element.text(I18nService.t('mute_nav'))
          request.error (data, status) ->
            $log.error('PostActions: UserService.unmute error', data) if data
            element.text(I18nService.t('muter_already_unmuted_this_user')) if 404 == status
            _toggle_mute()

        else
          Mixpanel.muteUser(post)
          _toggle_mute()
          request = UserService.mute(post.user.id)
          request.success ->
            element.text(I18nService.t('unmute_nav'))
          request.error (data) ->
            if data?.errors
              codes = []
              for error in data.errors
                codes.push Normalize.error(error.code)
              if 'muter_already_muted_this_user' in codes
                element.text(I18nService.t('muter_already_muted_this_user_error_body'))

            _toggle_mute()

        request.finally ->
          _set_status(post, STATUSES.IDLE)

      reported = false
      scope.report_post = (post, e) ->
        return if scope.working(post)
        if !reported
          _set_status(post, STATUSES.WORKING)

          elem = angular.element(e.target)

          e.stopPropagation() if e

          request = Post.report(post)

          request.success ->
            elem.text(I18nService.t('reported_body'))
            reported = true

          request.error ->
            elem.text(I18nService.t('reported_error_body'))

          request.finally ->
            _set_status(post, STATUSES.IDLE)
        else
          e.stopPropagation()

      scope.delete_post = (post, options) ->
        options.event.stopPropagation() if options.event
        return if scope.working(post)
        _set_status(post, STATUSES.WORKING)

        request = Post.destroy(post, attrs.index)

        request.success ->
          if 'post' == options.referrer
            Post.clearScrollIds()
            Navigation.go('feed')
          else
            if not parseInt(attrs.index, 10)
              Post.clearScrollIds()
              Navigation.back()

        request.error (data) ->
          $log.error "uniiDeletePost: could not delete post. ", data

        request.finally ->
          _set_status(post, STATUSES.IDLE)

      scope.toggle_actions()


    directive =
      restrict: 'E'
      templateUrl: '/views/'+I18nService.locale()+'/directives/post_actions.html'
      link: link

    return directive
  ]

  angular.module('Unii')
    .directive('uniiPostActions', PostActions)
)()