###
# @desc Directive for signaling no internet
# @file noInternet.directive.js.coffee
###

(->

  noInternet = ['$rootScope', 'I18nService', ($rootScope, I18nService) ->

    link = (scope, element, attrs) ->
      if attrs.fixed == 'true'
        element.addClass('fixed')

      unless attrs.neutral == 'true'
        $rootScope.$on 'USER_SETTINGS', ->
          element.addClass("user-#{$rootScope.userSettings.color.name}")

      element.addClass("user-#{$rootScope.userSettings?.color?.name}")

      $rootScope.noInternet = false

      scope.dismiss_timeout = ->
        $rootScope.dismissTimeout = true

      connectionListener = $rootScope.$on 'TIMEOUT', (e, value) ->
        $rootScope.noInternet = value
        $rootScope.dismissTimeout = false if value

      scope.$on '$destroy', ->
        connectionListener()

    directive =
      restrict: 'E'
      templateUrl: '/views/'+I18nService.locale()+'/directives/no_internet.html'
      replace: true
      scope:
        fixed: '@'
      link: link

    return directive

  ]

  angular.module('Unii')
    .directive('noInternet', noInternet)
)()
