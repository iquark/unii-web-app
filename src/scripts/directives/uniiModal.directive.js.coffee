###
# @desc Directive for information modal
# @file uniiModal.directive.js.coffee
###

(->

  Modal = ['I18nService', 'UI', '$rootScope', (I18nService, UI, $rootScope) ->

    _buttons =
      close: I18nService.t('close_nav')
      cancel: I18nService.t('cancel_nav')
      ok: I18nService.t('ok_nav')
      resend: I18nService.t('resend_nav')
      leave: I18nService.t('leave_nav')
      unblock: I18nService.t('unblock_nav')
      unmute: I18nService.t('unmute_nav')
      yes: I18nService.t('yes_nav')
      no: I18nService.t('no_nav')

    _titles =
      unmute: I18nService.t('unmute_confirm_title')
      unblock: I18nService.t('unblock_confirm_title')
      saveFailed: 'Could not save your changes.'
      unsavedChanges: I18nService.t('leave_view_unsaved_changes_body')
      unsavedComment: I18nService.t('unsaved_comment_changes_body')
      multimedia: I18nService.t('post_can_only_include_one_media_type_error_body')
      multivideo: I18nService.t('post_can_only_include_one_video_error_body')
      filetype: I18nService.t('unsupported_file_body_error_body')
      filesize: I18nService.t('unsupported_file_size_error_body')
      commentCreateError: I18nService.t('comment_add_failed_error_body')


    link = (scope) ->

      scope.closeModal = ->
        scope.showModal = false

      $rootScope.$on 'SHOW_MODAL', (e, data, callback) ->

        scope.showModal = true
        scope.text = {}
        scope.message = ''

        scope.actionBtn = ->
          scope.closeModal()
          callback()

        switch data.config

          when 'confirm.unmute'
            scope.title         = _titles.unmute
            scope.text.dismiss  = _buttons.cancel
            scope.text.action   = _buttons.unmute

          when 'confirm.unblock'
            scope.title         = _titles.unblock
            scope.text.dismiss  = _buttons.cancel
            scope.text.action   = _buttons.unblock

          when 'save.failed'
            scope.title         = _titles.saveFailed
            scope.text.dismiss  = _buttons.cancel
            scope.text.action   = _buttons.resend

          when 'cancel.warning'
            scope.title         = _titles.unsavedChanges
            scope.text.dismiss  = _buttons.cancel
            scope.text.action   = _buttons.leave

          when 'cancel.comment'
            scope.title         = _titles.unsavedComment
            scope.text.dismiss  = _buttons.no
            scope.text.action   = _buttons.yes

          when 'preuni.warning'
            scope.title         = data.meta.title
            scope.text.dismiss  = _buttons.close

          when 'post.multimedia_warning'
            scope.title         = _titles.multimedia
            scope.text.action   = _buttons.close

          when 'post.multivideo_warning'
            scope.title         = _titles.multivideo
            scope.text.action   = _buttons.close

          when 'post.filetype_warning'
            scope.title         = _titles.filetype
            scope.text.action   = _buttons.close

          when 'post.filesize_warning'
            scope.title         = _titles.filesize
            scope.text.action   = _buttons.close

          when 'comment.create_error'
            scope.title         = _titles.commentCreateError
            scope.text.dismiss  = _buttons.no
            scope.text.action   = _buttons.yes

          when 'general.errors'
            scope.title         = data.meta.title
            scope.text.dismiss  = _buttons.close

    directive =
      restrict: 'E'
      templateUrl: '/views/'+I18nService.locale()+'/directives/modal.html'
      link: link

    return directive
  ]

  angular.module('Unii')
    .directive('uniiModal', Modal)
)()
