###
# @desc Directive for intro tour of app
# @file intro.directive.js.coffee
###

(->

  Intro = ['Mixpanel', 'Navigation', 'Resolution', '$window', (Mixpanel, Navigation, Resolution, $window) ->

    page_width    = parseInt(angular.element(document).width(), 10)
    snap_points   = [0, page_width, 2*page_width, 3*page_width]
    start_point   = null
    end_point     = null
    move_point    = null
    images        = []

    Mixpanel.startEvent('Intro Screen Viewed')

    _add_class = (elem, class_name) ->
      elem.addClass(class_name)

    _remove_class = (elem, class_name) ->
      elem.removeClass(class_name)


    _snap_to = (element, step, direction) ->

      # animate text slider
      element.addClass('transition')
      xPoint = "#{-step*page_width}px"
      # element.css 'transform', "translate3d(#{-step*page_width}, 0, 0)"
      element.css {
        '-webkit-transform' : "translate3d(#{xPoint}, 0, 0)"
        '-moz-transform' : "translate3d(#{xPoint}, 0, 0)"
        'transform' : "translate3d(#{xPoint}, 0, 0)"
      }

      # remove transition class once animation is complete
      # If the class remains, the touchmove event stutters as
      # the element has transition properties
      setTimeout(_remove_class, 500, element, 'transition')

      # animate background
      if 'left' == direction
        _add_class(images[step-1], 'fade')
      else
        _remove_class(images[step], 'fade')

    link = (scope, element) ->

      scope.view = Navigation.go

      images = [
        angular.element('.bg-1')
        angular.element('.bg-2')
        angular.element('.bg-3')
        angular.element('.bg-4')
      ]

      angular.forEach images, (image) ->
        if Resolution.isRetina()
          _add_class(image, 'retina')
        else
          _add_class(image, 'normal')

      # set first step in slide show
      scope.step = 0
      # set slider current position
      slider_point = 0

      # set all slides to width of page
      element.children().css 'width', page_width
      # set slides container to correct width
      element.css 'width', 4*page_width

      # set touch area to listen for events
      touch_area = angular.element('.intro')


      ###
      # EVENTS
      ###

      touch_area.on 'touchstart', (event) ->
        # set start touch point x co-ordinate
        start_point = event.originalEvent.touches[0].pageX

      touch_area.on 'touchmove', (event) ->
        # update move touch point x co-oridnate
        move_point = event.originalEvent.touches[0].pageX
        # calculate how much slider should move
        move_to = Math.min((move_point - start_point) + slider_point, 0)
        move_to = "#{move_to}px"
        # animate slider
        element.css {
          '-webkit-transform' : "translate3d(#{move_to}, 0, 0)"
          '-moz-transform' : "translate3d(#{move_to}, 0, 0)"
          'transform' : "translate3d(#{move_to}, 0, 0)"
        }

      endAction = (event) ->
        regEx = /(button|text)/
        return if regEx.test(event.target.className)
        # set end touch point x co-ordinate
        end_point = event.originalEvent.changedTouches[0].pageX
        # set distance of user swipe
        distance = end_point - start_point

        # determine direction of user swipe and set next step
        if distance < 0
          direction = 'left'
          next_step = Math.min((scope.step+1),3)
        else
          direction = 'right'
          next_step = Math.max((scope.step-1),0)

        # update the sliders current position
        slider_point = -snap_points[next_step]

        # animate slider
        _snap_to(element, next_step, direction)

        # update slide indicator
        if next_step != scope.step
          Mixpanel.introScreenViewed(next_step+1)
          Mixpanel.startEvent('Intro Screen Viewed')
        scope.$apply -> scope.step = next_step

      touch_area.on 'touchend', endAction
      # for cases like: the user is using it and switches off the mobile
      touch_area.on 'touchcancel', endAction

      # if the user changes the orientation the width values are reset
      angular.element($window).bind 'resize', ->
        scope.$apply ->
          page_width    = parseInt(angular.element(document).width(), 10)
          snap_points   = [0, page_width, 2*page_width, 3*page_width]
          # set all slides to width of page
          element.children().css 'width', page_width
          # set slides container to correct width
          element.css 'width', 4*page_width
          direction = if scope.step > 0 then 'left' else 'rigth'
          _snap_to(element, scope.step, direction)

    directive =
      restrict: 'A'
      link: link
  ]

  angular.module('Unii')
    .directive('uniiIntro', Intro)
)()