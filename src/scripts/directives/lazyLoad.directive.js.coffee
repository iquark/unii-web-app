###
# @desc Directive for lazy loading images
# @file lazyLoad.directive.js.coffee
###

(->

  lazyLoad = ['LazyLoad', (LazyLoad) ->

    link = (scope, element) ->

      watchBackground = scope.$watch 'background', (url)->
        if url
          LazyLoad.load element, ->
            element.css "background-image","url("+url+")"

      watchImg = scope.$watch 'src', (url)->
        if url
          LazyLoad.load element, ->
            element.src url

      element.on '$destroy', ->
        watchBackground()
        watchImg()

    directive =
      transclude: true
      scope:
        background: '='
        src: '='
      link: link

    return directive
  ]

  angular.module('Unii')
    .directive('lazyLoad', lazyLoad)
)()