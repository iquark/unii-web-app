###
# @desc Directive link cards in feed
# @file uniiLink.directive.js.coffee
###

(->

  Link = ->

    link = (scope, element) ->

      images = scope.post.medias.data[0]?.metadata?.payload?.images

      if images?
        image = images.large || images.small || images.thumb
      else
        image = null

      if image?
        element.css
          "background-image" : "url("+scope.post.medias.data[0].metadata.payload.images.large+")"


    directive =
      restrict: 'A'
      link: link

    return directive

  angular.module('Unii')
    .directive('uniiLink', Link)
)()
