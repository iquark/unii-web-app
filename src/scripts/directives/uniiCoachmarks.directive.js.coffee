###
# @desc Directive to show the coachmarks
# @file uniiCoachmarks.directive.js.coffee
###

(->

  Coachmarks = ['CoachMarksService', '$compile', 'CurrentUser', 'I18nService', '$rootScope', (CoachMarksService, $compile, CurrentUser, I18nService, $rootScope) ->

    link = (scope, element) ->
      brand = angular.element('.brand')
      container = brand.find('#we-are-at')
      university = container.find('#uni-name')
      switchingFeed = brand.find('#switching-feed')
      path = element.find('path')
      svg = element.find('svg')
      text = element.find('#coachmarks-text')
      width = element.context.parentNode.offsetParent.clientWidth
      height = element.context.parentNode.offsetParent.clientHeight
      svg.attr 'width', width
      svg.attr 'height', height
      radius = 27
      stroke = 3
      shapeDrawn = false
      scope.circleStroke = 2

      _drawRectangle = ->
        angular.element('#switching-feed').addClass('notVisible')
        radius = 10
        y = brand[0].clientHeight - (switchingFeed[0].clientHeight + 4 + container[0].clientHeight) # the 7 is the margin bottom of switching-feed
        x = university[0].offsetLeft-10
        uniNameWidth = Math.min(university.width(), container[0].clientWidth/2)
        rwidth = uniNameWidth+10
        rheight = university.height() + radius + radius/2
        scope.path = "M-#{stroke},-#{stroke}h#{width+stroke*2}v#{height+stroke*2}h-#{width+stroke*2} V300z
          M#{x},#{y}
          a #{radius},#{radius} 0 0 1 #{radius},-#{radius}
          h #{rwidth - radius}
          a #{radius},#{radius} 0 0 1 #{radius},#{radius}
          v #{rheight-2*radius}
          a #{radius},#{radius} 0 0 1 -#{radius},#{radius}
          h #{radius - rwidth}
          a #{radius},#{radius} 0 0 1 -#{radius},-#{radius}
          z"

      _drawCircle = ->
        cx = width/2
        cy = brand[0].clientHeight - (switchingFeed[0].clientHeight - 11 + container[0].clientHeight) # the 7 is the margin bottom of switching-feed
        scope.path = "M-#{stroke},-#{stroke}h#{width+stroke*2}v#{height+stroke*2}h-#{width+stroke*2} V300z
          M #{cx},#{cy}
          m -#{radius}, 0
          a #{radius},#{radius} 0 1,0 #{radius*2},0
          a #{radius},#{radius} 0 1,0 -#{radius*2},0
          z"


      _highlightChevron = ->
        chevron = container.find('#chevron')
        cx = width+(chevron[0].offsetLeft-brand[0].clientWidth) + chevron[0].clientWidth/2
        cy = brand[0].clientHeight - (switchingFeed[0].clientHeight - 7 + container[0].clientHeight) # the 7 is the margin bottom of switching-feed
        
        scope.path = "M-#{stroke},-#{stroke}h#{width+stroke*2}v#{height+stroke*2}h-#{width+stroke*2} V300z
          M #{cx - width},#{cy}
          m -#{radius}, 0
          a #{radius},#{radius} 0 1,0 #{radius*2},0
          a #{radius},#{radius} 0 1,0 -#{radius*2},0
          z"

        scope.textBody = I18nService.t('whats_hot_feed_coachmark_body',
          value1: "<span>"+I18nService.t('whats_hot_coachmark_value1')+'</span>',
          value2: "<span>"+I18nService.t('whats_hot_coachmark_value2')+'</span>')

        text.css
          'top': brand[0].clientHeight+10,
          'left': cy + 60

      _highlightUniName = (name) ->
        if name.length > 4
          _drawRectangle()
        else
          _drawCircle()

        scope.textBody = I18nService.t('now_feed_universtity_coachmark_body',
          value1: "<span>"+I18nService.t('now_feed_university_coachmark_value')+'</span>')

        text.css
          'top': brand[0].clientHeight+10,
          'left': width/2 - 100 # the textbox is 200 width
  
        shapeDrawn = true

      if scope.page == 'feed'
        _highlightUniName(scope.name) if scope.name
      else # whatsnew
        _highlightChevron()

      scope.userSettings = $rootScope.userSettings

      userSettingsWatch = $rootScope.$watch 'userSettings', (value) ->
        if value
          user = JSON.parse(localStorage.getItem('currentUser'))
          if 'feed' == scope.page and not shapeDrawn
            _highlightUniName(user.university.name_short)
          text.addClass "has-text-#{value.color.name}"
          userSettingsWatch()

      element.bind 'touchstart mousedown', (event) ->
        event.preventDefault()
        CurrentUser.setFlag("coachmarks-#{scope.page}")
        scope.$destroy()
        CoachMarksService.close()

      scope.$on '$destroy', ->
        userSettingsWatch()

    directive =
      restrict: 'E'
      link: link
      templateUrl: '/views/'+I18nService.locale()+'/directives/coachmarks.html'
      scope:
        name: '@'
        page: '@'

    return directive
  ]

  angular.module('Unii')
    .directive('uniiCoachmarks', Coachmarks)
)()
