(->

  ThumbSlides = ['LazyLoad', '$log', 'MediaService', 'Mixpanel', 'Navigation', '$timeout', '$location', 'I18nService', (LazyLoad, $log, MediaService, Mixpanel, Navigation, $timeout, $location, I18nService) ->

    link = (scope, element) ->

      SLIDE_WIDTH = 125 # width (px) of individual slide
      SLIDE_BUFFER = 500 # distance (px) from end of slides API request is made
      TIMER = 700 # time in ms before chevron re-appears

      scope.images = []
      scope.profile = scope.profile()
      scope.view_slide = (index) ->
        Navigation.go('profileSlides', {id: scope.profile.id}).hash(index+1)
      scope.chevron = false
      scope.show_slider = false

      side_scroller = element.find('.side-scroller')
      side_scroller_width = 0
      scrollMin = null
      scrollMax = null
      page_width = angular.element(document).width()
      
      timer = null
      paginating_images = false

      setSliderWidth = ->
        if scope.images.length > 5
          scope.show_slider = true
          side_scroller_width = scope.images.length*SLIDE_WIDTH
          showChevron()

      showChevron = ->
        return if (side_scroller.scrollLeft()+page_width) > (side_scroller_width-20)
        scope.chevron = true

        LazyLoad.trigger()

        if (typeof scrollMin == 'number')

          lowIndex = scrollMin/SLIDE_WIDTH
          lowIndex = Math.round(lowIndex)

          highIndex = (scrollMax+page_width)/SLIDE_WIDTH
          highIndex = Math.round(highIndex)-1

          photosViewed = highIndex-lowIndex

          currentUser = /me/.test($location.path())

          Mixpanel.photoCarouselScrolled(currentUser, photosViewed)

          scrollMin = null
          scrollMax = null
        

      scrollCallback = (e) ->
        scrollBox = angular.element(this)
        if scrollMin
          scrollMin = Math.min(scrollMin, scrollBox.scrollLeft())
        else
          Mixpanel.startEvent('Scrolling Carousel')
          scrollMin = scrollBox.scrollLeft()
        scrollMax = Math.max(scrollMax, scrollBox.scrollLeft())
        scope.$apply -> scope.chevron = false
        $timeout.cancel(timer) if timer
        timer = $timeout(showChevron, TIMER)

        if (side_scroller_width - side_scroller.scrollLeft()) < SLIDE_BUFFER
          paginateImages()

      scroll_listener = side_scroller.bind 'scroll', scrollCallback

      paginateImages = ->
        return if paginating_images
        paginating_images = true

        success = (data) ->
          scope.images = MediaService.cachedImages(scope.profile.id)
          setSliderWidth()

        error = (message) ->
          $log.error "paginateImages: #{message}"

        updateUI = ->
          paginating_images = false

        MediaService
          .paginateImages(scope.profile.id)
          .then(success)
          .catch(error)
          .finally(updateUI)
      
      loadImages = ->
        return if loading_images
        loading_images = true

        success = (data) ->
          scope.images = MediaService.cachedImages(scope.profile.id)
          setSliderWidth()

        error = (message) ->
          $log.info "ThumbSlides: #{message}"

        updateUI = ->
          loading_images = false

        MediaService
          .getImages(scope.profile.id)
          .then(success)
          .catch(error)
          .finally(updateUI)

      loadImages()


      scope.$on '$destroy', ->
        $log.info 'ThumbSlides: Scroll listener removed.'
        scroll_listener.unbind 'scroll', scrollCallback

    directive =
      restrict: 'E'
      templateUrl: '/views/'+I18nService.locale()+'/directives/thumb_slides.html'
      scope:
        profile: '&'

      link: link

    return directive

  ]

  angular.module('Unii')
    .directive('uniiThumbSlides', ThumbSlides)
)()