###
# @desc Directive to toggle liking of a post
# @file uniiToggleLikePost.directive.js.coffee
# @example <div data-unii-toggle-favourite-post post="post" referrer="{{referrer}}"></div>
###

(->

  ToggleFavouritePost = ['Post', '$rootScope', (Post, $rootScope) ->

    link = (scope, element) ->
      element.bind 'click', (e) ->
        e.stopPropagation()
        scope.userSettings = $rootScope.userSettings

        if scope.post.user_favourited
          Post.unfavourite(scope.post, scope.referrer)
        else
          Post.favourite(scope.post, scope.referrer)

    directive =
      restrict: 'A'
      link: link
      scope:
        post: '='
        referrer: '@'

    return directive

  ]

  angular.module('Unii')
    .directive('uniiToggleFavouritePost', ToggleFavouritePost)

)()