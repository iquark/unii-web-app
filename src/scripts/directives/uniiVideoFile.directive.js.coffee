###
# @desc Directive video files in post view
# @file uniiVideoFile.directive.js.coffee
###

(->

  VideoFile = ['AssetsService', 'Resolution', (AssetsService, Resolution) ->

    DPR = Resolution.devicePixelRatio()

    link = (scope, element) ->
      src = scope.post.medias.data[0].versions.ld || scope.post.medias.data[0].url
      # post version is cropped. 'original' version has been removed from v2.
      poster = scope.post.medias.data[0].images?.post?[DPR] || AssetsService.getAsset('videoPlaceholder')

      element[0].src    = src
      element[0].poster = poster

    directive =
      restrict: 'A'
      link: link

    return directive

  ]

  angular.module('Unii')
    .directive('uniiVideoFile', VideoFile)
)()
