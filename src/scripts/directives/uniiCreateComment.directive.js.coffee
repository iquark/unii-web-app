###
# @desc Directive for creating comments
# @file uniiCreateComment.directive.js.coffee
###

(->

  CreateComment = ['Comment', 'Mixpanel', 'Navigation', 'Post', '$rootScope','UI', '$timeout', '$log', 'I18nService', (Comment, Mixpanel, Navigation, Post, $rootScope, UI, $timeout, $log, I18nService) ->

    link = (scope, element) ->
      # Should be using the same scope as view controller.
      # ng-switch creates a new scope that seems to affect this directive,
      # so setting scope to scope.$parent
      scope = scope.$parent
      scope.sending_comment = false
      button = element.find('#post-comment')

      _createComment = ->
        scope.sending_comment = true
        data = scope.comment_form

        if not (scope.post.comments.length) and data.content == ''
          scope.$apply -> data.errors.push {message: "Comment must have SOME content!"}
          scope.sending_comment = false
        else
          file = scope.post.comments[0]

          unless undefined == file
            data.media = file.details

          create_comment = Comment.create(scope.post.id, data)

          create_comment.success (data) ->

            Mixpanel.createComment(data.comment, scope.post)

            # show comment in post view
            scope.post.comments.data.push data.comment
            # clear comment form
            scope.comment_form = {}
            # clear the comment media queue from Main Ctrl
            #scope.$parent.post.comments = []
            Post.newComment(scope.post.id)
            scope.post.comments_count += 1

            # scroll the comments window to bottom on new comment

            comment_modal = angular.element('.comments-poster-sections')[0]

            comment_textarea = element.find('#comment-form-text')

            if comment_modal

              scrollCommentsWindow = ->
                comment_modal.scrollTop = comment_modal.scrollHeight
                comment_textarea.css 'height', '44px'

              $timeout(scrollCommentsWindow, 300)

            else
              # user has moved away
              Navigation
                .go('post', {id: scope.post.id})
                .search({state: 'comments'})

          create_comment.error (e) ->
            $log.error "uniiCreateComment: error ", e
            data =
              config: 'comment.create_error'
            $rootScope.$emit 'SHOW_MODAL', data, ->
              _createComment()

          create_comment.finally ->
            # remove any media showing
            scope.sending_comment = false

      button.bind 'click', _createComment


    directive =
      restrict: 'A'
      link: link

    return directive

  ]

  angular.module('Unii')
    .directive('uniiCreateComment', CreateComment)
)()