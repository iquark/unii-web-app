###
# @desc Directive to truncate a users name
#
# Directive will programatically truncate a users last name, one
# name at a time if they have multiple, until the length of the entire
# name is below max set in directive e.g Alexander W. B.
#
# If the name is still not short enough, the first name will be
# truncated with an ellipsis e.g Alex... W. B.
#
# @file uniiTruncateName.directive.js.coffee
###

(->

  TruncateName = ->

    _escapedInput = (string) ->
      string = string || ''
      string.replace(/&/g, '&amp;')
      .replace(/"/g, '&quot;')
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;')
      string

    _nameTooLong = (firstName, lastNamesAry, maxChars) ->
      last = lastNamesAry.join(' ')
      (last.length + firstName.length) > maxChars

    _getNameToTruncate = (lastNamesAry) ->
      lastNamesAry.reverse()
      result = null
      # For loop will return first available middle name for truncation
      for name in lastNamesAry[1..-1]
        if not /\./.test(name)
          result = name
          break
      lastNamesAry.reverse()
      result


    link = (scope, element, attrs) ->
      return unless scope.user
      user = scope.user()
      maxChars = parseInt(attrs.uniiTruncateName, 10) || 25

      firstName = _escapedInput(user.first_name)
      lastName  = _escapedInput(user.last_name)
      nameArray = lastName.split(' ')

      checkLength = ->
        if _nameTooLong(firstName, nameArray, maxChars)
          name = _getNameToTruncate(nameArray)
          if name
            cutName = name[0] + '.'
            nameArray[nameArray.indexOf(name)] = cutName
            lastName = nameArray.join(' ')
            checkLength()
          else
            # Middle names cannot be truncated anymore - let's truncate
            # the users last name as well if possible
            if /\./.test(nameArray[nameArray.length-1])
              # Last name cannot be truncated any more, we're gonna have to
              # truncate the first name too.
              freeChars = maxChars - lastName.length - 3 # for ellipsis ...
              cutName = firstName.slice(0, freeChars)
              firstName = cutName + '...'

            else
              nameArray[nameArray.length-1] = nameArray[nameArray.length-1][0] + '.'
              lastName = nameArray.join(' ')
              checkLength()




          
      checkLength()

      element.text(firstName + ' ' + lastName)


    directive =
      restrict: 'A'
      scope:
        user: '&'
      link: link

    return directive

  angular.module('Unii')
    .directive('uniiTruncateName', TruncateName)
)()