###
# @desc Directive video cards in feed
# @file uniiVideo.directive.js.coffee
###

(->

  Video = ['LazyLoad', 'AssetsService', (LazyLoad, AssetsService) ->

    link = (scope, element) ->
      scalar = 1
      height = element.width() * scalar

      element.css { height: height }

      LazyLoad.load element, ->
          img = new Image()

          image = scope.post.medias.data[0].images.feed || AssetsService.getAsset('imagePlaceholder')

          img.src = image

          img.onload = ->
            element.css("background-image","url("+image+")")

          element.find(".linkable").css "height", height-75

    directive =
      restrict: 'A'
      link: link

    return directive

  ]

  angular.module('Unii')
    .directive('uniiVideo', Video)
)()