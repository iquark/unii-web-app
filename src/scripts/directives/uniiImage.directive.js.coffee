###
# @desc image that will be lazy loaded. Image has a default placeholder
# that will be swapped out when requested image has loaded.
# @file uniiImage.directive.js.coffee
# @example <div data-unii-image image="image_url" size="square"></div>
###

(->
  
  UniiImage = ['Resolution', 'LazyLoad', (Resolution, LazyLoad) ->

    DPR = Resolution.devicePixelRatio()

    link = (scope, element) ->

      image = scope.image()

      return unless image

      element.addClass('versions') if image.versions?.action_bar

      switch scope.size
        when 'square'
          element.css 'height', element.width()

      LazyLoad.load element, ->
        img = new Image()
        image = image.versions[scope.format]?[DPR] || image.url
        img.src = image
        img.onload = ->
          element.css "background-image","url("+image+")"


    directive =
      restrict: 'A'
      link: link
      scope:
        image: '&'
        size: '@'
        format: '@'

    return directive
  ]


  angular.module('Unii')
    .directive('uniiImage', UniiImage)

)()

