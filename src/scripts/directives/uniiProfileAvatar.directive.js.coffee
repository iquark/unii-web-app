###
# @desc Directive for profile avatar
# @file uniiProfileAvatar.directive.js.coffee
###

(->

  ProfileAvatar = ['Browser', 'Resolution', '$window', (Browser, Resolution, $window) ->

    DPR = Resolution.devicePixelRatio()
    IOS7 = Browser.isIos7()

    link = (scope, element) ->

      set_avatar = (update) ->
        if scope.profile.avatar
          img = new Image()

          imageUrl = scope.profile.avatar.versions.profile?[DPR] || scope.profile.avatar.url

          img.src = imageUrl

          img.onload = ->
            element.children('li').css {
              "background-image" : "url("+imageUrl+")"
            }
            scope.$apply -> scope.loading = false if update

      watch = scope.$watch 'trigger', (loading) ->
        if loading
          set_avatar(true)

      set_avatar()

      # .profile-box height is set to 100vw
      # Resize listener is needed due to how iOS7 handles vw/vh units.
      if IOS7
        _resizeListener = ->
          elem = angular.element('.profile-box')
          elem.css 'height', elem.width()

        $window.addEventListener 'resize', _resizeListener

      scope.$on '$destroy', ->
        watch()
        if IOS7
          $window.removeEventListener 'resize', _resizeListener

    directive =
      restrict: 'A'
      scope:
        profile: '='
        loading: '='
        trigger: '='
      link: link

    return directive

  ]


  angular.module('Unii')
    .directive('uniiProfileAvatar', ProfileAvatar)
)()