###
# @desc Directive for feedback/support form
# @file feedbackForm.directive.js.coffee
###

(->

  FeedbackForm = ['Email', '$routeParams', 'UI', 'Validation', '$log', 'I18nService', (Email, $routeParams, UI, Validation, $log, I18nService) ->

    link = (scope) ->

      scope.success = false

      # Regex to override default email regex for validation
      # Default allows emails such as 'x@x'
      scope.email_regex = Validation.regex.email
      # Regex forbids digits being used in name field.
      scope.name_regex  = Validation.regex.name

      $routeParams.t = $routeParams.t || 'support'

      scope.submit_form = ->

        scope.errors = []

        UI.sending_form = true

        if scope.SupportForm.$valid

          scope.form.type = $routeParams.t

          request = Email.feedbackForm(scope.form)

          request.success ->

            scope.form = {}
            scope.SupportForm.$setPristine()

            UI.sending_form = false
            scope.success = true

          request.error (data)->
            $log.error 'feedbackForm: Error trying to send email. ', data
            UI.sending_form = false

        else
          scope.errors.push {message: I18nService.t('form_incomplete_error_body')}
          UI.sending_form = false

    directive =
      restrict: 'E'
      templateUrl: '/views/'+I18nService.locale()+'/directives/feedback_form.html'
      link: link

    return directive
  ]

  angular.module('Unii')
    .directive('feedbackForm', FeedbackForm)
)()
