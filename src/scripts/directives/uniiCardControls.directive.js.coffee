###
# @desc Directive for card controls template
# @file uniiCardControls.directive.js.coffee
###

(->

  CardControls = [ 'PostActionsService', 'I18nService', (PostActionsService, I18nService) ->

    link = (scope, element) ->
      menu = null
      internalScope = null

      scope.createMenu = (event, index)->
        event.stopPropagation() if event

        PostActionsService.show(scope.post, angular.element(element[0].parentNode.parentNode), index)
        return

    directive =
      restrict: 'A'
      link: link
      templateUrl: '/views/'+I18nService.locale()+'/directives/cardcontrols.html'

    return directive
  ]

  angular.module('Unii')
    .directive('uniiCardControls', CardControls)
)()