###
# @desc Directive album cards in feed
# @file uniiAlbum.directive.js.coffee
###

(->

  Album = ['Navigation', 'Post', (Navigation, Post)->

    link = (scope, element) ->

      scope.viewSlide = (postId, options) ->
        Post.setScrollId(postId, options.referrer)
        Navigation.go('postSlides', {id: postId}).hash(options.index+1)

      element.css "height", element.width()

    directive =
      restrict: 'A'
      link: link

    return directive

  ]

  angular.module('Unii')
    .directive('uniiAlbum', Album)
)()
