###
# @desc This directive will hide/show navbar and compose button as
# user scrolls down/up the feed
# @file uniiScrollAnimations.directive.js.coffee
# @example <div data-unii-scroll-animations></div>
###

(->

  Scroll = ['$window', 'Browser', '$timeout', ($window, Browser, $timeout) ->

    link = (scope) ->

      # set distance in px user should scroll before hiding buttons
      SCROLL_BUFFER = 10

      # public
      scope.hideElements = false
      scope.animatingElements = false

      # private
      scrollTimer = null
      startPoint = null
      endPoint = null


      _freeAnimation = ->
        scope.animatingElements = false

      _toggleButtons = (direction) ->
        scope.animatingElements = true
        if direction
          return if scope.hideElements
          scope.hideElements = true
        else
          return unless scope.hideElements
          scope.hideElements = false
        $timeout(_freeAnimation, 800)

      _scrollingEnd = ->
        endPoint = $window.scrollY
        toggle = Math.abs(startPoint-endPoint) > SCROLL_BUFFER
        direction = endPoint > startPoint
        if endPoint < 10
          _toggleButtons(false)
        else if toggle
          _toggleButtons(direction)

        startPoint = null
        scrollTimer = null

      _scrollBind = ->
        $timeout.cancel(scrollTimer) if scrollTimer

        scrollTimer = $timeout(_scrollingEnd, 75)

      _startPoint = ->
        startPoint = $window.scrollY unless startPoint

      angular.element($window).bind 'scroll', _scrollBind
      angular.element($window).bind 'touchstart', _startPoint

      whatsHotWatcher = scope.$watch 'viewingWhatsHot', (newValue, oldValue) ->
        if newValue != oldValue
          toggleButtons(false) # show UI when user switches feed

      # remove event listener
      scope.$on '$destroy', ->
        angular.element($window).unbind 'scroll', _scrollBind
        angular.element($window).unbind 'touchstart', _startPoint
        whatsHotWatcher()

    directive =
      restrict: 'A'
      link: link

    return directive

  ]

  angular.module('Unii')
    .directive('uniiScrollAnimations', Scroll)
)()
