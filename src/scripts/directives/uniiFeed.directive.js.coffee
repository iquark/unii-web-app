(->

  Feed = ['AssetsService', 'Post', '$location', 'Normalize', 'PostActionsService', '$rootScope', 'UI', '$log', 'Link', 'UserService', 'I18nService', 'Mixpanel', '$timeout', (AssetsService, Post, $location, Normalize, PostActionsService, $rootScope, UI, $log, Link, UserService, I18nService, Mixpanel, $timeout) ->

    link = (scope) ->
      scope.location = $location

      scope.userColor = Normalize.color

      scope.viewPost = Post.view

      scope.viewProfile = UserService.viewProfile

      minPostViewed = null
      maxPostViewed = null
      changingFeed = false

      scope.postInView = (obj) ->
        return if UI.view_loading or changingFeed
        if obj.inview
          if typeof minPostViewed == 'number'
            minPostViewed = Math.min(minPostViewed, obj.index)
          else
            minPostViewed = obj.index
          maxPostViewed = Math.max(maxPostViewed, obj.index)


      userSettingsListener = $rootScope.$on 'USER_SETTINGS', ->
        scope.userSettings = $rootScope.userSettings
      scope.userSettings = $rootScope.userSettings

      scope.externalLink = Link.external

      feedChangeListener = scope.$on 'CHANGING_FEED', (e, value) ->
        changingFeed = value
        if value
          Mixpanel.feedBrowse(scope.referrer, minPostViewed, maxPostViewed)
          minPostViewed = null
          maxPostViewed = null

      scope.$on '$destroy', ->
        PostActionsService.closeAll()
        userSettingsListener()
        feedChangeListener()
        Mixpanel.feedBrowse(scope.referrer, minPostViewed, maxPostViewed)

    directive =
      restrict: 'A'
      templateUrl: '/views/'+I18nService.locale()+'/directives/cards.html'
      scope:
        feed: "="
        referrer: "@"
        viewpost: '&'
      link: link

    return directive

  ]

  angular.module('Unii')
    .directive('uniiFeed', Feed)
)()
