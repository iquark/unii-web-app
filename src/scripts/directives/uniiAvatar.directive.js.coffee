###
# @desc Directive for user avatars
# @file uniiAvatar.directive.js.coffee
###

(->

  Avatar = ['Navigation', 'Resolution', 'UserService', (Navigation, Resolution, UserService) ->

    DPR = Resolution.devicePixelRatio()

    link = (scope, element) ->
      scope.$watch 'user', (user) ->
        if user and user.avatar

          avatar = scope.user.avatar.versions.avatar?[DPR] || scope.user.avatar.url
          img = new Image()
          img.src = avatar

          img.onload = ->
            element.css "background-image", "url("+avatar+")"
            element.children('.svg').hide()

      element.bind 'click', (e) ->
        return unless scope.user
        scope.$apply ->
          if scope.referrer and scope.postid
            UserService.viewProfile(scope.user.id, {event: e, referrer: scope.referrer, postId: scope.postid})
          else
            UserService.viewProfile(scope.user.id, {event: e})


    directive =
      restrict: 'A'
      scope:
        user: '='
        referrer: '@'
        postid: '@'
        apple: '@'
      link: link

    return directive
    
  ]

  angular.module('Unii')
    .directive('uniiAvatar', Avatar)
)()