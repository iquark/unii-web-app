###
# @desc Directive for creating timestamps on cards in feed
# @file uniiCardTimestamp.directive.js.coffee
###
(->

  Timestamp = ['$filter', 'I18nService', ($filter, I18nService) ->

    ONE_SECOND = 1000
    ONE_MINUTE = ONE_SECOND * 60
    ONE_HOUR = ONE_MINUTE * 60
    ONE_DAY = ONE_HOUR * 24
    SEVEN_DAYS = ONE_DAY * 7
    OBFUSCATED_COLORS = ['#5bffc6', '#ffcb2f']

    link = (scope, element, attrs) ->

      object = scope.object()
      timestamp = Date.parse(object.created_at)

      postDate = new Date(object.created_at)
      postYear = postDate.getFullYear()

      currentYear = new Date()
      currentYear = currentYear.getFullYear()

      postAge =  Date.now() - timestamp


      if 'full' == attrs.uniiCardTimestamp
        timestamp = $filter('date')(object.created_at, "HH:mm") + ' ' + new Date(object.created_at).toLocaleDateString(I18nService.locale)

        scope.$parent.timestampObfuscated = true if object.user.color in OBFUSCATED_COLORS and ('status_update' == object.type)
      else
        timestamp = switch
          when postAge < ONE_MINUTE/3
            I18nService.t('now_title') # Just now
          when postAge < ONE_MINUTE
            time = parseInt(postAge/ONE_SECOND, 10)
            translation = if 'is_viewed' of object then 'multiple_seconds_ago_text' else 'multiple_seconds_ago'
            I18nService.t(translation, {time: time}) # 3m
          when postAge < ONE_HOUR
            time = parseInt(postAge/ONE_MINUTE, 10)
            translation = if 'is_viewed' of object then 'multiple_minutes_ago_text' else 'multiple_minutes_ago'
            I18nService.t(translation, {time: time}) # 3m
          when postAge < ONE_DAY
            time = parseInt(postAge/ONE_HOUR, 10)
            translation = if 'is_viewed' of object then 'multiple_hours_ago_text' else 'multiple_hours_ago_text'
            I18nService.t('multiple_hours_ago', {time: time}) # 7h
          when postAge < SEVEN_DAYS
            $filter('date')(object.created_at, "EEE") # Fri
          when (postAge > SEVEN_DAYS) and (postYear == currentYear)
            $filter('date')(object.created_at, "d MMM") # 15 Aug
          else
            new Date(object.created_at).toLocaleDateString(I18nService.locale) # 15/08/2014

      element.text(timestamp)

    directive =
      restrict: 'A'
      scope:
        object: '&'
      link: link

    return directive


]

  angular.module('Unii')
    .directive('uniiCardTimestamp', Timestamp)
)()
