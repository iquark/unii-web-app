###
# @desc Directive to simulate the mobile menu
# @file uniiMobileMenu.directive.js.coffee
# The data it expects is an array of objects like:
#   [
#     {label: "some text", highlight: true/false, handler: function that handles the click},
#     ...
#   ]
###

(->

  MobileMenu = ['I18nService', '$log', 'MobileMenuService', '$rootScope', (I18nService, $log, MobileMenuService, $rootScope) ->

    link = (scope, element) ->
      overlay = element.find('#overlay')
      overlay.bind 'click', MobileMenuService.close

      scope.showCancel = MobileMenuService.showCancel()
      scope.options = MobileMenuService.getOptions()
      scope.type = MobileMenuService.getType()
      scope.highlight = $rootScope.userSettings.color.name
      scope.close = MobileMenuService.close

      scope.$on '$destroy', ->
        $log.info 'MobileMenu: destroying'

    directive =
      restrict: 'E'
      templateUrl: 'views/'+I18nService.locale()+'/directives/menu.html'
      link: link
      scope: {}

    return directive
  ]

  angular.module('Unii')
    .directive('uniiMobileMenu', MobileMenu)
)()