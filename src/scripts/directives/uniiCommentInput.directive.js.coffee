###
# @desc Directive for comment input field in post view
# @file uniiCommentInput.directive.js.coffee
###

(->

  CommentInput = ['Browser', 'UI', (Browser, UI) ->

    link = (scope, element) ->

      _focus = ->
        scope.$apply -> UI.creatingComment = true

      _focusout = ->
        UI.creatingComment = false
        angular.element('body').get(0).style.webkitTransform = 'scale(1)' if Browser.isIos7() and angular.element('body').get(0).style.webkitTransform?

      element.bind 'focus', _focus
      element.bind 'focusout', _focusout

      # remove event listeners
      scope.$on '$destroy', ->
        angular.element('body').get(0).style.webkitTransform = '' if Browser.isIos7() and angular.element('body').get(0).style.webkitTransform?
        element.unbind 'focus', _focus
        element.unbind 'focusout', _focusout


    directive =
      restrict: 'A'
      link: link

    return directive

  ]

  angular.module('Unii')
    .directive('uniiCommentInput', CommentInput)
)()
