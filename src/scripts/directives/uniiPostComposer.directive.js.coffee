###
# @desc Directive for composing posts
# @file uniiPostComposer.directive.js.coffee
###

(->

  PostComposer = ['$timeout', 'UI', 'Browser', ($timeout, UI, Browser) ->

    link = (scope, element) ->
      scope.show_keyboard = false

      keyboard = element.find('#keyboard')
      media = element.find('#medias')
      textarea = element.find('#create-post-text')

      _show_keyboard = ->
        textarea.focus()

      keyboard.bind 'click', _show_keyboard

      textarea.bind 'focus', ->
        scope.$apply -> scope.show_keyboard = true
        keyboard.unbind 'click', _show_keyboard

      textarea.bind 'blur', ->
        scope.$apply -> scope.show_keyboard = false
        $timeout ->
          scope.$apply ->
            keyboard.bind 'click', _show_keyboard
          , 50

      media.bind 'click', ->
        scope.$apply -> scope.show_keyboard = false

    directive =
      restrict: 'A'
      link: link

    return directive

  ]

  angular.module('Unii')
    .directive('uniiPostComposer', PostComposer)
)()
