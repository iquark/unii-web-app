

(->

  Slideshow = ['LazyLoad', '$location', '$q', '$route', '$rootScope', 'MediaService', 'Mixpanel', (LazyLoad, $location, $q, $route, $rootScope, MediaService, Mixpanel) ->

    link = (scope, element) ->
      selector = angular.element(angular.element('.selector')[0])
      slide = angular.element(angular.element('.slide')[0])
      slides = element.find('#slides')
      token = angular.element(selector.find('#token')[0])
      bluePoint = selector.find('#point')
      selectorWidth =  parseInt(selector.css('width').split('px')[0], 10)
      tokenWidth  =  parseInt(token.css('width').split('px')[0], 10)
      scope.initialLoad = true
      photosViewed = 0
      Mixpanel.startEvent('Slideshow Viewed')

      # controls points where the token will be placed
      # to see those control points, you can write the following below #token
      # .point{'ng-repeat'=>"point in points", 'ng-style'=>"{'left': '{{point}}px'}"}
      scope.points = []
      newPosition = 0

      # the hole selector is clickable to select the photo you wish
      # the space is divided in portions, the percentage for each image
      selector.bind 'click', (e) ->
        portions = selectorWidth/number_of_slides_loaded
        position = Math.ceil(e.clientX/portions)
        scope.viewing = position
        _update_token()
        _jump_to_slide(scope.viewing)

      # listeners to allow to drag the token
      dragging = false

      token.on 'touchstart', (e) ->
        dragging = true
        token.addClass('dragging')
        newPosition = e.originalEvent.touches[0].clientX
        return false

      # for each movement it will move the token if dragging
      # and will load the nearest slide
      token.on 'touchmove', (e) ->
        if dragging
          left = parseInt(token.css('margin-left').split('px')[0], 10) # left of token
          left = 0 if isNaN(left)
          pos = e.originalEvent.touches[0].clientX
          newPosition = pos-tokenWidth/2

          token.css('margin-left', (newPosition)+'px')
          # calculates the position of the point in the token, to search
          # the nearest slide to select, that position is proportionally set
          # from the position of the token on the selector, here is a visual sample
          #  |-[ .   ]--------|
          #  |-----[  .  ]----|
          #  |--------[   . ]-|
          # that makes the experience easier to select slides on the borders
          newPoint = left+((pos/selectorWidth)*tokenWidth) # point into token (relative to the left of token)
          portion = selectorWidth/number_of_slides_loaded  # portion size
          currentPoint = Math.floor(newPoint/portion)      # get current point

          index = currentPoint + 1
          index = 1 if index <= 0  # if the user has moved to the left limit it will be the first
          # will be the last if is the right limit, and checks if necessary to call the next page
          if index > number_of_slides_loaded
            index = number_of_slides_loaded
            _load_next_page(index+2)

          scope.viewing = index
          scope.$apply()
          _jump_to_slide(scope.viewing)

        return false

      # once is finished it will smoothly move the token to the
      # nearest point, loading the slide attached to it
      token.on 'touchend', (e) ->
        dragging = false
        token.removeClass('dragging')
        _update_token()
        return false

      # constants
      SLIDE_BUFFER = 3

      # updates the position of the token
      _update_token = ->
        token.css('margin-left', (scope.points[scope.viewing-1])+'px')

      _calculate_points = ->
        portions = selectorWidth/number_of_slides_loaded
        minusToken = portions-tokenWidth

        scope.points = []
        scope.points.push portions*i+(minusToken*(i/(number_of_slides_loaded-1))) for i in [0..number_of_slides_loaded-1]

      # variables
      margin_top = 0
      scope.loading_next_page = false
      number_of_slides = scope.total_slides
      number_of_slides_loaded = scope.slides.length
      slides_window_height = element.find('.slides-window').height()
      slides_container_height = slides_window_height*(number_of_slides_loaded-1)
      portions = selectorWidth/number_of_slides_loaded
      _calculate_points()
      scope.viewing = Math.min((parseInt($location.hash(), 10) || 1), number_of_slides)

      # check the number is in the range
      scope.viewing = 1 if isNaN(scope.viewing) or scope.viewing < 1
      scope.viewing = scope.total_slides if scope.viewing > scope.total_slides

      _update_token()

      # private
      _move_to_slide = (slide) ->
        if slide == scope.viewing
          margin_top = -(scope.viewing*slides_window_height)
          slides.css 'margin-top', margin_top
          LazyLoad.trigger()
          scope.initialLoad = false
        else
          _jump_to_slide(scope.viewing)

      _load_next_page = (slide) ->
        # it stops the request if we are already loading
        # or the user changed to a slide we already have
        return if scope.loading_next_page or slide <= number_of_slides_loaded
        scope.loading_next_page = true
        request = MediaService.paginateImages($route.current.params.id)
        request.then(
          (data) ->
            scope.slides = data.data
            scope.slides.pagination = data.pagination
            number_of_slides_loaded = scope.slides.length
        )
        request.finally ->
          _calculate_points()
          _update_token()
          scope.loading_next_page = false
          _jump_to_slide(slide)
          scope.loading_next_page

      _beginning_of_slideshow = ->
        scope.viewing <= 1

      _end_of_slideshow = ->
        (number_of_slides_loaded == number_of_slides) and
          (number_of_slides == scope.viewing)

      _in_slideshow_buffer_zone = ->
        ((number_of_slides_loaded - scope.viewing) < SLIDE_BUFFER) and number_of_slides != number_of_slides_loaded

      _jump_to_slide = (slide) ->
        $location.hash(scope.viewing)
        photosViewed += 1

        if slide > number_of_slides_loaded
          _load_next_page(slide)
        else
          _move_to_slide(slide)
        true

      _jump_to_slide(scope.viewing)

      # public
      scope.slide_images_left = ->
        return if _end_of_slideshow()

        # load more images if reaching the end of slideshow
        if _in_slideshow_buffer_zone()
          _load_next_page() unless scope.loading_next_page

        scope.viewing += 1
        _jump_to_slide(scope.viewing)
        _update_token()
        true

      scope.slide_images_right = ->
        return if _beginning_of_slideshow()

        scope.viewing -= 1
        _jump_to_slide(scope.viewing)
        _update_token()
        true

      scope.range_slide_images = ->
        scope.viewing = parseInt(scope.viewing, 10)
        _jump_to_slide(scope.viewing)



      scope.$on '$destroy', ->
        currentUser = parseInt($route.current.params.id, 10) == $rootScope.userSettings.id
        Mixpanel.slideshowViewed(currentUser, photosViewed)


    directive =
      restrict: 'A'
      link: link

    return directive

  ]

  angular.module('Unii')
    .directive('uniiSlideshow', Slideshow)

)()
