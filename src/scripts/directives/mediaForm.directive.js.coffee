(->

  MediaForm = ['File', 'MediaService', '$log', '$interval', 'UI', '$rootScope', '$q', '$timeout', 'STATUSES', '$window', 'I18nService', (File, MediaService, $log, $interval, UI, $rootScope, $q, $timeout, STATUSES, $window, I18nService) ->

    link = (scope, element) ->
  
      scope.options =
        type: "POST"
        dataType: "xml"
        acceptFileTypes: /(\.|\/)(mov|m4v|mp4|jpe?g|png)$/i
        maxFileSize: 20000000 # 20MB
        loadVideoMaxFileSize: 50000000 # 50MB
        progressInterval: 10
        disableImageResize: false
        imageMaxWidth: 1920
        imageMaxHeight: 1920
        previewMinHeight: 300
        previewMaxHeight: 300
        previewMinWidth: 300
        previewMaxWidth: 300
        previewCrop: true
        sequentialUploads: true
        autoUpload: false

      submittingFile = false
      fileAddError = false

      # converts the file and then submits the data
      _convertImage = (file, data) ->

        _success = (blob) ->
          data.files[0] = blob
          _populateForm(data, blob)

        _error = (message) ->
          $log.error 'ImageConversion:error', message

        MediaService
          .convertImage(file)
          .then(_success, _error)

      # puts the info about the file to upload into the form, and submit it
      _populateForm = (data, file) ->
        # MUST check that file has it's metadata attached
        if file?.metadata and not submittingFile
          submittingFile = true
          form = angular.element('#media-form')
          form.attr('action', file.metadata.form_action)
          form.find('input[name="Content-type"]').val(file.metadata.form_fields['content-type'])
          form.find('input[name="policy"]').val(file.metadata.form_fields['Policy'])
          form.find('input[name="key"]').val(file.metadata.form_fields['key'])
          form.find('input[name="AWSAccessKeyId"]').val(file.metadata.form_fields['AWSAccessKeyId'])
          form.find('input[name="signature"]').val(file.metadata.form_fields['signature'])
          form.find('input[name="acl"]').val(file.metadata.form_fields['acl'])
          data.submit()
          submittingFile = false
        else
          $timeout(_populateForm, 1000, data, file)


      _addSingle = (data, fileExt) ->
        # Make API call to server by providing a file extension
        # On success, create a details object and attach to file object
        # Will also populate form with fields supplied from server.
        # The details object is used in the Post Creation service.
        deferred = $q.defer()
        file = data.files[0]
        fileType = File.getType(file)

        _success = (response) ->
          $log.debug 'Got file details from our API', response
          file.metadata =
            details:
              type: fileType
              url: response['final_location']
            form_fields: response.static_fields
            form_action: response.url
          
          deferred.resolve(file)

        _error = (message) ->
          deferred.reject(message)
        
        MediaService
          .addSingle(fileExt)
          .then(_success, _error)

        deferred.promise

      # the user tries to retry the submission
      event_fileretry = scope.$on 'fileretry', (e, queue, data) ->
        if queue.length == 0
          MediaService.setStatus(STATUSES.ERROR)
        else
          MediaService.setStatus(STATUSES.WORKING)
          namemap = {}

          # when the call to the API is successful convert the image if necessary
          # and submits the file
          _success = (response) ->
            if response.uniqueID
              j = namemap[response.uniqueID]
              if File.isType(/image/, response)
                _convertImage(response, data[j])
              else
                _populateForm(data[j], queue[j])
            else
              $log.error 'MediaForm: File has no uniqueID attached.'
              MediaService.setStatus(STATUSES.ERROR)



          # when the call to the API fails, the status changes to ERROR
          _error = ->
            MediaService.setStatus(STATUSES.ERROR)

          # if the file has all the data to populate, then send it to S3
          # otherwise before it will get the data to send it to S3
          for i in [0..queue.length-1] by 1
            delete queue[i].error if queue[i].error
            if queue[i].metadata and queue[i].metadata.details
              _populateForm(data[i], queue[i])
            else
              file = queue[i]
              fileExt = File.getExtension(file)
              file.uniqueID = Date.now()
              namemap[file.uniqueID] = i

              _addSingle(data[i], fileExt).then(_success, _error)

      # EVENTS CREATED BY THE MEDIA UPLOADER
      event_fileuploadadd = scope.$on 'fileuploadadd', (e, data) ->

        $log.info 'File added by user....adding file to the media'

        MediaService.getCallback('fileuploadadd')()
        file = data.files[0]

        fileAddError = false


        if File.isValid(file)
          # check if is there already a File in the media queue, and they are the same type, and if is an image
          # only one video is allowed
          mQueue = MediaService.mediaQueue()
          if mQueue.length > 0

            if File.getType(mQueue[0]) != File.getType(file)
              fileAddError = true
              data = config: 'post.multimedia_warning'
              $rootScope.$emit 'SHOW_MODAL', data, ->
                fileAddError = false

            else if File.getType(file) == 'Video'
              fileAddError = true
              data = config: 'post.multivideo_warning'
              $rootScope.$emit 'SHOW_MODAL', data, ->
                fileAddError = false
            else
              MediaService.getCallback('fileuploadadd')()
        else
          fileAddError = true
          data = config: 'post.filetype_warning'
          $rootScope.$emit 'SHOW_MODAL', data, ->
            fileAddError = false


      event_fileuploadprocessdone = scope.$on 'fileuploadprocessdone', (e, data) ->
        unless fileAddError
          MediaService.addToQueue(data)
          MediaService.getCallback('fileuploadprocessdone')()

      event_fileuploadstart = scope.$on 'fileuploadstart', (e, data) ->
        MediaService.setStatus(STATUSES.WORKING)
        $log.info 'uploading file', data

      event_fileuploadprogressall = scope.$on 'fileuploadprogressall', (e, data) ->
        progress = parseInt data.loaded / data.total * 100, 10
        $('.progress-bar').css 'width', "#{progress}%"

      event_fileuploaddone = scope.$on 'fileuploaddone', (e, data) ->
        $log.info 'File has finished uploading:', data

      event_fileuploadstop = scope.$on 'fileuploadstop', () ->
        $log.info 'ALL FILES HAVE FINISHED UPLOADING'
        MediaService.setStatus(STATUSES.IDLE)

      event_fileuploadfail = scope.$on 'fileuploadfail', () ->
        $log.info 'ALL FILES HAVE failed UPLOADING'
        MediaService.setStatus(STATUSES.ERROR)

      event_fileprocessfail = scope.$on 'fileuploadprocessfail', (e, data) ->
        $log.info 'MediaForm: File failed to process.'
        if /File is too large/.test(data.files[0].error)
          fileAddError = true
          data = config: 'post.filesize_warning'
          $rootScope.$emit 'SHOW_MODAL', data, ->
            fileAddError = false


      element.on '$destroy', ->
        $log.info 'destroying listeners'
        event_fileretry()
        event_fileuploadadd()
        event_fileuploadprocessdone()
        event_fileuploadstart()
        event_fileuploadprogressall()
        event_fileuploaddone()
        event_fileuploadstop()
        event_fileuploadfail()
        event_fileprocessfail()

    directive =
      restrict: 'E'
      template: '<form data-as="file" data-file-upload="options" id="media-form" method="post" enctype="multipart/form-data" name="MediaUploadForm">
            <input id="Content-type" name="Content-type" type="hidden" ng-value="content_type">
            <input id="key" name="key" type="hidden" ng-value="key">
            <input id="acl" name="acl" type="hidden" ng-value="acl">
            <input id="AWSAccessKeyId" name="AWSAccessKeyId" type="hidden" ng-value="AWSAccessKeyId">
            <input id="policy" name="policy" type="hidden" ng-value="policy">
            <input id="signature" name="signature" type="hidden" ng-value="signature">
            <input capture="camera" class="hidden needsclick" id="media-file" name="file" type="file">
            <input accept="image/*" class="hidden needsclick" id="image-file" name="file" type="file">
          </form>'
      link: link

    return directive

  ]

  angular.module('Unii')
    .directive('uniiMediaForm', MediaForm)
)()