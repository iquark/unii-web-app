(->

  EditProfile = ['CurrentUser', 'I18nService', 'Mixpanel', 'Navigation', '$rootScope', 'UI', 'Post', 'Validation', (CurrentUser, I18nService, Mixpanel, Navigation, $rootScope, UI, Post, Validation) ->

    link = (scope) ->

      scope.savingProfile = false

      scope.formValues = {
        genders: [
          {
            type: I18nService.t('male_option')
            value: 'male'
          }
          {
            type: I18nService.t('female_option')
            value: 'female'
          }
          {
            type: I18nService.t('undefined_option')
            value: 'gender_not_defined'
          }
        ]
        colors: [
          '#fe4444',
          '#fe9526',
          '#ffcb2f',
          '#5bffc6',
          '#60c9f8',
          '#cb49cb'
        ]
      }

      scope.checkName = ->
        scope.editForm.name.$error.lastName = false if !scope.editForm.name.$error.lastName
        Validation.checkName(scope.profileForm.name, scope.editForm.name)

      scope.submit = ->
        return if scope.savingProfile or scope.editForm.$pristine
        scope.savingProfile = true

        scope.profileForm.errors = []

        if scope.profileForm and scope.profileForm.name and !scope.editForm.name.$invalid

          # data related to the form
          form = {
            name: scope.profileForm.name
            gender: scope.profileForm.gender
            tagline: scope.profileForm.tagline
            color: scope.profileForm.color.code
            course_id: scope.profileForm.course?.id
          }

          Mixpanel.editProfile(form)

          request = CurrentUser.edit(form)

          request.success (data) ->

            # Store updated user in memory
            CurrentUser.store data.user
            # clear form
            CurrentUser.resetProfileForm()

            # clear feed cache - if user updates their name it will
            # not be updated in the feed otherwise
            Post.clearCache()

            Navigation.back()

          request.error (data) ->
            scope.profileForm.errors = []
            if data
              angular.forEach data.errors, (item) ->
                if item.code == 'name:cant_be_blank' and scope.editForm.name.$dirty
                  message = I18nService.t('name_incomplete_error_body')
                else
                  message = I18nService.t(item.code.replace(':', '_')+'_error_body')

                scope.profileForm.errors.push message

              scope.editForm.name.$error.lastName = true
              scope.editForm.name.$invalid = true
              data =
                config: 'general.errors'
                meta:
                  title: scope.profileForm.errors.join('<br>')
            else
              data =
                config: 'general.errors'
                meta:
                  title: I18nService.t('settings_failed_to_save_error_body')

            $rootScope.$emit 'SHOW_MODAL', data

          request.finally ->
            scope.savingProfile = false

        else
          scope.savingProfile = false

      nameListener = scope.$watch 'scope.editForm', ->
        scope.checkName()

      scope.$on '$destroy', ->
        nameListener()

    directive =
      restrict: 'A'
      link: link

    return directive

  ]

  angular.module('Unii')
    .directive('uniiEditProfileForm', EditProfile)
)()
