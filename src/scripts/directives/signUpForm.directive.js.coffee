###
# @desc Directive for signing up to the app
# @file signUpForm.directive.js.coffee
###

(->

  signUpForm = ['ipCookie', 'CurrentUser', '$timeout', 'Authentication', 'Mixpanel', 'Navigation', '$rootScope', 'Browser', 'ITUNES_LINK', 'Validation', 'I18nService', '$location', (ipCookie, CurrentUser, $timeout, Authentication, Mixpanel, Navigation, $rootScope, Browser, ITUNES_LINK, Validation, I18nService, $location) ->

    link = (scope) ->
      scope.signup =
        form: {}
        errors: {}

      scope.name_regex  = Validation.regex.name
      scope.email_regex = Validation.regex.email
      promoDomains = JSON.parse localStorage.getItem 'promo_domains'

      scope.checkName = ->
        Validation.checkName(scope.signup.form.name, scope.SignUpForm.uName)

      scope.promoUni = (email) ->
        if email
          domain = email.split(/@/)[1]
          if promoDomains and domain in promoDomains
            scope.showPromoField = true
        else
          scope.showPromoField = false

      scope.submit = ->
        Mixpanel.signupSubmitted(scope.signup.form)
        scope.registering = true

        # reset errors
        scope.signup.errors = {}

        if scope.SignUpForm.$valid

          _success = (data) ->
            ipCookie('token', data['data']['access_token'], {expires: 7, domain: $location.host()})

            if !data.data.user.is_fake_user
              # log with MixPanel
              Mixpanel.identify(data.data.user.id)

            $rootScope.$emit 'auth.signedin', data.data.user

            # redierct user to verification wall
            Navigation.go('preUni')

            # If the user is playing in the challenge, and has in an iOS
            # device, the native app should be opened or user redirected
            # to Unii app on AppStore.
            if Browser.isIos() and scope.promo_code
              angular.element('<iframe />').attr('src', 'unii://').attr('style', 'display:none;').appendTo('body')

              _itunesRedirect = ->
                $rootScope.redirectTo ITUNES_LINK

              $timeout(_itunesRedirect, 1000)

            _updateUI = ->
              scope.signup.errors = {}

            $timeout(_updateUI, 3000)

          _error = (data, status) ->
            scope.signup.errors = {}
            if data?
              if status!=500 && data?.data?.errors
                angular.forEach data.data.errors, (error) ->
                  scope.signup.errors[error.code.replace(':', '_')] = true
                  scope.SignUpForm.uName.$invalid = true if error.code.match('name')
                  scope.SignUpForm.uEmail.$invalid = true if error.code.match('email')
                  scope.SignUpForm.uPassword.$invalid = true if error.code.match('password')

              else
                scope.signup.errors['server_error_body'] = true
            else
              scope.signup.errors['server_error_body'] = true

          _finally = ->
            scope.registering = false

          Authentication
            .signup(scope.signup.form)
            .success(_success)
            .error(_error)
            .finally(_finally)

        else
          scope.registering = false
          scope.signup.errors = {}
          scope.signup.errors['form_incomplete_error_body'] = true

    directive =
      restrict: 'E'
      templateUrl: '/views/'+I18nService.locale()+'/directives/signup_form.html'
      link: link

    return directive


  ]

  angular.module('Unii')
    .directive('signUpForm', signUpForm)
)()
