###
# @desc Directive for signing into the app
# @file signInForm.directive.js.coffee
###

(->

  signInForm = ['Authentication', '$window', '$timeout', 'UI', 'Navigation', 'ipCookie', '$rootScope', 'CurrentUser', 'RAILS_ENV', 'Notification', 'I18nService', 'Mixpanel', '$location', (Authentication, $window, $timeout, UI, Navigation, ipCookie, $rootScope, CurrentUser, RAILS_ENV, Notification, I18nService, Mixpanel, $location) ->

    link = (scope) ->
      scope.submit = ->
        Mixpanel.signinSubmitted()

        UI.signing_in = true

        # reset errors
        scope.signin_form.errors = {}

        if scope.SignInForm.$valid

          signin = Authentication.signin(scope.signin_form.email, scope.signin_form.password)

          signin.success (data) ->

            user = data.data.user
            ipCookie('token', data['data']['access_token'], {expires: 7, domain: $location.host()})

            unless user.is_fake_user
              # log with MixPanel
              Mixpanel.identify(user.id)
              Mixpanel.register(user)

            $rootScope.$emit 'auth.signedin', data.data.user

            # redierct user to feed
            Navigation.go('feed')

            _clearForm = ->
              scope.signin_form = {}

            $timeout(_clearForm, 3000)

          signin.error (data) ->
            scope.signin_form.errors = {}
            if data?
              angular.forEach data.data.errors, (error) ->
                scope.signin_form.errors[error.code] = true
            else
              scope.signin_form.errors['server_error_body'] = true

          signin.finally ->
            UI.signing_in = false

        else
          scope.signin_form.errors = {}
          scope.signin_form.errors['user_credentials:are_invalid'] = true
          UI.signing_in = false

    directive =
      restrict: 'E'
      templateUrl: '/views/'+I18nService.locale()+'/directives/signin_form.html'
      link: link

    return directive

  ]

  angular.module('Unii')
    .directive('signInForm', signInForm)
)()


