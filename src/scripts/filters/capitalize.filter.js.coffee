###
# @desc Capitalizes the words of a string
# @file capitalize.filter.js.coffee
# @example <div class="name">{{ name | capitalize }}</div>
###

(->

  capitalize = ->
    return (string) ->
      return unless string

      return (
        string.split(' ').map (word) ->
          if word
            word[0].toUpperCase() + word[1..-1].toLowerCase()
          else
            ''
      ).join ' '

  angular.module('Unii')
    .filter('capitalize', capitalize)

)()