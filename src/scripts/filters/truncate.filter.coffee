###
# @desc Capitalizes the words of a string
# @file capitalize.filter.js.coffee
# @example <div class="name">{{ name | capitalize }}</div>
###

(->

  truncate = ->
    escapedInput = (string) ->
      return unless string
      string
        .replace(/&/g, '&amp;')
        .replace(/"/g, '&quot;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        
    return (string, truncateLength, expandable) ->

      input = string
      color = 'white'
      if input and (input.length > truncateLength)
        input = input.substring(0, truncateLength)
        while (input.charAt(input.length - 1) == ' ')
          input = input.substr(0, input.length - 1)

        input += '...'
        if expandable
          return escapedInput(input) + '<span class=\'see-more large\'>See more</span>'
        else
          return escapedInput(input)

      else
        return escapedInput(input)



  angular.module('Unii')
    .filter('truncate', truncate)

)()