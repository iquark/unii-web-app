// Generated on 2015-03-31 using generator-angular 0.11.1
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Configurable paths for the application
  var appConfig = {
    app: require('./bower.json').appPath || 'src',
    dist: 'dist'
  };

  // custom delimiters for i18n
  grunt.template.addDelimiters('custom', '{[', ']}');

  // Define the configuration for all the tasks
  grunt.initConfig({

    aws: grunt.file.readJSON('credentials.json'),
    pkg: grunt.file.readJSON('package.json'),

    // Project settings
    yeoman: appConfig,

    // Watches files for changes and runs tasks based on the changed files
    watch: {
      bower: {
        files: ['bower.json'],
        tasks: ['wiredep:build']
      },
      js: {
        files: ['<%= yeoman.app %>/scripts/{,*/}*.js'],
        tasks: ['newer:jshint:all'],
        options: {
          livereload: '<%= connect.options.livereload %>'
        }
      },
      jsTest: {
        files: ['test/spec/{,*/}*.js'],
        tasks: ['newer:jshint:test', 'karma']
      },
      compass: {
        files: ['<%= yeoman.app %>/styles/{,**/}*.{scss,sass}'],
        tasks: ['compass:build', 'autoprefixer', 'includeSource:build', 'wiredep']
      },
      haml: {
        files: ['<%= yeoman.app %>/views/{,**/}*.haml'],
        tasks: ['haml:build', 'i18n:build']
      },
      html: {
        files: ['<%= yeoman.app %>/index.html'],
        tasks: ['includeSource:build']
      },
      coffee: {
        files: ['<%= yeoman.app %>/scripts/{,**/}*.coffee'],
        exclude: ['<%= yeoman.app %>/scripts/config/production.js.coffee'],
        tasks: ['coffeelint', 'coffee:build', 'replace:build']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      livereload: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          '<%= yeoman.app %>/*.html',
          '<%= yeoman.app %>/views/{,**/}*.html',
          '<%= yeoman.app %>/images/{,**/}*.{png,jpg,jpeg,gif,webp,svg}'
        ]
      }
    },

    // The actual grunt server settings
    connect: {
      options: {
        port: 9000,
        // Change this to '0.0.0.0' to access the server from outside.
        hostname: '0.0.0.0',
        livereload: 35729
      },
      livereload: {
        options: {
          open: true,
          middleware: function (connect) {
            return [
              connect.static('build'),
              connect().use(
                '/bower_components',
                connect.static('./bower_components')
              ),
              connect().use(
                '/src/styles',
                connect.static('./src/styles')
              ),
              connect.static(appConfig.app)
            ];
          }
        }
      },
      testUnit: {
        options: {
          port: 9001,
          middleware: function (connect) {
            return [
              connect.static('build'),
              connect.static('test'),
              connect().use(
                '/bower_components',
                connect.static('./bower_components')
              ),
              connect.static(appConfig.app)
            ];
          }
        }
      },
      testSpec: {
        options: {
          port: 9002,
          middleware: function (connect) {
            return [
              connect.static('build'),
              connect.static('test'),
              connect().use(
                '/bower_components',
                connect.static('./bower_components')
              ),
              connect.static(appConfig.app)
            ];
          }
        }
      },
      dist: {
        options: {
          open: true,
          base: '<%= yeoman.dist %>'
        }
      }
    },

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: {
        src: [
          'Gruntfile.js',
          '<%= yeoman.app %>/scripts/{,**/}*.js'
        ]
      },
      test: {
        options: {
          jshintrc: 'test/.jshintrc'
        },
        src: ['test/spec/{,**/}*.js']
      }
    },

    coffeelint: {
      app: ['<%= yeoman.app %>/scripts/{,**/}*.coffee'],
      options: {
        'max_line_length': { 'level': 'ignore'},
        'no_trailing_whitespace': {'level': 'warn'}
      }
    },
    // Empties folders to start fresh
    clean: {
      dist: ['dist'],
      build: ['build', '.tmp'],
      temp: ['.tmp']
    },

    // Add vendor prefixed styles
    autoprefixer: {
      options: {
        browsers: ['last 1 version']
      },
      build: {
        options: {
          map: true,
        },
        files: [{
          expand: true,
          cwd: 'build/styles/',
          src: '{,**/}*.css',
          dest: 'build/styles/'
        }]
      },
      dist: {
        files: [{
          expand: true,
          cwd: 'build/styles/',
          src: '{,**/}*.css',
          dest: 'build/styles/'
        }]
      }
    },

    // Automatically inject Bower components into the app
    wiredep: {
      build: {
        src: ['build/index.html'],
        ignorePath:  /\.\.\//,
        exclude: [
        'bower_components/blueimp-file-upload/js/jquery.fileupload-jquery-ui.js',
        'bower_components/blueimp-tmpl/js/tmpl.js',
        'bower_components/blueimp-file-upload/js/jquery.fileupload-audio.js'
        ],
        overrides: {
          'blueimp-load-image': {
            'main': 'js/load-image.all.min.js'
          }
        }
      },
      dist: {
        src: ['<%= yeoman.dist %>/index.html'],
        ignorePath:  /\.\.\//,
        exclude: [
        'bower_components/blueimp-file-upload/js/jquery.fileupload-jquery-ui.js',
        'bower_components/blueimp-tmpl/js/tmpl.js',
        'bower_components/blueimp-file-upload/js/jquery.fileupload-audio.js'
        ],
        overrides: {
          'blueimp-load-image': {
            'main': 'js/load-image.all.min.js'
          }
        }
      },
      test: {
        devDependencies: true,
        src: '<%= karma.unit.configFile %>',
        ignorePath:  /\.\.\//,
        fileTypes:{
          js: {
            block: /(([\s\t]*)\/{2}\s*?bower:\s*?(\S*))(\n|\r|.)*?(\/{2}\s*endbower)/gi,
              detect: {
                js: /'(.*\.js)'/gi
              },
              replace: {
                js: '\'{{filePath}}\','
              }
            }
          }
      },
      sass: {
        src: ['<%= yeoman.app %>/styles/{,**/}*.{scss,sass}'],
        ignorePath: /(\.\.\/){1,2}bower_components\//
      }
    },

    // Compiles Sass to CSS and generates necessary files if requested
    compass: {
      options: {
        sassDir: '<%= yeoman.app %>/styles',
        cssDir: 'build/styles',
        generatedImagesDir: 'build/images/generated',
        imagesDir: '<%= yeoman.app %>/images',
        javascriptsDir: '<%= yeoman.app %>/scripts',
        fontsDir: '<%= yeoman.app %>/styles/fonts',
        importPath: './bower_components',
        httpImagesPath: '/images',
        httpGeneratedImagesPath: '/images/generated',
        httpFontsPath: '/styles/fonts',
        relativeAssets: false,
        assetCacheBuster: false,
        raw: 'Sass::Script::Number.precision = 10\n'
      },
      dist: {
        options: {
          generatedImagesDir: '<%= yeoman.dist %>/images/generated'
        }
      },
      build: {
        options: {
          sourcemap: true
        }
      }
    },

    // Renames files for browser caching purposes
    filerev: {
      dist: {
        src: [
          '<%= yeoman.dist %>/scripts/{,**/}*.js',
          '<%= yeoman.dist %>/styles/{,**/}*.css',
          '<%= yeoman.dist %>/images/{,**/}*.{png,jpg,jpeg,gif,webp,svg}',
          '<%= yeoman.dist %>/styles/fonts/{,**/}*.{eot,svg,ttf,woff}'
        ]
      }
    },

    // Reads HTML for usemin blocks to enable smart builds that automatically
    // concat, minify and revision files. Creates configurations in memory so
    // additional tasks can operate on them
    useminPrepare: {
      html: 'build/index.html',
      options: {
        dest: '<%= yeoman.dist %>',
        flow: {
          html: {
            steps: {
              js: ['concat', 'uglifyjs'],
              css: ['cssmin']
            },
            post: {}
          }
        }
      }
    },

    // Performs rewrites based on filerev and the useminPrepare configuration
    usemin: {
      html: ['<%= yeoman.dist %>/*.html', '<%= yeoman.dist %>/views/{,**/}*.html'],
      css: ['<%= yeoman.dist %>/styles/{,**/}*.css'],
      options: {
        assetsDirs: [
          '<%= yeoman.dist %>',
          '<%= yeoman.dist %>/images',
          '<%= yeoman.dist %>/styles'
        ]
      }
    },

    // The following *-min tasks will produce minified files in the dist folder
    // By default, your `index.html`'s <!-- Usemin block --> will take care of
    // minification. These next options are pre-configured if you do not wish
    // to use the Usemin blocks.
    cssmin: {
      dist: {
        files: {
          '<%= yeoman.dist %>/styles/app.min.css': [
            'build/styles/{,*/}*.css'
          ]
        }
      }
    },
    uglify: {
      dist: {
        files: {
          '<%= yeoman.dist %>/scripts/app.min.js': [
            'build/scripts/{,*/}*.js'
          ]
        }
      }
    },
    concat: {
      dist: {}
    },

    imagemin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>/images',
          src: ['{,**/}*.{png,jpg,jpeg,gif}'],
          dest: '<%= yeoman.dist %>/images'
        }]
      }
    },

    svgmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>/images',
          src: ['{,**/}*.svg', "!unii-icons/misc/avatar.svg"],
          dest: '<%= yeoman.dist %>/images'
        }]
      }
    },

    htmlmin: {
      dist: {
        options: {
          collapseWhitespace: true,
          conservativeCollapse: true,
          collapseBooleanAttributes: true,
          removeCommentsFromCDATA: true,
          removeOptionalTags: true
        },
        files: [{
          expand: true,
          cwd: '<%= yeoman.dist %>',
          src: ['*.html', 'views/{,**/}*.html'],
          dest: '<%= yeoman.dist %>'
        }]
      }
    },

    // ng-annotate tries to make the code safe for minification automatically
    // by using the Angular long form for dependency injection.
    ngAnnotate: {
      dist: {
        files: [{
          expand: true,
          cwd: 'build/concat/scripts',
          src: '*.js',
          dest: 'build/concat/scripts'
        }]
      }
    },

    // Replace Google CDN references
    cdnify: {
      dist: {
        html: ['<%= yeoman.dist %>/*.html']
      }
    },

    // Copies remaining files to places other tasks can use
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: 'build',
          dest: '<%= yeoman.dist %>',
          src: [
            '*.{ico,png,txt}',
            '.htaccess',
            'eula/index.html',
            'views/{,**/}*.html',
            'images/{,**/}*.{webp,svg}',
            'styles/fonts/{,**/}*.{eot,svg,ttf,woff}',
            'vendor/{,**/}*.{css,js}'
          ]
        }, {
          expand: true,
          cwd: 'build/images',
          dest: '<%= yeoman.dist %>/images',
          src: ['generated/*']
        }]
      },
      build: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= yeoman.app %>',
          dest: 'build',
          src: [
            '*.{ico,png,txt}',
            '.htaccess',
            'eula/index.html',
            'views/{,**/}*.html',
            'images/{,**/}*.{webp}',
            'images/unii-icons/misc/avatar.svg',
            'styles/fonts/{,**/}*.{eot,svg,ttf,woff}',
            'vendor/{,**/}*.{css,js}'
          ]
        }, {
          expand: true,
          cwd: 'build/images',
          dest: '<%= yeoman.dist %>/images',
          src: ['generated/*']
        }]
      },
      index: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= yeoman.app %>',
          dest: '<%= yeoman.dist %>',
          src: [
            'index.html'
          ]
        }]
      },
      html: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= yeoman.app %>',
          dest: 'build/',
          src: [
            'views/{,**/}*.html'
          ]
        }, {
          expand: true,
          cwd: 'build/images',
          dest: '<%= yeoman.dist %>/images',
          src: ['generated/*']
        }]
      },
      styles: {
        expand: true,
        cwd: '<%= yeoman.app %>/styles',
        dest: 'build/styles/',
        src: '{,**/}*.css'
      },
      bower: {
        files: [ {expand: true, cwd: 'build', src: 'index.html', dest: 'index.html'} ]
      }
    },

    // Run some tasks in parallel to speed up the build process
    concurrent: {
      build: [
        'compass:build',
        'haml:build',
        'coffee:build',
        'copy:build',
        'json:i18n'
      ],
      test: [
        'compass',
        'haml:build',
        'json:i18n'
      ],
      dist: [
        'compass:dist',
        'svgmin',
        'imagemin',
        'copy:dist',
        'copy:index',
        'cssmin',
        'uglify'
      ]
    },
    coffee: {
      test: {
        expand: true,
        sourceMap: true,
        sourceMapDir: 'build/maps/',
        flatten: false,
        src: ['<%= yeoman.app %>/scripts/{,**/}*.coffee', '<%= yeoman.app %>/scripts/!config/production.coffee', 'test/{,**/}*.coffee'],
        dest: 'build/scripts',
        ext: '.js'
      },
      build: {
        expand: true,
        flatten: false,
        cwd: '<%= yeoman.app %>/scripts',
        src: ['{,**/}*.coffee', '!config/production.coffee'],
        dest: 'build/scripts',
        ext: '.js'
      },
      dist: {
        expand: true,
        separator: ';',
        flatten: false,
        cwd: '<%= yeoman.app %>/scripts',
        src: ['{,**/}*.coffee', '!config/development.coffee'],
        dest: '<%= yeoman.dist %>/scripts',
        ext: '.js'
      }
    },
    haml: {
      options: {
        language: 'ruby',
        rubyHamlCommand: 'haml -q -t ugly'
      },
      html: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>/views',
          dest: 'build/views',
          src: ['{,**/}*.haml'],
          ext: '.html'
        }]
      },
      build: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>/views',
          dest: '.tmp/haml',
          src: ['{,**/}*.haml'],
          ext: '.html'
        }]
      }
    },
    i18n: {
      dist: {
        src: ['build/views/{,**/}*.html'],
        options: {
          output: '<%= yeoman.dist %>/views',
        }
      },
      build: {
        src: ['.tmp/haml/{,**/}*.html'],
        options: {
          output: 'build/views',
          base: '.tmp/haml',
        }
      },
      options: {
        base: 'build/views',
        format: 'json',
        locales: 'locales/*.json',
        delimiters: 'custom'
      }
    },
    json: {
      i18n: {
        options: {
            namespace: 'I18n',
            includePath: false,
            processName: function(filename) {
                return filename;
            }
        },
        src: ['locales/*.json'],
        dest: 'build/vendor/translations.js'
      }
    },
    shell: {
      options: {
          stderr: false
      },
      WEB_TRANSLATE_API_KEY: '3go7hJ0BLdcU2oNcjSSqTg',
      'wti-install': {
        command: 'bundler install'
      },
      'wti-init': {
        command: 'wti init <%= shell.WEB_TRANSLATE_API_KEY %>'
      },
      'wti-pull': {
        command: 'wti pull -t json -o locales/%locale%'
      }
    },
    // Test settings
    karma: {
      unit: {
        configFile: 'test/karma.conf.js',
        singleRun: true
      }
    },

    // Task to set environment variables
    replace: {

      build: {
        options: {
          patterns: [
            {
              match: 'environment',
              replacement: 'development'
            },
            {
              match: 'proxyDomain',
              replacement: 'http://unii-web-staging.herokuapp.com'
            },
            {
              match: 'apiDomain',
              replacement: 'development.platform.unii.com'
            },
            {
              match: 'appVersion',
              replacement: '<%=  pkg.version %>'
            }
          ]
        },
        files: [
          {expand: true, src: 'build/scripts/config/constants.js'}
        ]
      },
      staging: {
        options: {
          patterns: [
            {
              match: 'environment',
              replacement: 'staging'
            },
            {
              match: 'proxyDomain',
              replacement: 'http://unii-web-staging.herokuapp.com'
            },
            {
              match: 'apiDomain',
              replacement: 'development.platform.unii.com'
            },
            {
              match: 'appVersion',
              replacement: '<%=  pkg.version %>'
            }
          ]
        },
        files: [
          {expand: true, src: 'build/scripts/config/constants.js'}
        ]
      },
      production: {
        options: {
          patterns: [
            {
              match: 'environment',
              replacement: 'production'
            },
            {
              match: 'proxyDomain',
              replacement: 'http://unii-web.herokuapp.com'
            },
            {
              match: 'apiDomain',
              replacement: 'platform.unii.com'
            },
            {
              match: 'appVersion',
              replacement: '<%=  pkg.version %>'
            }
          ]
        },
        files: [
          {expand: true, src: 'build/scripts/config/constants.js'}
        ]
      }
    },
    // Takes all files in styles/scripts folder and injects inline tags
    // for them into html file.
    includeSource: {
      options: {
        basePath: 'build',
        baseUrl: '/'
      },
      build: {
        files: {
          'build/index.html': 'src/index.html'
        }
      },
      dist: {
        options: {
          basePath: '<%= yeoman.dist %>'
        },
        files: {
          '<%= yeoman.dist %>/index.html': 'src/index.html'
        }
      }
    },

    // Deploying to S3
    s3: {
      options: {
        accessKeyId: '<%= aws.accessKeyId %>',
        secretAccessKey: '<%= aws.secretAccessKey %>',
        region: 'eu-west-1'
      },
      staging: {
        options: {
          bucket: 'staging.app.unii.com'
        },
        cwd: 'dist/',
        src: '**'
      },
      production: {
        options: {
          bucket: 'app.unii.com'
        },
        cwd: 'dist/',
        src: '**'
      }
    }
  });


  grunt.registerTask('serve', 'Compile then start a connect web server', function (target) {
    if (target === 'dist') {
      grunt.log.writeln('Serving production app....');
      return grunt.task.run(['build:dist', 'connect:dist:keepalive']);
    }

    grunt.task.run([
      'build',
      'replace:build', // set env variables
      'connect:livereload',
      'watch'
    ]);
  });

  grunt.loadNpmTasks('grunt-haml');
  grunt.loadNpmTasks('grunt-i18n');
  grunt.loadNpmTasks('grunt-shell');
  grunt.loadNpmTasks('grunt-contrib-coffee');
  grunt.loadNpmTasks('grunt-json');
  grunt.loadNpmTasks('grunt-coffeelint');
  grunt.loadNpmTasks('grunt-replace');
  grunt.loadNpmTasks('grunt-include-source');
  grunt.loadNpmTasks('grunt-aws');

  grunt.registerTask('server', 'DEPRECATED TASK. Use the "serve" task instead', function (target) {
    grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
    grunt.task.run(['serve:' + target]);
  });

  grunt.registerTask('test:e2e', [
    'clean:build',
    'wiredep',
    'coffeelint',
    'concurrent:test',
    'autoprefixer:build',
    'connect:test_spec',
    'karma'
  ]);

  grunt.registerTask('test:unit', [
    'clean:build',
    'wiredep',
    'coffeelint',
    'json:i18n',
    'autoprefixer:build',
    'connect:test_unit',
    'karma'
  ]);

  grunt.registerTask('test', ['test:unit', 'test:e2e']);

  grunt.registerTask('staging', [
    'clean:dist',
    'wiredep',
    'coffeelint',
    'coffee:build',
    'replace:build',
    'useminPrepare',
    'concurrent:dist',
    'autoprefixer:dist',
    'concat',
    'ngAnnotate',
    'copy:dist',
    'copy:html',
    'i18n',
    'cdnify',
    'cssmin',
    'uglify',
    'filerev',
    'usemin',
    'htmlmin'
  ]);

  grunt.registerTask('build', [
    'clean:build',
    'coffeelint',
    'concurrent:build',
    'i18n:build',
    'autoprefixer:build',
    'includeSource:build',
    'wiredep:build',
    'clean:temp'
  ]);

  grunt.registerTask('build:dist', [
    'clean:dist',
    'coffeelint',
    'concurrent:dist',
    'includeSource:dist',
    'wiredep:dist',
    'useminPrepare',
    'concat',
    'cssmin',
    'uglify',
    'filerev',
    'usemin'
  ]);

  grunt.registerTask('deploy', 'Deploys the app to staging and/or production environments.', function(){

    if ('staging' === grunt.option('env')) {

      grunt.log.writeln('Deploying to staging env...');
      grunt.task.run([
        'build', // generate a new build
        'replace:staging', // set env variables
        'build:dist', // generate a new distribution build
        's3:staging' // deploy to staging
        ]);

    } else if ('production' === grunt.option('env')) {

      grunt.log.writeln('Deploying to production env...');
      grunt.task.run([
        'build', // generate a new build
        'replace:production', // set env variables
        'build:dist', // generate a new distribution build
        's3:production' // deploy to production
        ]);

    } else {
      grunt.fail.warn('Cannot deploy. Environment was not found.');
    }

  });

  grunt.registerTask('translations', [
    'shell:wti-install',
    'shell:wti-init',
    'shell:wti-pull'
  ]);

  grunt.registerTask('default', [
    'newer:jshint',
    'test',
    'build'
  ]);
};
