# Web-app

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.11.1.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Translations

The command to get the latests translations is:

`$ grunt translations`

It gets the WTI tool, inits with the API key and retrieve the translations in JSON.

The translations will be at `/locales` folder.

## Development

To start the server that uses livereload:

`$ grunt serve`

You can see all the generated files in the `/.tmp` folder.

# Build

To make the build of our Web App:

`$ grunt serve`

It will create the build into the `/dist` folder.

## Build with test

Just execute:

`$ grunt`

## File Uploader

Remove the following files:
* `bower_components/blueimp-load-image/js/load-image-ios.js`
* `bower_components/blueimp-load-image/js/load-image-orientation.js`
* `bower_components/blueimp-load-image/js/load-image-meta.js`
* `bower_components/blueimp-load-image/js/load-image-exif.js`
* `bower_components/blueimp-load-image/js/load-image-exif-map.js`
* `bower_components/blueimp-file-upload/js/jquery.fileupload-jquery-ui.js`
* `bower_components/blueimp-tmpl/js/tmpl.js`

Add following script tags to `app/index.html`:
* `<script src="bower_components/blueimp-load-image/js/load-image.all.min.js"></script>` _before_ `<script src="bower_components/blueimp-canvas-to-blob/js/canvas-to-blob.js"></script>`
