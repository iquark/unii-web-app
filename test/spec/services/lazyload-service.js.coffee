describe 'LazyLoad Service', ->
  lazyLoadService = null
  httpBackend = null
  _API_DOMAIN = null
  _window = null
  _document = null

  beforeEach(module('Unii', 'Testing', 'TestDevelopment', 'API.USERS'))

  beforeEach inject (LazyLoad, $httpBackend, API_DOMAIN, $window, $document) ->
    lazyLoadService = LazyLoad
    httpBackend = $httpBackend
    _API_DOMAIN = API_DOMAIN
    _window = $window
    _document = $document


  describe '#trigger', ->

    it 'should call fire the resize event', (done) ->
      spyOn(_window, 'dispatchEvent')
      lazyLoadService.trigger()
      expect(_window.dispatchEvent).toHaveBeenCalled()
      done()


  describe '#load', ->

    describe 'when element is in view', ->

      it 'should fire the callback', (done) ->
        _callback = ->
          console.log 'callback fired!'
          done()
        lazyLoadService.load(angular.element('body'), _callback)

    describe 'when element is NOT in view', ->

      it 'should NOT fire the callback', (done) ->

        clientRect = ->
          {
            top: -1000000
            bottom: 0
            left: 0
            right: 0
            width: 0
            height: 0
          }

        _callback = ->
          console.log 'callback fired!'
          done.fail()

        lazyLoadService.load(angular.element({getBoundingClientRect: clientRect}), _callback)
        done()
