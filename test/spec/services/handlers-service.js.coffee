describe 'Logging handler', ->
  logService = null

  beforeEach(module('Unii', 'Testing'))

  describe 'The app is executed on development', () ->

    beforeEach(module 'Unii', 'TestDevelopment')

    beforeEach inject(($log) ->
      logService = $log

      spyOn console, 'log'
      spyOn console, 'info'
      spyOn console, 'warn'
      spyOn console, 'debug'
      spyOn console, 'error'
    )

    it 'should show the message at the given level', ->
      logService.log 'message'
      logService.info 'message'
      logService.warn 'message'
      logService.debug 'message'
      logService.error 'message'

      expect(console.log).toHaveBeenCalled()
      expect(console.info).toHaveBeenCalled()
      expect(console.warn).toHaveBeenCalled()
      expect(console.debug).toHaveBeenCalled()
      expect(console.error).toHaveBeenCalled()

  describe 'The app is executed on production', ->
    beforeEach(module 'Unii', 'TestProduction')

    beforeEach inject(($log) ->
      logService = $log

    )

    it 'should not show any message, nor errors if New Relic is not set up into the system', ->
      spyOn console, 'log'
      spyOn console, 'info'
      spyOn console, 'warn'
      spyOn console, 'debug'
      spyOn console, 'error'

      logService.log 'message'
      logService.info 'message'
      logService.warn 'message'
      logService.debug 'message'
      logService.error 'message'

      expect(console.log).not.toHaveBeenCalled()
      expect(console.info).not.toHaveBeenCalled()
      expect(console.warn).not.toHaveBeenCalled()
      expect(console.debug).not.toHaveBeenCalled()
      expect(console.error).not.toHaveBeenCalled()

    it 'should not show any error if New Relic is not set up', inject(($window) ->
      $window.NREUM = {
        noticeError: ->
          null
      }
      spyOn($window.NREUM, 'noticeError')

      logService.error('message')

      expect($window.NREUM.noticeError).toHaveBeenCalled()
    )