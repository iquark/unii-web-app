describe 'Current User Service', ->
  userService = null
  rootScope = null
  mockCurrentUser = null
  testCurrentUser = null
  httpBackend = null
  _API_DOMAIN = null
  logService = null
  _API_CURRENT_USER = null
  _API_USER_POSTS = null
  
  
  beforeEach(module('Unii', 'Testing', 'TestDevelopment', 'API.USERS'))

  beforeEach inject((CurrentUser, $httpBackend, API_DOMAIN, $log, API_CURRENT_USER, API_USER_POSTS) ->
    userService = CurrentUser
    mockCurrentUser =
      first_name: 'Joe'
      last_name: 'Bloggs'
      email: 'joe.bloggs@fake.com'
    httpBackend = $httpBackend
    _API_DOMAIN = API_DOMAIN
    _API_CURRENT_USER = API_CURRENT_USER
    _API_USER_POSTS = API_USER_POSTS
    )

  describe '#me', ->
    it 'should return the current user profile', (done) ->

      httpBackend
        .expectGET("http://#{_API_DOMAIN}/api/v2/users/me")
        .respond(200, _API_CURRENT_USER)

      testCurrentUser = (data) ->
        # normalise user object
        _API_CURRENT_USER.user.color = {
          name: 'yellow'
          code: "#ffcb2f"
        }
        expect(data).toEqual(_API_CURRENT_USER.user)
        done()

      userService
        .get()
        .then(testCurrentUser)
        .catch(done.fail)

      httpBackend.flush()


  describe '#edit_me', ->

    it 'should update the current user', (done) ->

      httpBackend
        .expectPUT("http://#{_API_DOMAIN}/api/v2/users/me", _API_CURRENT_USER)
        .respond(200, {currentUser: _API_CURRENT_USER})

      testCurrentUser = (data) ->
        expect(data.currentUser).toEqual(_API_CURRENT_USER)
        done()

      userService
        .edit(_API_CURRENT_USER)
        .success(testCurrentUser)
        .error(done.fail)
        
      httpBackend.flush()


  describe '#setAvatar', ->
    mediaService = null
    file =
      metadata:
        details:
          url: 'test.jpg'
    avatarForm =
      avatar:
        url: file.metadata.details.url

    beforeEach inject (MediaService) ->
      mediaService = MediaService
      spyOn console, 'error'

    it 'should set avatar if there is media in the queue', ->

      mediaService.addToQueue({files: [file]})

      httpBackend
        .expectPUT("http://#{_API_DOMAIN}/api/v2/users/me", avatarForm)
        .respond(200)

      userService.setAvatar()
      httpBackend.flush()


    it 'should NOT set avatar if there is NO media in the queue', ->

      userService.setAvatar()
      expect(console.error).toHaveBeenCalled()

  describe '#switchStorage', ->

    it 'should switch the storage used to store current user', ->
      localStorage.setItem 'currentUser', JSON.stringify(mockCurrentUser)
      userService.switchStorage(false)
      expect(localStorage.getItem('currentUser')).toBeNull()
      expect(JSON.parse(sessionStorage.getItem('currentUser')).first_name).toEqual('Joe')
      expect(JSON.parse(sessionStorage.getItem('currentUser')).last_name).toEqual('Bloggs')
      expect(JSON.parse(sessionStorage.getItem('currentUser')).email).toEqual('joe.bloggs@fake.com')

  describe '#set', ->

    beforeEach inject(($injector) ->
      rootScope = $injector.get('$rootScope')
      spyOn(rootScope, '$emit')
      )

    it 'should emit when user settings have been changed', ->
      userService.store({first_name: 'Joe'})
      expect(rootScope.$emit).toHaveBeenCalledWith('USER_SETTINGS')

    it 'should normalize the attributes passed', ->
      userService.store({color: '#ffcb2f'}, (data) ->
        expect(data.color).toEqual({name: 'yellow', code: '#ffcb2f'})
        )

  describe '#get_notifications', ->

    it 'should call the notifications endpoint successfully', (done) ->
      httpBackend.expectGET("http://#{_API_DOMAIN}/api/v2/notifications").respond(200)
      userService.notifications().success(done).error(done.fail)
      httpBackend.flush()

  describe '#paginateNotifications', ->

    it 'should get the next page of notifications successfully', (done) ->
      httpBackend.expectGET("http://#{_API_DOMAIN}/api/v2/notifications?last_id=101").respond(200)
      userService.paginateNotifications({next_page: "http://#{_API_DOMAIN}/api/v2/notifications?last_id=101"}).success(done).error(done.fail)
      httpBackend.flush()

  describe '#markNotificationsAsViewed', ->

    beforeEach inject(($log) ->
      logService = $log
      spyOn console, 'log'
      )

    it 'should call the end point successfully and log results', (done) ->
      httpBackend.expectPUT("http://#{_API_DOMAIN}/api/v2/notifications", {ids: [1,2,4]}).respond(200)
      userService.markNotificationsAsViewed([1,2,4]).success ->
        expect(console.log).toHaveBeenCalled()
        done()
      .error(done.fail)
      httpBackend.flush()

  describe '#resendConfirmationEmail', ->

    it 'should call API to resend email', (done) ->

      httpBackend
        .expectPOST("http://#{_API_DOMAIN}/api/v2/users/me/resend_confirmation_email")
        .respond(200)

      userService
        .resendConfirmationEmail()
        .success(done)
        .error(done.fail)

      httpBackend.flush()


  describe '#resetPassword', ->

    it 'should call API to reset user password', (done) ->

      httpBackend
        .expectPOST("http://#{_API_DOMAIN}/api/v2/users/me/password/forgot", {email: _API_CURRENT_USER.email})
        .respond(200)

      userService
        .resetPassword(_API_CURRENT_USER.email)
        .success(done)
        .error(done.fail)

      httpBackend.flush()


      

