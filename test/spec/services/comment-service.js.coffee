describe 'Comment Service', ->
  commentService = null
  httpBackend = null
  _API_DOMAIN = null

  beforeEach(module('Unii', 'Testing', 'TestDevelopment', 'API.USERS'))

  beforeEach inject (Comment, $httpBackend, API_DOMAIN) ->
    commentService = Comment
    httpBackend = $httpBackend
    _API_DOMAIN = API_DOMAIN

  describe '#create', ->

    it 'should call API to create comment', (done) ->

      data = content: 'Test comment.'

      httpBackend
        .expectPOST("http://#{_API_DOMAIN}/api/v2/posts/1/comments", data)
        .respond(200)

      commentService
        .create(1, data)
        .success(done)
        .error(done.fail)

      httpBackend.flush()


  describe '#favourite', ->

    it 'should call API to favourite a comment', (done) ->

      httpBackend
        .expectPOST("http://#{_API_DOMAIN}/api/v2/posts/1/comments/1/favourites")
        .respond(200)

      commentService
        .favourite(1, 1)
        .success(done)
        .error(done.fail)

      httpBackend.flush()

  describe '#unfavourite', ->

    it 'should call API to unfavourite a comment', (done) ->

      httpBackend
        .expectDELETE("http://#{_API_DOMAIN}/api/v2/posts/1/comments/1/favourites")
        .respond(200)

      commentService
        .unfavourite(1, 1)
        .success(done)
        .error(done.fail)

      httpBackend.flush()

  describe '#load', ->

    it 'should call API to load more comments', (done) ->

      pagination = next_page: "http://#{_API_DOMAIN}/api/v2/posts/1/comments?last_id=100"

      httpBackend
        .expectGET("http://#{_API_DOMAIN}/api/v2/posts/1/comments?last_id=100")
        .respond(200)

      commentService
        .load(pagination)
        .success(done)
        .error(done.fail)

      httpBackend.flush()


