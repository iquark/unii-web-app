describe '$log Service', ->
  logService = null
  httpBackend = null
  _API_DOMAIN = null

  beforeEach(module('Unii', 'Testing'))

  describe 'when in production', ->

    beforeEach(module('TestProduction'))

    beforeEach inject ($log, $window) ->
      logService = $log

    describe '#log', ->

      it 'should return null', ->
        expect(logService.log('Logging some information..')).toBeNull()

    describe '#info', ->

      it 'should return null', ->
        expect(logService.info('Logging some information..')).toBeNull()

    describe '#warn', ->

      it 'should return null', ->
        expect(logService.warn('Logging a warning..')).toBeNull()

    describe '#debug', ->

      it 'should return null', ->
        expect(logService.debug('Debugging some information..')).toBeNull()



  describe 'when in development', ->

    beforeEach(module('TestDevelopment'))

    beforeEach inject ($log, $window) ->
      logService = $log

    describe '#log', ->

      it 'should log the comments', ->
        spyOn(console, 'log')
        logService.log('Logging some information..')
        expect(console.log).toHaveBeenCalled()

    describe '#info', ->

      it 'should log the information', ->
        spyOn(console, 'info')
        logService.info('Logging some information..')
        expect(console.info).toHaveBeenCalled()

    describe '#warn', ->

      it 'should rlog the warning', ->
        spyOn(console, 'warn')
        logService.warn('Logging a warning..')
        expect(console.warn).toHaveBeenCalled()

    describe '#debug', ->

      it 'should debug the information', ->
        spyOn(console, 'debug')
        logService.debug('Debugging some information..')
        expect(console.debug).toHaveBeenCalled()

    describe '#error', ->

      it 'should log the error', ->
        spyOn(console, 'error')
        logService.error('error')
        expect(console.error).toHaveBeenCalled()


