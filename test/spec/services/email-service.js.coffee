describe 'EmailService', ->
  emailService = null
  httpBackend = null
  _API_DOMAIN = null

  beforeEach(module('Unii', 'Testing', 'TestDevelopment', 'API.USERS'))

  beforeEach inject (Email, $httpBackend, API_DOMAIN) ->
    emailService = Email
    httpBackend = $httpBackend
    _API_DOMAIN = API_DOMAIN


  describe '#feedbackForm', ->


    it 'should call API to send form', (done) ->

      data = {}

      httpBackend
        .expectPOST("http://#{_API_DOMAIN}/feedback", data)
        .respond(200)

      emailService
        .feedbackForm(data)
        .success(done)
        .error(done.fail)

      httpBackend.flush()


