describe 'Authentication Service', ->
  authenticationService = null
  rootScope = null
  mockCurrentUser = null
  httpBackend = null
  _API_DOMAIN = null
  _API_CURRENT_USER = null
  rootScope = null

  beforeEach(module('Unii', 'Testing', 'TestDevelopment', 'API.USERS'))

  beforeEach inject (Authentication, $httpBackend, API_DOMAIN, API_CURRENT_USER, API_USER_POSTS, $rootScope) ->
    authenticationService = Authentication
    rootScope = $rootScope
    mockCurrentUser =
      first_name: 'Joe'
      last_name: 'Bloggs'
      email: 'joe.bloggs@fake.com'
      password: 'password'
    httpBackend = $httpBackend
    _API_CURRENT_USER = API_CURRENT_USER


  describe '#signin', ->

    it 'should call API to sign user in', (done) ->

      httpBackend
        .expectPOST("/api/signin", {email: mockCurrentUser.email, password: mockCurrentUser.password})
        .respond(200)

      authenticationService
        .signin(mockCurrentUser.email, mockCurrentUser.password)
        .success(done)
        .error(done.fail)

      httpBackend.flush()


  describe '#signout', ->

    it 'should call API to sign user out', (done) ->

      httpBackend
        .expectGET("/api/signout")
        .respond(200)

      authenticationService
        .signout()
        .success(done)
        .error(done.fail)

      httpBackend.flush()

    it 'should emit signed out broadcast', ->
      spyOn(rootScope, '$emit')
      authenticationService.signout()
      expect(rootScope.$emit).toHaveBeenCalledWith('auth.signout')


  describe '#signup', ->

    it 'should call API to sign user up', (done) ->

      form = {
        name: "#{mockCurrentUser.first_name} #{mockCurrentUser.last_name}"
        email: mockCurrentUser.email
        password: 'password'
      }

      httpBackend
        .expectPOST("/api/signup", form)
        .respond(200)

      authenticationService
        .signup(form)
        .success(done)
        .error(done.fail)

      httpBackend.flush()


