describe 'Link Service', ->
  linkService = null
  httpBackend = null
  _API_DOMAIN = null

  beforeEach(module('Unii', 'Testing', 'TestDevelopment', 'API.USERS'))

  beforeEach inject (Link, $httpBackend, API_DOMAIN) ->
    linkService = Link
    httpBackend = $httpBackend
    _API_DOMAIN = API_DOMAIN

  describe '#load', ->

    it 'should call API to send link', (done) ->

      httpBackend
        .expectPOST("http://#{_API_DOMAIN}/api/v2/medias", {type: 'Link', url: 'http://www.fake.com'})
        .respond(200)

      linkService
        .load('http://www.fake.com')
        .success(done)
        .error(done.fail)

      httpBackend.flush()


    it 'should return a promise', (done) ->
      promise = linkService.load('http://www.fake.com')
      expect(promise.then).toBeDefined()
      done()


  describe '#poll', ->

    it 'should call the API to poll requested URL', (done) ->

      httpBackend
        .expectGET("http://www.fake.com")
        .respond(200)

      linkService
        .poll('http://www.fake.com')
        .success(done)
        .error(done.fail)

      httpBackend.flush()

    it 'should return a promise', (done) ->
      promise = linkService.poll('http://www.fake.com')
      expect(promise.then).toBeDefined()
      done()


  describe '#getMetadata', ->

    it 'should call the API to get link metadata', (done) ->

      httpBackend
        .expectGET("http://#{_API_DOMAIN}/api/v2/medias/1")
        .respond(200)

      linkService
        .getMetadata(1)
        .success(done)
        .error(done.fail)

      httpBackend.flush()

    it 'should return a promise', (done) ->
      promise = linkService.getMetadata(1)
      expect(promise.then).toBeDefined()
      done()

  describe '#external', ->

    it 'should return a URL when http URL scheme is provided', ->
      expect(linkService.external('http://www.fake.com')).toEqual('http://www.fake.com')

    it 'should return a URL when http URL scheme is NOT provided', ->
      expect(linkService.external('www.fake.com')).toEqual('http://www.fake.com')

