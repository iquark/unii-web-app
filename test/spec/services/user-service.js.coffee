describe 'User Service', ->
  userModelService = null
  mediaService = null
  logService = null
  rootScope = null
  testCurrentUser = null
  testResponse = null
  httpBackend = null
  _API_DOMAIN = null
  logService = null
  _API_CURRENT_USER = null
  _API_USER_POSTS = null

  beforeEach(module('Unii', 'Testing', 'TestDevelopment', 'API.USERS'))

  beforeEach inject (UserService, $httpBackend, API_DOMAIN, $log, API_CURRENT_USER, API_USER_POSTS) ->
    userModelService = UserService
    httpBackend = $httpBackend
    _API_DOMAIN = API_DOMAIN
    logService = $log
    _API_CURRENT_USER = API_CURRENT_USER
    _API_USER_POSTS = API_USER_POSTS



  describe '#mute_user', ->

    it 'should call the API to mute user', (done) ->

      httpBackend
        .expectPOST("http://#{_API_DOMAIN}/api/v2/users/1/mute")
        .respond(200)

      userModelService
        .mute(1)
        .success(done)
        .error(done.fail)

      httpBackend.flush()

  describe '#unmute_user', ->

    it 'should call the API to unmute user', (done) ->

      httpBackend
        .expectDELETE("http://#{_API_DOMAIN}/api/v2/users/1/mute")
        .respond(200)

      userModelService
        .unmute(1)
        .success(done)
        .error(done.fail)

      httpBackend.flush()

  describe '#block_user', ->

    it 'should call the API to block a user', (done) ->

      httpBackend
        .expectPOST("http://#{_API_DOMAIN}/api/v2/users/1/block")
        .respond(200)

      userModelService
        .block(1)
        .success(done)
        .error(done.fail)

      httpBackend.flush()

  describe '#unblock_user', ->

    it 'should call the API to unblock a user', (done) ->

      httpBackend
        .expectDELETE("http://#{_API_DOMAIN}/api/v2/users/1/block")
        .respond(200)

      userModelService
        .unblock(1)
        .success(done)
        .error(done.fail)

      httpBackend.flush()

  describe '#get_profile', ->

    it 'should return the requested profile', (done) ->

      httpBackend
        .expectGET("http://#{_API_DOMAIN}/api/v2/users/1")
        .respond(200, {user: _API_CURRENT_USER})

      testCurrentUser = (data) ->
        expect(data).toEqual(_API_CURRENT_USER)
        done()

      userModelService
        .profile(1)
        .then(testCurrentUser)
        .catch(done.fail)

      httpBackend.flush()

  describe '#paginatePosts', ->

    it 'should return more posts for selected user', (done) ->

      httpBackend
        .expectGET("http://#{_API_DOMAIN}/api/v2/users/1/posts?last_id=101")
        .respond(200, _API_USER_POSTS)

      testResponse = (data) ->
        expect(data).toEqual(_API_USER_POSTS)
        done()

      userModelService
        .paginatePosts({next_page: "http://#{_API_DOMAIN}/api/v2/users/1/posts?last_id=101"})
        .success(testResponse)
        .error(done.fail)

      httpBackend.flush()



