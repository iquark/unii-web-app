fdescribe 'Activity Controller', ->
  userModelService = null
  mediaService = null
  logService = null
  rootScope = null
  testCurrentUser = null
  testResponse = null
  httpBackend = null
  _API_DOMAIN = null
  logService = null
  _API_CURRENT_USER = null
  _API_USER_POSTS = null
  _STATUSES = null
  _LINK = null
  $rootScope = null
  $controller = null
  controller = null
  $scope = null
  Post = null
  Navigation = null

  beforeEach(module('Unii', 'Testing', 'TestDevelopment', 'API.USERS'))

  beforeEach inject (MediaService, $httpBackend, API_DOMAIN, $log, API_CURRENT_USER, API_USER_POSTS, STATUSES, Link, _$controller_, _Post_, _$rootScope_, _Navigation_) ->
    mediaService = MediaService
    httpBackend = $httpBackend
    _API_DOMAIN = API_DOMAIN
    logService = $log
    _API_CURRENT_USER = API_CURRENT_USER
    _API_USER_POSTS = API_USER_POSTS
    _STATUSES = STATUSES
    _LINK = Link
    $controller = _$controller_
    $rootScope = _$rootScope_
    $scope = $rootScope.$new()
    Post = _Post_
    controller = $controller('ActivityController', {$scope: $scope})
    Navigation = _Navigation_

  describe '$scope.notificationRedirect', ->
    it 'should call refer to Post.view', ->
      expect($scope.notificationRedirect).toEqual(Post.view)

  describe '$scope.goBack', ->
    it 'should call refer to Navigation.back', ->
      expect($scope.goBack).toEqual(Navigation.back)


