angular.module 'Testing', []
    .constant 'API_DOMAIN', 'localhost'
    .constant 'TIMEOUT', 15000
    .constant 'INTERVAL_NOTIFICATION', 5000
    .constant 'EMAIL_REGEX', /[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/
  	.constant 'URL_REGEX', /(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w\.-]*)*\/?/
    .constant 'NAME_REGEX', /^[a-zA-Z\s]+$/
    .constant 'ITUNES_LINK', 'https://itunes.apple.com/gb/app/unii/id900539930?mt=8'
    .constant 'STATUSES', {IDLE: 0, ERROR: 1, WORKING: 2, FINISHED: 3}

angular.module 'TestDevelopment', []
    .constant 'RAILS_ENV', 'development'

angular.module('TestProduction', [])
    .constant 'RAILS_ENV', 'production'