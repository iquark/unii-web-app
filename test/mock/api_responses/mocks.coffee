angular.module 'API.USERS', []
  .constant 'API_CURRENT_USER', {
    user: {
      id: 1
      first_name: 'Joe'
      last_name: 'Bloggs'
      email: 'joe.bloggs@fake.com'
      uni_email: 'joe.bloggs@unii.com'
      dob: '1974-04-25'
      gender: 'male'
      relationship_status: 'single'
      friends_count: 7
      tagline: "Forever young"
      color: "#ffcb2f"
      state: "uni_verified"
      avatar: {
        id: 2287
        type: "Image"
        url: "http://fake.com/img.jpg"
        versions: {}
        image_tags: []
      }
      university: {
        id: 1
        name: "Manchester University"
      }

      posts: {
        data: [
          {
            id: 56537
            type: "status_update"
            content: "MyText"
            created_at: "Wed, 11 Jun 2014 10:19:47 UTC +00:00"
            likes_count: 0
            comments_count: 0
            user_liked: false
            user: {
              id: 124319
              first_name: "first_name"
              last_name: "last_name"
              color: "#ffcb2f"
              avatar: {
                id: 132655,
                type: "Image"
                url: "http://fake.com/img.jpg"
                versions: {}
                image_tags: []
              }
            }
            location: null
            medias: []
            hashtag_items: []
            mentions: []
          }
        ]
        pagination: {
          current_page: 1
          total_pages: 1
          next_page: null
        }
      }
    }
  }

  .constant 'API_USER_POSTS', {
    posts: {
      data: [
        {
          id: 16,
          content: "Hey #jude. Hello, @James!"
          created_at: "2014-04-25 12:34:56 UTC"
          likes_count: 3
          comments_count: 1
          user_liked: false
          user: {
            id: 7
            first_name: "David"
            last_name: "Games"
            color: "#123"
            avatar: {
              id: 2287
              type: "Image"
              url: "http://fake.com/img.jpg"
              versions: {}
              image_tags: []
            }
          }
          medias: [
            {
              type: "Link"
              url: "http://instagram.com/ZSADCR"
              metadata: {
                subtype: "instagram"
                payload: {
                  media_type: "image"
                  id: "123123123123"
                  title: "Some Title"
                  like_count: 10
                  images: {
                    thumb: "http://distillery.s3.amazonaws.com/media/2010/07/16a.jpg"
                    small: "http://distillery.s3.amazonaws.com/media/2010/07/16b.jpg"
                    large: "http://distillery.s3.amazonaws.com/media/2010/07/16c.jpg"
                  }
                  user: {
                    username: 'kevin'
                    profile_url: 'http://instagram.com/kevin'
                  }
                }
              }
            }
          ]


          comments: {
            data: [
              {
                id: 1
                content: "That's awesome!"
                created_at: "2014-04-25 12:34:56 UTC"
                likes_count: 3
                user_liked: true
                user: {
                  id: 6
                  first_name: "James"
                  last_name: "Martin"
                  color: "#123"
                  avatar: {
                    id: 2287
                    type: "Image"
                    url: "http://fake.com/img.jpg"
                    versions: {}
                    image_tags: []
                  }
                }
                media: {
                  type: "Image"
                  url: "http://fake.com/img.png"
                }
              }
            ]
            pagination: {
              current_page: 0
              total_pages: 0
              next_page: null
            }
          }
        }
      ]
      pagination: {
        current_page: 1
        total_pages: 6
        next_page: "http://unii.herokuapp.com/api/v1/users/7/posts?page=2&last_id=86"
      }
    }
  }
